﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using PosCloudAdminPanel.Data;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using PosCloudAdminPanel.Core;
using PosCloudAdminPanel.Core.Model;
using PosCloudAdminPanel.Core.ViewModel;
using PosCloudAdminPanel.Persistence;
using Microsoft.Extensions.FileProviders;

namespace PosCloudAdminPanel
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.Configure<CookiePolicyOptions>(options =>
            {
                // This lambda determines whether user consent for non-essential cookies is needed for a given request.
                options.CheckConsentNeeded = context => true;
                options.MinimumSameSitePolicy = SameSiteMode.None;
            });
        
            
            services.AddDbContext<ApplicationDbContext>(options =>
                options.UseSqlServer(
                    Configuration.GetConnectionString("DefaultConnection")));

            services.AddDefaultIdentity<ApplicationUser>()
                .AddEntityFrameworkStores<ApplicationDbContext>()
                .AddDefaultUI();

            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);
           


            services.AddScoped<IUnitOfWork, UnitOfWork>();
        }
        

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseDatabaseErrorPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                app.UseHsts();
            }

            Mapper.Initialize(config =>
                {
                    config.CreateMap<ClientViewModel, Client>().ForMember(a=>a.ClientId,g=>g.Ignore());
                    config.CreateMap<Client, ClientViewModel>();
                    config.CreateMap<ClientAppKeyViewModel, ClientAppKey>();
                    config.CreateMap<ClientAppKey, ClientAppKeyViewModel>().ForMember(a=>a.ClientName,o=>o.MapFrom(g=>g.Client.CompanyName));
                    config.CreateMap<ComplaintViewModel, Complaint>();
                    config.CreateMap<Complaint, ComplaintViewModel>().ForMember(a=>a.User,o=>o.MapFrom(a=>a.ApplicationUser.UserName));
                    config.CreateMap<ComplaintReplyViewModel, ComplaintReply>();
                    config.CreateMap<ComplaintReply, ComplaintReplyViewModel>();
                    config.CreateMap<ProductViewModel, Product>();
                    config.CreateMap<Product, ProductViewModel>();
                    config.CreateMap<PackageViewModel, Package>();
                    config.CreateMap<Package, PackageViewModel>();
                    config.CreateMap<PackageProductViewModel, PackageProduct>();
                    config.CreateMap<PackageProduct, PackageProductViewModel>()
                        .ForMember(a=>a.PackageName,o=>o.MapFrom(g=>g.Package.Name))
                        .ForMember(a=>a.ProductName,o=>o.MapFrom(g=>g.Product.Name))
                        .ForMember(a => a.ProductPrice, o => o.MapFrom(g => g.Product.Price));
                    config.CreateMap<OrderViewModel, Order>();
                    config.CreateMap<Order, OrderViewModel>().ForMember(a=>a.ClientName,o=>o.MapFrom(g=>g.Client.FirstName + " " + g.Client.LastName));
                    config.CreateMap<OrderDetailViewModel, OrderDetail>();
                    config.CreateMap<OrderDetail, OrderDetailViewModel>();
                    config.CreateMap<InvoiceViewModel, Invoice>();
                    config.CreateMap<Invoice, InvoiceViewModel>();
                    config.CreateMap<Client, ClientApiViewModel>();
                }
                  );
            app.UseHttpsRedirection();
            app.UseDefaultFiles();
            app.UseStaticFiles();
            app.UseCookiePolicy();

            app.UseAuthentication();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "areaRoute",
                    template: "{area:exists}/{controller=Home}/{action=Index}/{id?}"
                    );
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });
        }
    }
}
