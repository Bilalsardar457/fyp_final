﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace PosCloudAdminPanel.Data.Migrations
{
    public partial class client3 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "ConfirmPassword",
                schema: "PosCloudAdmin",
                table: "Clients",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Password",
                schema: "PosCloudAdmin",
                table: "Clients",
                type: "varchar(6)",
                nullable: false,
                defaultValue: "");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ConfirmPassword",
                schema: "PosCloudAdmin",
                table: "Clients");

            migrationBuilder.DropColumn(
                name: "Password",
                schema: "PosCloudAdmin",
                table: "Clients");
        }
    }
}
