﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace PosCloudAdminPanel.Data.Migrations
{
    public partial class InitialMigration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.EnsureSchema(
                name: "PosCloudAdmin");

            migrationBuilder.CreateTable(
                name: "Clients",
                schema: "PosCloudAdmin",
                columns: table => new
                {
                    CreatedOn = table.Column<DateTime>(type: "DateTime2", nullable: false),
                    CreatedById = table.Column<string>(nullable: true),
                    UpdatedById = table.Column<string>(nullable: true),
                    UpdatedOn = table.Column<DateTime>(type: "DateTime2", nullable: false),
                    IsActive = table.Column<bool>(nullable: false),
                    ClientId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Name = table.Column<string>(type: "varchar", maxLength: 150, nullable: false),
                    Email = table.Column<string>(type: "varchar", maxLength: 150, nullable: false),
                    Address = table.Column<string>(type: "varchar", maxLength: 150, nullable: true),
                    PrimaryContact = table.Column<string>(type: "varchar", maxLength: 150, nullable: false),
                    SecondaryContact = table.Column<string>(type: "varchar", maxLength: 150, nullable: true),
                    City = table.Column<string>(type: "varchar", maxLength: 150, nullable: true),
                    State = table.Column<string>(type: "varchar", maxLength: 150, nullable: true),
                    Country = table.Column<string>(type: "varchar", maxLength: 150, nullable: true),
                    CompanyName = table.Column<string>(type: "nvarchar", maxLength: 150, nullable: false),
                    CompanyEmail = table.Column<string>(type: "varchar", maxLength: 150, nullable: false),
                    CompanyAddress = table.Column<string>(type: "nvarchar", maxLength: 150, nullable: false),
                    CompanyContact = table.Column<string>(type: "varchar", maxLength: 150, nullable: false),
                    CompanyCity = table.Column<string>(type: "varchar", maxLength: 150, nullable: true),
                    CompanyState = table.Column<string>(type: "varchar", maxLength: 150, nullable: true),
                    CompanyCountry = table.Column<string>(type: "varchar", maxLength: 150, nullable: true),
                    CompanyNtn = table.Column<string>(type: "varchar", maxLength: 150, nullable: true),
                    CompanyRegistrationNumber = table.Column<string>(type: "varchar", maxLength: 150, nullable: true),
                    CompanyLogo = table.Column<byte[]>(type: "varbinary(MAX)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Clients", x => x.ClientId);
                });

            migrationBuilder.CreateTable(
                name: "ClientAppKey",
                schema: "PosCloudAdmin",
                columns: table => new
                {
                    CreatedOn = table.Column<DateTime>(type: "DateTime2", nullable: false),
                    CreatedById = table.Column<string>(nullable: true),
                    UpdatedById = table.Column<string>(nullable: true),
                    UpdatedOn = table.Column<DateTime>(type: "DateTime2", nullable: false),
                    IsActive = table.Column<bool>(nullable: false),
                    ComplaintId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ClientId = table.Column<Guid>(nullable: false),
                    Subject = table.Column<string>(type: "varchar", maxLength: 150, nullable: false),
                    Description = table.Column<string>(type: "varchar", maxLength: 150, nullable: false),
                    Status = table.Column<string>(type: "varchar", maxLength: 150, nullable: false),
                    ComplaintType = table.Column<string>(type: "varchar", maxLength: 150, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ClientAppKey", x => x.ComplaintId);
                    table.ForeignKey(
                        name: "FK_ClientAppKey_Clients_ClientId",
                        column: x => x.ClientId,
                        principalSchema: "PosCloudAdmin",
                        principalTable: "Clients",
                        principalColumn: "ClientId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ClientAppKeys",
                schema: "PosCloudAdmin",
                columns: table => new
                {
                    CreatedOn = table.Column<DateTime>(type: "DateTime2", nullable: false),
                    CreatedById = table.Column<string>(nullable: true),
                    UpdatedById = table.Column<string>(nullable: true),
                    UpdatedOn = table.Column<DateTime>(type: "DateTime2", nullable: false),
                    IsActive = table.Column<bool>(nullable: false),
                    AppKey = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    IsUsed = table.Column<bool>(type: "bit", nullable: false, defaultValue: false),
                    ClientId = table.Column<Guid>(nullable: false),
                    IMEI = table.Column<string>(type: "varchar", maxLength: 150, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ClientAppKeys", x => new { x.ClientId, x.AppKey, x.IMEI });
                    table.ForeignKey(
                        name: "FK_ClientAppKeys_Clients_ClientId",
                        column: x => x.ClientId,
                        principalSchema: "PosCloudAdmin",
                        principalTable: "Clients",
                        principalColumn: "ClientId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ComplaintReplies",
                schema: "PosCloudAdmin",
                columns: table => new
                {
                    CreatedOn = table.Column<DateTime>(type: "DateTime2", nullable: false),
                    CreatedById = table.Column<string>(nullable: true),
                    UpdatedById = table.Column<string>(nullable: true),
                    UpdatedOn = table.Column<DateTime>(type: "DateTime2", nullable: false),
                    IsActive = table.Column<bool>(nullable: false),
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ComplaintId = table.Column<int>(nullable: false),
                    ApplicationUserId = table.Column<string>(nullable: false),
                    Subject = table.Column<string>(type: "varchar", maxLength: 150, nullable: false),
                    Description = table.Column<string>(type: "varchar", maxLength: 150, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ComplaintReplies", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ComplaintReplies_AspNetUsers_ApplicationUserId",
                        column: x => x.ApplicationUserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ComplaintReplies_ClientAppKey_ComplaintId",
                        column: x => x.ComplaintId,
                        principalSchema: "PosCloudAdmin",
                        principalTable: "ClientAppKey",
                        principalColumn: "ComplaintId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_ClientAppKey_ClientId",
                schema: "PosCloudAdmin",
                table: "ClientAppKey",
                column: "ClientId");

            migrationBuilder.CreateIndex(
                name: "IX_ClientAppKeys_IMEI",
                schema: "PosCloudAdmin",
                table: "ClientAppKeys",
                column: "IMEI",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_ComplaintReplies_ApplicationUserId",
                schema: "PosCloudAdmin",
                table: "ComplaintReplies",
                column: "ApplicationUserId");

            migrationBuilder.CreateIndex(
                name: "IX_ComplaintReplies_ComplaintId",
                schema: "PosCloudAdmin",
                table: "ComplaintReplies",
                column: "ComplaintId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ClientAppKeys",
                schema: "PosCloudAdmin");

            migrationBuilder.DropTable(
                name: "ComplaintReplies",
                schema: "PosCloudAdmin");

            migrationBuilder.DropTable(
                name: "ClientAppKey",
                schema: "PosCloudAdmin");

            migrationBuilder.DropTable(
                name: "Clients",
                schema: "PosCloudAdmin");
        }
    }
}
