﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace PosCloudAdminPanel.Data.Migrations
{
    public partial class vat : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "VatNumber",
                schema: "PosCloudAdmin",
                table: "Clients",
                type: "varchar(150)",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "VatNumber",
                schema: "PosCloudAdmin",
                table: "Clients");
        }
    }
}
