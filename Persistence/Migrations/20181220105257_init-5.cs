﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace PosCloudAdminPanel.Data.Migrations
{
    public partial class init5 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ClientAppKey_Clients_ClientId",
                schema: "PosCloudAdmin",
                table: "ClientAppKey");

            migrationBuilder.DropForeignKey(
                name: "FK_ComplaintReplies_ClientAppKey_ComplaintId",
                schema: "PosCloudAdmin",
                table: "ComplaintReplies");

            migrationBuilder.DropPrimaryKey(
                name: "PK_ClientAppKey",
                schema: "PosCloudAdmin",
                table: "ClientAppKey");

            migrationBuilder.RenameTable(
                name: "ClientAppKey",
                schema: "PosCloudAdmin",
                newName: "Complaints",
                newSchema: "PosCloudAdmin");

            migrationBuilder.RenameColumn(
                name: "Status",
                schema: "PosCloudAdmin",
                table: "Complaints",
                newName: "Message");

            migrationBuilder.RenameColumn(
                name: "Description",
                schema: "PosCloudAdmin",
                table: "Complaints",
                newName: "ComplaintStatus");

            migrationBuilder.RenameIndex(
                name: "IX_ClientAppKey_ClientId",
                schema: "PosCloudAdmin",
                table: "Complaints",
                newName: "IX_Complaints_ClientId");

            migrationBuilder.AlterColumn<Guid>(
                name: "ClientId",
                schema: "PosCloudAdmin",
                table: "Complaints",
                nullable: true,
                oldClrType: typeof(Guid));

            migrationBuilder.AddColumn<DateTime>(
                name: "Date",
                schema: "PosCloudAdmin",
                table: "Complaints",
                type: "date",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<byte[]>(
                name: "Image",
                schema: "PosCloudAdmin",
                table: "Complaints",
                type: "varbinary(max)",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "IsReply",
                schema: "PosCloudAdmin",
                table: "Complaints",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<int>(
                name: "ParentComplaintId",
                schema: "PosCloudAdmin",
                table: "Complaints",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "UserId",
                schema: "PosCloudAdmin",
                table: "Complaints",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Complaints",
                schema: "PosCloudAdmin",
                table: "Complaints",
                column: "ComplaintId");

            migrationBuilder.CreateIndex(
                name: "IX_Complaints_ParentComplaintId",
                schema: "PosCloudAdmin",
                table: "Complaints",
                column: "ParentComplaintId");

            migrationBuilder.CreateIndex(
                name: "IX_Complaints_UserId",
                schema: "PosCloudAdmin",
                table: "Complaints",
                column: "UserId");

            migrationBuilder.AddForeignKey(
                name: "FK_ComplaintReplies_Complaints_ComplaintId",
                schema: "PosCloudAdmin",
                table: "ComplaintReplies",
                column: "ComplaintId",
                principalSchema: "PosCloudAdmin",
                principalTable: "Complaints",
                principalColumn: "ComplaintId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Complaints_Clients_ClientId",
                schema: "PosCloudAdmin",
                table: "Complaints",
                column: "ClientId",
                principalSchema: "PosCloudAdmin",
                principalTable: "Clients",
                principalColumn: "ClientId",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Complaints_Complaints_ParentComplaintId",
                schema: "PosCloudAdmin",
                table: "Complaints",
                column: "ParentComplaintId",
                principalSchema: "PosCloudAdmin",
                principalTable: "Complaints",
                principalColumn: "ComplaintId",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Complaints_AspNetUsers_UserId",
                schema: "PosCloudAdmin",
                table: "Complaints",
                column: "UserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ComplaintReplies_Complaints_ComplaintId",
                schema: "PosCloudAdmin",
                table: "ComplaintReplies");

            migrationBuilder.DropForeignKey(
                name: "FK_Complaints_Clients_ClientId",
                schema: "PosCloudAdmin",
                table: "Complaints");

            migrationBuilder.DropForeignKey(
                name: "FK_Complaints_Complaints_ParentComplaintId",
                schema: "PosCloudAdmin",
                table: "Complaints");

            migrationBuilder.DropForeignKey(
                name: "FK_Complaints_AspNetUsers_UserId",
                schema: "PosCloudAdmin",
                table: "Complaints");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Complaints",
                schema: "PosCloudAdmin",
                table: "Complaints");

            migrationBuilder.DropIndex(
                name: "IX_Complaints_ParentComplaintId",
                schema: "PosCloudAdmin",
                table: "Complaints");

            migrationBuilder.DropIndex(
                name: "IX_Complaints_UserId",
                schema: "PosCloudAdmin",
                table: "Complaints");

            migrationBuilder.DropColumn(
                name: "Date",
                schema: "PosCloudAdmin",
                table: "Complaints");

            migrationBuilder.DropColumn(
                name: "Image",
                schema: "PosCloudAdmin",
                table: "Complaints");

            migrationBuilder.DropColumn(
                name: "IsReply",
                schema: "PosCloudAdmin",
                table: "Complaints");

            migrationBuilder.DropColumn(
                name: "ParentComplaintId",
                schema: "PosCloudAdmin",
                table: "Complaints");

            migrationBuilder.DropColumn(
                name: "UserId",
                schema: "PosCloudAdmin",
                table: "Complaints");

            migrationBuilder.RenameTable(
                name: "Complaints",
                schema: "PosCloudAdmin",
                newName: "ClientAppKey",
                newSchema: "PosCloudAdmin");

            migrationBuilder.RenameColumn(
                name: "Message",
                schema: "PosCloudAdmin",
                table: "ClientAppKey",
                newName: "Status");

            migrationBuilder.RenameColumn(
                name: "ComplaintStatus",
                schema: "PosCloudAdmin",
                table: "ClientAppKey",
                newName: "Description");

            migrationBuilder.RenameIndex(
                name: "IX_Complaints_ClientId",
                schema: "PosCloudAdmin",
                table: "ClientAppKey",
                newName: "IX_ClientAppKey_ClientId");

            migrationBuilder.AlterColumn<Guid>(
                name: "ClientId",
                schema: "PosCloudAdmin",
                table: "ClientAppKey",
                nullable: false,
                oldClrType: typeof(Guid),
                oldNullable: true);

            migrationBuilder.AddPrimaryKey(
                name: "PK_ClientAppKey",
                schema: "PosCloudAdmin",
                table: "ClientAppKey",
                column: "ComplaintId");

            migrationBuilder.AddForeignKey(
                name: "FK_ClientAppKey_Clients_ClientId",
                schema: "PosCloudAdmin",
                table: "ClientAppKey",
                column: "ClientId",
                principalSchema: "PosCloudAdmin",
                principalTable: "Clients",
                principalColumn: "ClientId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_ComplaintReplies_ClientAppKey_ComplaintId",
                schema: "PosCloudAdmin",
                table: "ComplaintReplies",
                column: "ComplaintId",
                principalSchema: "PosCloudAdmin",
                principalTable: "ClientAppKey",
                principalColumn: "ComplaintId",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
