﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace PosCloudAdminPanel.Data.Migrations
{
    public partial class ComplaintFix1 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "ParentComplaintComplaintId",
                schema: "PosCloudAdmin",
                table: "Complaints",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Complaints_ParentComplaintComplaintId",
                schema: "PosCloudAdmin",
                table: "Complaints",
                column: "ParentComplaintComplaintId");

            migrationBuilder.AddForeignKey(
                name: "FK_Complaints_Complaints_ParentComplaintComplaintId",
                schema: "PosCloudAdmin",
                table: "Complaints",
                column: "ParentComplaintComplaintId",
                principalSchema: "PosCloudAdmin",
                principalTable: "Complaints",
                principalColumn: "ComplaintId",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Complaints_Complaints_ParentComplaintComplaintId",
                schema: "PosCloudAdmin",
                table: "Complaints");

            migrationBuilder.DropIndex(
                name: "IX_Complaints_ParentComplaintComplaintId",
                schema: "PosCloudAdmin",
                table: "Complaints");

            migrationBuilder.DropColumn(
                name: "ParentComplaintComplaintId",
                schema: "PosCloudAdmin",
                table: "Complaints");
        }
    }
}
