﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace PosCloudAdminPanel.Data.Migrations
{
    public partial class new6 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "FaxNo",
                schema: "PosCloudAdmin",
                table: "Clients",
                type: "varchar(150)",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "varchar(150)");

            migrationBuilder.AddColumn<int>(
                name: "PackageId",
                schema: "PosCloudAdmin",
                table: "Clients",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateTable(
                name: "Orders",
                schema: "PosCloudAdmin",
                columns: table => new
                {
                    CreatedOn = table.Column<DateTime>(type: "DateTime2", nullable: false),
                    CreatedById = table.Column<string>(nullable: true),
                    UpdatedById = table.Column<string>(nullable: true),
                    UpdatedOn = table.Column<DateTime>(type: "DateTime2", nullable: false),
                    IsActive = table.Column<bool>(nullable: false),
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ClientId = table.Column<Guid>(nullable: false),
                    PackageName = table.Column<string>(type: "varchar(150)", nullable: false),
                    Price = table.Column<decimal>(type: "decimal", nullable: false),
                    Discount = table.Column<decimal>(type: "decimal", nullable: false),
                    PaymentTerms = table.Column<string>(type: "varchar(150)", nullable: false),
                    OrderDate = table.Column<DateTime>(type: "datetime", nullable: false),
                    IsInvoiced = table.Column<bool>(type: "bit", nullable: false, defaultValue: false),
                    Status = table.Column<bool>(type: "bit", nullable: false, defaultValue: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Orders", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Orders_Clients_ClientId",
                        column: x => x.ClientId,
                        principalSchema: "PosCloudAdmin",
                        principalTable: "Clients",
                        principalColumn: "ClientId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Packages",
                schema: "PosCloudAdmin",
                columns: table => new
                {
                    CreatedOn = table.Column<DateTime>(type: "DateTime2", nullable: false),
                    CreatedById = table.Column<string>(nullable: true),
                    UpdatedById = table.Column<string>(nullable: true),
                    UpdatedOn = table.Column<DateTime>(type: "DateTime2", nullable: false),
                    IsActive = table.Column<bool>(nullable: false),
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(type: "varchar(150)", nullable: false),
                    Price = table.Column<decimal>(type: "decimal", nullable: false),
                    Discount = table.Column<decimal>(type: "decimal", nullable: false),
                    PaymentTerms = table.Column<string>(type: "varchar(150)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Packages", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Products",
                schema: "PosCloudAdmin",
                columns: table => new
                {
                    CreatedOn = table.Column<DateTime>(type: "DateTime2", nullable: false),
                    CreatedById = table.Column<string>(nullable: true),
                    UpdatedById = table.Column<string>(nullable: true),
                    UpdatedOn = table.Column<DateTime>(type: "DateTime2", nullable: false),
                    IsActive = table.Column<bool>(nullable: false),
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(type: "varchar(150)", nullable: false),
                    Price = table.Column<decimal>(type: "decimal", nullable: false),
                    Description = table.Column<string>(type: "varchar(150)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Products", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Invoices",
                schema: "PosCloudAdmin",
                columns: table => new
                {
                    CreatedOn = table.Column<DateTime>(type: "DateTime2", nullable: false),
                    CreatedById = table.Column<string>(nullable: true),
                    UpdatedById = table.Column<string>(nullable: true),
                    UpdatedOn = table.Column<DateTime>(type: "DateTime2", nullable: false),
                    IsActive = table.Column<bool>(nullable: false),
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    OrderId = table.Column<int>(nullable: false),
                    DueAmount = table.Column<decimal>(type: "decimal", nullable: false),
                    PaidAmount = table.Column<decimal>(type: "decimal", nullable: false),
                    PaymentDate = table.Column<DateTime>(type: "datetime", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Invoices", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Invoices_Orders_OrderId",
                        column: x => x.OrderId,
                        principalSchema: "PosCloudAdmin",
                        principalTable: "Orders",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "OrderDetails",
                schema: "PosCloudAdmin",
                columns: table => new
                {
                    CreatedOn = table.Column<DateTime>(type: "DateTime2", nullable: false),
                    CreatedById = table.Column<string>(nullable: true),
                    UpdatedById = table.Column<string>(nullable: true),
                    UpdatedOn = table.Column<DateTime>(type: "DateTime2", nullable: false),
                    IsActive = table.Column<bool>(nullable: false),
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    OrderId = table.Column<int>(nullable: false),
                    ProductName = table.Column<string>(type: "varchar(150)", nullable: false),
                    Quantity = table.Column<decimal>(type: "decimal", nullable: false),
                    Price = table.Column<decimal>(type: "decimal", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_OrderDetails", x => x.Id);
                    table.ForeignKey(
                        name: "FK_OrderDetails_Orders_OrderId",
                        column: x => x.OrderId,
                        principalSchema: "PosCloudAdmin",
                        principalTable: "Orders",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "PackageProducts",
                schema: "PosCloudAdmin",
                columns: table => new
                {
                    CreatedOn = table.Column<DateTime>(type: "DateTime2", nullable: false),
                    CreatedById = table.Column<string>(nullable: true),
                    UpdatedById = table.Column<string>(nullable: true),
                    UpdatedOn = table.Column<DateTime>(type: "DateTime2", nullable: false),
                    IsActive = table.Column<bool>(nullable: false),
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    PackageId = table.Column<int>(nullable: false),
                    ProductId = table.Column<int>(nullable: false),
                    Quantity = table.Column<decimal>(type: "decimal", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PackageProducts", x => x.Id);
                    table.ForeignKey(
                        name: "FK_PackageProducts_Packages_PackageId",
                        column: x => x.PackageId,
                        principalSchema: "PosCloudAdmin",
                        principalTable: "Packages",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_PackageProducts_Products_ProductId",
                        column: x => x.ProductId,
                        principalSchema: "PosCloudAdmin",
                        principalTable: "Products",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Clients_PackageId",
                schema: "PosCloudAdmin",
                table: "Clients",
                column: "PackageId");

            migrationBuilder.CreateIndex(
                name: "IX_Invoices_OrderId",
                schema: "PosCloudAdmin",
                table: "Invoices",
                column: "OrderId");

            migrationBuilder.CreateIndex(
                name: "IX_OrderDetails_OrderId",
                schema: "PosCloudAdmin",
                table: "OrderDetails",
                column: "OrderId");

            migrationBuilder.CreateIndex(
                name: "IX_Orders_ClientId",
                schema: "PosCloudAdmin",
                table: "Orders",
                column: "ClientId");

            migrationBuilder.CreateIndex(
                name: "IX_PackageProducts_PackageId",
                schema: "PosCloudAdmin",
                table: "PackageProducts",
                column: "PackageId");

            migrationBuilder.CreateIndex(
                name: "IX_PackageProducts_ProductId",
                schema: "PosCloudAdmin",
                table: "PackageProducts",
                column: "ProductId");

            migrationBuilder.AddForeignKey(
                name: "FK_Clients_Packages_PackageId",
                schema: "PosCloudAdmin",
                table: "Clients",
                column: "PackageId",
                principalSchema: "PosCloudAdmin",
                principalTable: "Packages",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Clients_Packages_PackageId",
                schema: "PosCloudAdmin",
                table: "Clients");

            migrationBuilder.DropTable(
                name: "Invoices",
                schema: "PosCloudAdmin");

            migrationBuilder.DropTable(
                name: "OrderDetails",
                schema: "PosCloudAdmin");

            migrationBuilder.DropTable(
                name: "PackageProducts",
                schema: "PosCloudAdmin");

            migrationBuilder.DropTable(
                name: "Orders",
                schema: "PosCloudAdmin");

            migrationBuilder.DropTable(
                name: "Packages",
                schema: "PosCloudAdmin");

            migrationBuilder.DropTable(
                name: "Products",
                schema: "PosCloudAdmin");

            migrationBuilder.DropIndex(
                name: "IX_Clients_PackageId",
                schema: "PosCloudAdmin",
                table: "Clients");

            migrationBuilder.DropColumn(
                name: "PackageId",
                schema: "PosCloudAdmin",
                table: "Clients");

            migrationBuilder.AlterColumn<string>(
                name: "FaxNo",
                schema: "PosCloudAdmin",
                table: "Clients",
                type: "varchar(150)",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "varchar(150)",
                oldNullable: true);
        }
    }
}
