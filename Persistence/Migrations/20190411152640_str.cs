﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace PosCloudAdminPanel.Data.Migrations
{
    public partial class str : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "IsOperational",
                schema: "PosCloudAdmin",
                table: "Clients",
                type: "bit",
                nullable: false,
                defaultValue: false
                );

            migrationBuilder.AddColumn<string>(
                name: "Currency",
                schema: "PosCloudAdmin",
                table: "Clients",
                type: "varchar(150)",
                nullable: true
                
                );

            migrationBuilder.AddColumn<DateTime>(
                name: "BusinessStartTime",
                schema: "PosCloudAdmin",
                table: "Clients",
                type: "datetime",
                nullable: false,
                defaultValueSql:"CURRENT_TIMESTAMP"
                );
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<bool>(
                name: "IsOperational",
                schema: "PosCloudAdmin",
                table: "Clients",
                nullable: false,
                oldClrType: typeof(bool),
                oldType: "bit",
                oldDefaultValue: false);

            migrationBuilder.AlterColumn<string>(
                name: "Currency",
                schema: "PosCloudAdmin",
                table: "Clients",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "varchar(150)",
                oldNullable: true);

            migrationBuilder.AlterColumn<DateTime>(
                name: "BusinessStartTime",
                schema: "PosCloudAdmin",
                table: "Clients",
                nullable: false,
                oldClrType: typeof(DateTime),
                oldType: "datetime");
        }
    }
}
