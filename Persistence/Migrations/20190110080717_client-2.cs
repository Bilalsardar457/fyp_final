﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace PosCloudAdminPanel.Data.Migrations
{
    public partial class client2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "Name",
                schema: "PosCloudAdmin",
                table: "Clients",
                newName: "PostalCode");

            migrationBuilder.AddColumn<string>(
                name: "FaxNo",
                schema: "PosCloudAdmin",
                table: "Clients",
                type: "varchar(150)",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "FirstName",
                schema: "PosCloudAdmin",
                table: "Clients",
                type: "varchar(150)",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "LastName",
                schema: "PosCloudAdmin",
                table: "Clients",
                type: "varchar(150)",
                nullable: false,
                defaultValue: "");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "FaxNo",
                schema: "PosCloudAdmin",
                table: "Clients");

            migrationBuilder.DropColumn(
                name: "FirstName",
                schema: "PosCloudAdmin",
                table: "Clients");

            migrationBuilder.DropColumn(
                name: "LastName",
                schema: "PosCloudAdmin",
                table: "Clients");

            migrationBuilder.RenameColumn(
                name: "PostalCode",
                schema: "PosCloudAdmin",
                table: "Clients",
                newName: "Name");
        }
    }
}
