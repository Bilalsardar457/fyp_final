﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace PosCloudAdminPanel.Data.Migrations
{
    public partial class InitialCreate2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "Subject",
                schema: "PosCloudAdmin",
                table: "ComplaintReplies",
                type: "varchar(150)",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "varchar",
                oldMaxLength: 150);

            migrationBuilder.AlterColumn<string>(
                name: "Description",
                schema: "PosCloudAdmin",
                table: "ComplaintReplies",
                type: "varchar(150)",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "varchar",
                oldMaxLength: 150);

            migrationBuilder.AlterColumn<string>(
                name: "State",
                schema: "PosCloudAdmin",
                table: "Clients",
                type: "varchar(150)",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "varchar",
                oldMaxLength: 150,
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "SecondaryContact",
                schema: "PosCloudAdmin",
                table: "Clients",
                type: "varchar(150)",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "varchar",
                oldMaxLength: 150,
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "PrimaryContact",
                schema: "PosCloudAdmin",
                table: "Clients",
                type: "varchar(150)",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "varchar",
                oldMaxLength: 150);

            migrationBuilder.AlterColumn<string>(
                name: "Name",
                schema: "PosCloudAdmin",
                table: "Clients",
                type: "varchar(150)",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "varchar",
                oldMaxLength: 150);

            migrationBuilder.AlterColumn<string>(
                name: "Email",
                schema: "PosCloudAdmin",
                table: "Clients",
                type: "varchar(150)",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "varchar",
                oldMaxLength: 150);

            migrationBuilder.AlterColumn<string>(
                name: "Country",
                schema: "PosCloudAdmin",
                table: "Clients",
                type: "varchar(150)",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "varchar",
                oldMaxLength: 150,
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "CompanyState",
                schema: "PosCloudAdmin",
                table: "Clients",
                type: "varchar(150)",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "varchar",
                oldMaxLength: 150,
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "CompanyRegistrationNumber",
                schema: "PosCloudAdmin",
                table: "Clients",
                type: "varchar(150)",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "varchar",
                oldMaxLength: 150,
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "CompanyNtn",
                schema: "PosCloudAdmin",
                table: "Clients",
                type: "varchar(150)",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "varchar",
                oldMaxLength: 150,
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "CompanyName",
                schema: "PosCloudAdmin",
                table: "Clients",
                type: "nvarchar(150)",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "nvarchar",
                oldMaxLength: 150);

            migrationBuilder.AlterColumn<string>(
                name: "CompanyEmail",
                schema: "PosCloudAdmin",
                table: "Clients",
                type: "varchar(150)",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "varchar",
                oldMaxLength: 150);

            migrationBuilder.AlterColumn<string>(
                name: "CompanyCountry",
                schema: "PosCloudAdmin",
                table: "Clients",
                type: "varchar(150)",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "varchar",
                oldMaxLength: 150,
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "CompanyContact",
                schema: "PosCloudAdmin",
                table: "Clients",
                type: "varchar(150)",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "varchar",
                oldMaxLength: 150);

            migrationBuilder.AlterColumn<string>(
                name: "CompanyCity",
                schema: "PosCloudAdmin",
                table: "Clients",
                type: "varchar(150)",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "varchar",
                oldMaxLength: 150,
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "CompanyAddress",
                schema: "PosCloudAdmin",
                table: "Clients",
                type: "nvarchar(150)",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "nvarchar",
                oldMaxLength: 150);

            migrationBuilder.AlterColumn<string>(
                name: "City",
                schema: "PosCloudAdmin",
                table: "Clients",
                type: "varchar(150)",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "varchar",
                oldMaxLength: 150,
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Address",
                schema: "PosCloudAdmin",
                table: "Clients",
                type: "varchar(150)",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "varchar",
                oldMaxLength: 150,
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "IMEI",
                schema: "PosCloudAdmin",
                table: "ClientAppKeys",
                type: "varchar(150)",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "varchar",
                oldMaxLength: 150);

            migrationBuilder.AlterColumn<string>(
                name: "Subject",
                schema: "PosCloudAdmin",
                table: "ClientAppKey",
                type: "varchar(150)",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "varchar",
                oldMaxLength: 150);

            migrationBuilder.AlterColumn<string>(
                name: "Status",
                schema: "PosCloudAdmin",
                table: "ClientAppKey",
                type: "varchar(150)",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "varchar",
                oldMaxLength: 150);

            migrationBuilder.AlterColumn<string>(
                name: "Description",
                schema: "PosCloudAdmin",
                table: "ClientAppKey",
                type: "varchar(150)",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "varchar",
                oldMaxLength: 150);

            migrationBuilder.AlterColumn<string>(
                name: "ComplaintType",
                schema: "PosCloudAdmin",
                table: "ClientAppKey",
                type: "varchar(150)",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "varchar",
                oldMaxLength: 150);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "Subject",
                schema: "PosCloudAdmin",
                table: "ComplaintReplies",
                type: "varchar",
                maxLength: 150,
                nullable: false,
                oldClrType: typeof(string),
                oldType: "varchar(150)");

            migrationBuilder.AlterColumn<string>(
                name: "Description",
                schema: "PosCloudAdmin",
                table: "ComplaintReplies",
                type: "varchar",
                maxLength: 150,
                nullable: false,
                oldClrType: typeof(string),
                oldType: "varchar(150)");

            migrationBuilder.AlterColumn<string>(
                name: "State",
                schema: "PosCloudAdmin",
                table: "Clients",
                type: "varchar",
                maxLength: 150,
                nullable: true,
                oldClrType: typeof(string),
                oldType: "varchar(150)",
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "SecondaryContact",
                schema: "PosCloudAdmin",
                table: "Clients",
                type: "varchar",
                maxLength: 150,
                nullable: true,
                oldClrType: typeof(string),
                oldType: "varchar(150)",
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "PrimaryContact",
                schema: "PosCloudAdmin",
                table: "Clients",
                type: "varchar",
                maxLength: 150,
                nullable: false,
                oldClrType: typeof(string),
                oldType: "varchar(150)");

            migrationBuilder.AlterColumn<string>(
                name: "Name",
                schema: "PosCloudAdmin",
                table: "Clients",
                type: "varchar",
                maxLength: 150,
                nullable: false,
                oldClrType: typeof(string),
                oldType: "varchar(150)");

            migrationBuilder.AlterColumn<string>(
                name: "Email",
                schema: "PosCloudAdmin",
                table: "Clients",
                type: "varchar",
                maxLength: 150,
                nullable: false,
                oldClrType: typeof(string),
                oldType: "varchar(150)");

            migrationBuilder.AlterColumn<string>(
                name: "Country",
                schema: "PosCloudAdmin",
                table: "Clients",
                type: "varchar",
                maxLength: 150,
                nullable: true,
                oldClrType: typeof(string),
                oldType: "varchar(150)",
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "CompanyState",
                schema: "PosCloudAdmin",
                table: "Clients",
                type: "varchar",
                maxLength: 150,
                nullable: true,
                oldClrType: typeof(string),
                oldType: "varchar(150)",
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "CompanyRegistrationNumber",
                schema: "PosCloudAdmin",
                table: "Clients",
                type: "varchar",
                maxLength: 150,
                nullable: true,
                oldClrType: typeof(string),
                oldType: "varchar(150)",
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "CompanyNtn",
                schema: "PosCloudAdmin",
                table: "Clients",
                type: "varchar",
                maxLength: 150,
                nullable: true,
                oldClrType: typeof(string),
                oldType: "varchar(150)",
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "CompanyName",
                schema: "PosCloudAdmin",
                table: "Clients",
                type: "nvarchar",
                maxLength: 150,
                nullable: false,
                oldClrType: typeof(string),
                oldType: "nvarchar(150)");

            migrationBuilder.AlterColumn<string>(
                name: "CompanyEmail",
                schema: "PosCloudAdmin",
                table: "Clients",
                type: "varchar",
                maxLength: 150,
                nullable: false,
                oldClrType: typeof(string),
                oldType: "varchar(150)");

            migrationBuilder.AlterColumn<string>(
                name: "CompanyCountry",
                schema: "PosCloudAdmin",
                table: "Clients",
                type: "varchar",
                maxLength: 150,
                nullable: true,
                oldClrType: typeof(string),
                oldType: "varchar(150)",
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "CompanyContact",
                schema: "PosCloudAdmin",
                table: "Clients",
                type: "varchar",
                maxLength: 150,
                nullable: false,
                oldClrType: typeof(string),
                oldType: "varchar(150)");

            migrationBuilder.AlterColumn<string>(
                name: "CompanyCity",
                schema: "PosCloudAdmin",
                table: "Clients",
                type: "varchar",
                maxLength: 150,
                nullable: true,
                oldClrType: typeof(string),
                oldType: "varchar(150)",
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "CompanyAddress",
                schema: "PosCloudAdmin",
                table: "Clients",
                type: "nvarchar",
                maxLength: 150,
                nullable: false,
                oldClrType: typeof(string),
                oldType: "nvarchar(150)");

            migrationBuilder.AlterColumn<string>(
                name: "City",
                schema: "PosCloudAdmin",
                table: "Clients",
                type: "varchar",
                maxLength: 150,
                nullable: true,
                oldClrType: typeof(string),
                oldType: "varchar(150)",
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Address",
                schema: "PosCloudAdmin",
                table: "Clients",
                type: "varchar",
                maxLength: 150,
                nullable: true,
                oldClrType: typeof(string),
                oldType: "varchar(150)",
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "IMEI",
                schema: "PosCloudAdmin",
                table: "ClientAppKeys",
                type: "varchar",
                maxLength: 150,
                nullable: false,
                oldClrType: typeof(string),
                oldType: "varchar(150)");

            migrationBuilder.AlterColumn<string>(
                name: "Subject",
                schema: "PosCloudAdmin",
                table: "ClientAppKey",
                type: "varchar",
                maxLength: 150,
                nullable: false,
                oldClrType: typeof(string),
                oldType: "varchar(150)");

            migrationBuilder.AlterColumn<string>(
                name: "Status",
                schema: "PosCloudAdmin",
                table: "ClientAppKey",
                type: "varchar",
                maxLength: 150,
                nullable: false,
                oldClrType: typeof(string),
                oldType: "varchar(150)");

            migrationBuilder.AlterColumn<string>(
                name: "Description",
                schema: "PosCloudAdmin",
                table: "ClientAppKey",
                type: "varchar",
                maxLength: 150,
                nullable: false,
                oldClrType: typeof(string),
                oldType: "varchar(150)");

            migrationBuilder.AlterColumn<string>(
                name: "ComplaintType",
                schema: "PosCloudAdmin",
                table: "ClientAppKey",
                type: "varchar",
                maxLength: 150,
                nullable: false,
                oldClrType: typeof(string),
                oldType: "varchar(150)");
        }
    }
}
