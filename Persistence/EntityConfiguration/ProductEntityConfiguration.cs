﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using PosCloudAdminPanel.Core.Model;
using PosCloudAdminPanel.Data;

namespace PosCloudAdminPanel.Persistence.EntityConfiguration
{
    public class ProductEntityConfiguration : IEntityTypeConfiguration<Product>
    {
        public void Configure(EntityTypeBuilder<Product> productConfiguration)
        {
            productConfiguration.ToTable("Products", ApplicationDbContext.DEFAULT_SCHEMA);
          

            productConfiguration.HasKey(x => new { x.Id });

            productConfiguration.Property(x => x.Name).HasColumnType("varchar(150)").IsRequired();
            productConfiguration.Property(x => x.Price).HasColumnType("decimal").IsRequired();
            productConfiguration.Property(x => x.Description).HasColumnType("varchar(150)").IsRequired(false);
        }
    }
}
