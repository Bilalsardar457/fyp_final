﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using PosCloudAdminPanel.Core.Model;
using PosCloudAdminPanel.Data;

namespace PosCloudAdminPanel.Persistence.EntityConfiguration
{
    public class OrderEntityConfiguration:IEntityTypeConfiguration<Order>
    {
        public void Configure(EntityTypeBuilder<Order> orderBuilderConfiguration)
        {
            orderBuilderConfiguration.ToTable("Orders", ApplicationDbContext.DEFAULT_SCHEMA);


            orderBuilderConfiguration.HasKey(x => new { x.Id });

            orderBuilderConfiguration.Property(x => x.PackageName).HasColumnType("varchar(150)").IsRequired();
            orderBuilderConfiguration.Property(x => x.Price).HasColumnType("decimal").IsRequired();
            orderBuilderConfiguration.Property(x => x.Discount).HasColumnType("decimal").IsRequired();
            orderBuilderConfiguration.Property(x => x.PaymentTerms).HasColumnType("varchar(150)").IsRequired();
            orderBuilderConfiguration.Property(x => x.OrderDate).HasColumnType("datetime").IsRequired();
            orderBuilderConfiguration.Property(x => x.IsInvoiced).HasColumnType("bit").IsRequired().HasDefaultValue(false);
            orderBuilderConfiguration.Property(x => x.Status).HasColumnType("varchar(150)").IsRequired();

            orderBuilderConfiguration.HasOne(a => a.Client).WithMany(a => a.Orders).HasForeignKey(a => a.ClientId)
                .IsRequired().OnDelete(DeleteBehavior.Cascade);
        }
    }
}
