﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using PosCloudAdminPanel.Core.Model;
using PosCloudAdminPanel.Data;

namespace PosCloudAdminPanel.Persistence.EntityConfiguration
{
    public class PackageProductEntityConfiguration:IEntityTypeConfiguration<PackageProduct>
    {
        public void Configure(EntityTypeBuilder<PackageProduct> productPackgeConfiguration)
        {
            productPackgeConfiguration.ToTable("PackageProducts", ApplicationDbContext.DEFAULT_SCHEMA);


            productPackgeConfiguration.HasKey(x => new { x.Id });

            
            productPackgeConfiguration.Property(x => x.Quantity).HasColumnType("decimal").IsRequired();

            productPackgeConfiguration.HasOne(a => a.Product).WithMany(a => a.PackageProducts)
                .HasForeignKey(a => a.ProductId).IsRequired().OnDelete(DeleteBehavior.Cascade);
            productPackgeConfiguration.HasOne(a => a.Package).WithMany(a => a.PackageProducts)
                .HasForeignKey(a => a.PackageId).IsRequired().OnDelete(DeleteBehavior.Cascade);


        }
    }
}
