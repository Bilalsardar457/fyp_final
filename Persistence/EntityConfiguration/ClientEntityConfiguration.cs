﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using PosCloudAdminPanel.Core.Model;
using PosCloudAdminPanel.Data;

namespace PosCloudAdminPanel.Persistence.EntityConfiguration
{
    public class ClientEntityConfiguration : IEntityTypeConfiguration<Client>
    {

        public void Configure(EntityTypeBuilder<Client> clientConfiguration)
        {
            clientConfiguration.ToTable("Clients", ApplicationDbContext.DEFAULT_SCHEMA);
            //******************************************************************************************* KEYS ********************
            clientConfiguration.HasKey(x => x.ClientId);
            clientConfiguration.Property(x => x.ClientId).HasColumnType("uniqueidentifier")
                .ValueGeneratedOnAdd();
            //******************************************************************************************* PROPERTIES ***************
            clientConfiguration.Property(x => x.FirstName).HasColumnType("varchar(150)").IsRequired();
            clientConfiguration.Property(x => x.LastName).HasColumnType("varchar(150)").IsRequired();
            clientConfiguration.Property(x => x.PostalCode).HasColumnType("varchar(150)").IsRequired();
            clientConfiguration.Property(x => x.FaxNo).HasColumnType("varchar(150)").IsRequired(false);
            clientConfiguration.Property(x => x.VatNumber).HasColumnType("varchar(150)").IsRequired(false);
            clientConfiguration.Property(x => x.Password).HasColumnType("varchar(6)").IsRequired();
            
            clientConfiguration.Property(x => x.Email).HasColumnType("varchar(150)").IsRequired();
            clientConfiguration.Property(x => x.PrimaryContact).HasColumnType("varchar(150)").IsRequired();

            clientConfiguration.Property(x => x.Address).HasColumnType("varchar(150)").IsRequired(false);
            clientConfiguration.Property(x => x.SecondaryContact).HasColumnType("varchar(150)").IsRequired(false);
            clientConfiguration.Property(x => x.City).HasColumnType("varchar(150)").IsRequired(false);
            clientConfiguration.Property(x => x.State).HasColumnType("varchar(150)").IsRequired(false);
            clientConfiguration.Property(x => x.Country).HasColumnType("varchar(150)").IsRequired(false);

            clientConfiguration.Property(x => x.CompanyName).HasColumnType("nvarchar(150)").IsRequired();
            clientConfiguration.Property(x => x.CompanyEmail).HasColumnType("varchar(150)").IsRequired();
            clientConfiguration.Property(x => x.CompanyAddress).HasColumnType("nvarchar(150)").IsRequired();
            clientConfiguration.Property(x => x.CompanyContact).HasColumnType("varchar(150)").IsRequired();
            clientConfiguration.Property(x => x.Currency).HasColumnType("varchar(150)").IsRequired(false);
            clientConfiguration.Property(x => x.BusinessStartTime).HasColumnType("datetime").IsRequired();
            clientConfiguration.Property(x => x.CompanyCity).HasColumnType("varchar(150)").IsRequired(false);
            clientConfiguration.Property(x => x.CompanyState).HasColumnType("varchar(150)").IsRequired(false);
            clientConfiguration.Property(x => x.IsOperational).HasColumnType("bit").IsRequired().HasDefaultValue(false);



            clientConfiguration.Property(x => x.CompanyCountry).HasColumnType("varchar(150)").IsRequired(false);
            clientConfiguration.Property(x => x.CompanyNtn).HasColumnType("varchar(150)").IsRequired(false);
            clientConfiguration.Property(x => x.CompanyRegistrationNumber).HasColumnType("varchar(150)").IsRequired(false);
            clientConfiguration.Property(x => x.CompanyLogo).HasColumnType("varbinary(MAX)").IsRequired(false);

            clientConfiguration.HasOne(a => a.Package).WithMany(a => a.Clients).HasForeignKey(a => a.PackageId)
                .IsRequired().OnDelete(DeleteBehavior.Cascade);
        }
    }

    }
