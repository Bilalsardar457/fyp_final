﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using PosCloudAdminPanel.Core.Model;
using PosCloudAdminPanel.Data;

namespace PosCloudAdminPanel.Persistence.EntityConfiguration
{
    public class ClientAppKeyEntityConfiguration:IEntityTypeConfiguration<ClientAppKey>
    {
        public void Configure(EntityTypeBuilder<ClientAppKey> clientAppKeyConfiguration)
        {
            clientAppKeyConfiguration.ToTable("ClientAppKeys", ApplicationDbContext.DEFAULT_SCHEMA);
            clientAppKeyConfiguration.HasKey(x => new { x.ClientId, x.AppKey, x.IMEI });
            clientAppKeyConfiguration.Property(x => x.AppKey).HasColumnType("uniqueidentifier").ValueGeneratedOnAdd();
            clientAppKeyConfiguration.Property(x => x.IMEI).HasColumnType("varchar(150)").IsRequired();
            clientAppKeyConfiguration.Property(x => x.IsUsed).HasColumnType("bit").IsRequired().HasDefaultValue(false);
            clientAppKeyConfiguration.HasIndex(a => a.IMEI).IsUnique();

            clientAppKeyConfiguration.HasOne(a => a.Client)
                .WithMany(a => a.ClientAppKeys)
                .HasForeignKey(a => a.ClientId).IsRequired()
                .OnDelete(DeleteBehavior.Cascade);
        }
    }
}
