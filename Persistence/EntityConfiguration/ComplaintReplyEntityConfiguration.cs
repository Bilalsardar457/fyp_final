﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using PosCloudAdminPanel.Core.Model;
using PosCloudAdminPanel.Data;

namespace PosCloudAdminPanel.Persistence.EntityConfiguration
{
    public class ComplaintReplyEntityConfiguration:IEntityTypeConfiguration<ComplaintReply>
    {
        public void Configure(EntityTypeBuilder<ComplaintReply> clientAppKeyConfiguration)
        {
            clientAppKeyConfiguration.ToTable("ComplaintReplies", ApplicationDbContext.DEFAULT_SCHEMA);
            clientAppKeyConfiguration.HasKey(x => new { x.Id });
            
            clientAppKeyConfiguration.Property(x => x.Subject).HasColumnType("varchar(150)").IsRequired();
            clientAppKeyConfiguration.Property(x => x.Description).HasColumnType("varchar(150)").IsRequired();

            clientAppKeyConfiguration.HasOne(a => a.ApplicationUser)
                .WithMany(a => a.ComplaintReplies)
                .HasForeignKey(a => a.ApplicationUserId).IsRequired()
                .OnDelete(DeleteBehavior.Cascade);
            //clientAppKeyConfiguration.HasOne(a => a.Complaint)
            //    .WithMany(a => a.ComplaintReplies)
            //    .HasForeignKey(a => a.ComplaintId).IsRequired()
            //    .OnDelete(DeleteBehavior.Cascade);
        }
    }
}
