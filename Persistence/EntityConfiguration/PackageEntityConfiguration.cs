﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using PosCloudAdminPanel.Core.Model;
using PosCloudAdminPanel.Data;

namespace PosCloudAdminPanel.Persistence.EntityConfiguration
{
    public class PackageEntityConfiguration:IEntityTypeConfiguration<Package>
    {
        public void Configure(EntityTypeBuilder<Package> packageBuilderConfiguration)
        {
            packageBuilderConfiguration.ToTable("Packages", ApplicationDbContext.DEFAULT_SCHEMA);


            packageBuilderConfiguration.HasKey(x => new { x.Id });

            packageBuilderConfiguration.Property(x => x.Name).HasColumnType("varchar(150)").IsRequired();
            packageBuilderConfiguration.Property(x => x.Price).HasColumnType("decimal").IsRequired();
            packageBuilderConfiguration.Property(x => x.Discount).HasColumnType("decimal").IsRequired();
            packageBuilderConfiguration.Property(x => x.PaymentTerms).HasColumnType("varchar(150)").IsRequired();
        }
    }
}
