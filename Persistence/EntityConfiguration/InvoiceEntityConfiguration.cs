﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using PosCloudAdminPanel.Core.Model;
using PosCloudAdminPanel.Data;

namespace PosCloudAdminPanel.Persistence.EntityConfiguration
{
    public class InvoiceEntityConfiguration: IEntityTypeConfiguration<Invoice>
    {
        public void Configure(EntityTypeBuilder<Invoice> invoiceBuilderConfiguration)
        {
            invoiceBuilderConfiguration.ToTable("Invoices", ApplicationDbContext.DEFAULT_SCHEMA);


            invoiceBuilderConfiguration.HasKey(x => new { x.Id });

            
            invoiceBuilderConfiguration.Property(x => x.DueAmount).HasColumnType("decimal").IsRequired();
            invoiceBuilderConfiguration.Property(x => x.PaidAmount).HasColumnType("decimal").IsRequired();
            
            invoiceBuilderConfiguration.Property(x => x.PaymentDate).HasColumnType("datetime").IsRequired();

            invoiceBuilderConfiguration.HasOne(a => a.Order).WithMany(a => a.Invoices).HasForeignKey(a => a.OrderId)
                .IsRequired().OnDelete(DeleteBehavior.Cascade);

        }
    }
}
