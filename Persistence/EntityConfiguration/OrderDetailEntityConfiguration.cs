﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using PosCloudAdminPanel.Core.Model;
using PosCloudAdminPanel.Data;

namespace PosCloudAdminPanel.Persistence.EntityConfiguration
{
    public class OrderDetailEntityConfiguration:IEntityTypeConfiguration<OrderDetail>
    {
        public void Configure(EntityTypeBuilder<OrderDetail> orderDetailBuilderConfiguration)
        {
            orderDetailBuilderConfiguration.ToTable("OrderDetails", ApplicationDbContext.DEFAULT_SCHEMA);


            orderDetailBuilderConfiguration.HasKey(x => new { x.Id });

            orderDetailBuilderConfiguration.Property(x => x.ProductName).HasColumnType("varchar(150)").IsRequired();
            orderDetailBuilderConfiguration.Property(x => x.Price).HasColumnType("decimal").IsRequired();
            orderDetailBuilderConfiguration.Property(x => x.Quantity).HasColumnType("decimal").IsRequired();

            orderDetailBuilderConfiguration.HasOne(a => a.Order).WithMany(a => a.OrderDetails)
                .HasForeignKey(a => a.OrderId).IsRequired().OnDelete(DeleteBehavior.Cascade);
        }
    }
}
