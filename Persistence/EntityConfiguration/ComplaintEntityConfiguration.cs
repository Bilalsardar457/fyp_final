﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using PosCloudAdminPanel.Core.Model;
using PosCloudAdminPanel.Data;

namespace PosCloudAdminPanel.Persistence.EntityConfiguration
{
    public class ComplaintEntityConfiguration:IEntityTypeConfiguration<Complaint>
    {
        public void Configure(EntityTypeBuilder<Complaint> complaintConfiguration)
        {
            complaintConfiguration.ToTable("Complaints", ApplicationDbContext.DEFAULT_SCHEMA);
            complaintConfiguration.HasKey(x => new { x.ComplaintId });
            
            complaintConfiguration.Property(x => x.Subject).HasColumnType("varchar(150)").IsRequired();
            complaintConfiguration.Property(x => x.Message).HasColumnType("varchar(150)").IsRequired();
            complaintConfiguration.Property(x => x.ComplaintType).HasColumnType("varchar(150)").IsRequired();
            complaintConfiguration.Property(x => x.ComplaintStatus).HasColumnType("varchar(150)").IsRequired();
            complaintConfiguration.Property(a => a.Date).HasColumnType("datetime").IsRequired().HasDefaultValueSql("CURRENT_TIMESTAMP");
            complaintConfiguration.Property(a => a.Image).HasColumnType("varbinary(max)").IsRequired(false);
            complaintConfiguration.Property(a => a.IsReply).HasColumnType("bit").IsRequired().HasDefaultValue(false);
            complaintConfiguration.HasOne(x => x.ApplicationUser).WithMany(x => x.Complaints).HasForeignKey(x => x.UserId)
                .IsRequired().OnDelete(DeleteBehavior.Restrict);
            complaintConfiguration.HasOne(x => x.ParentComplaint).WithMany().HasForeignKey(x => x.ParentComplaintId)
                .IsRequired(false).OnDelete(DeleteBehavior.Restrict);
            complaintConfiguration.HasMany(x => x.ComplaintReplies).WithOne().HasForeignKey(x => x.ParentComplaintId)
                .IsRequired(false).OnDelete(DeleteBehavior.Restrict);
        }
    }
}
