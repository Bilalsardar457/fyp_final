﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Internal;
using PosCloudAdminPanel.Core.Model;
using PosCloudAdminPanel.Core.Repository;
using PosCloudAdminPanel.Data;

namespace PosCloudAdminPanel.Persistence.Repository
{
    public class ComplaintRepository:IComplaintRepository
    {
        private ApplicationDbContext _context;

        public ComplaintRepository(ApplicationDbContext context)
        {
            _context = context;
        }
        public IEnumerable<Complaint> GetComplaints()
        {
            return _context.Complaints.Include(a=>a.ApplicationUser).Where(a=>a.ParentComplaintId==null).ToList();
        }

        public Complaint GetComplaintById(int id)
        {
               var data= _context.Complaints.Include(a=>a.ApplicationUser).Include(a=>a.ComplaintReplies).FirstOrDefault(a=>a.ComplaintId==id);

            return data;
        }


        public void AddComplaint(Complaint complaint)
        {
            
                _context.Complaints.Add(complaint);
            
        }

        public void UpdateComplaint(int id, Complaint complaint)
        {
            if (complaint.ComplaintId != id)
            {
                complaint.ComplaintId = id;
            }
            else { }
            _context.Complaints.Attach(complaint);
            _context.Entry(complaint).State = EntityState.Modified;
        }

        public void DeleteComplaint(int id)
        {
            var complaint = new Complaint { ComplaintId = id };
            _context.Complaints.Attach(complaint);
            _context.Entry(complaint).State = EntityState.Deleted;
        }
    }
}
