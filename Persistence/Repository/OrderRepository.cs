﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Internal;
using PosCloudAdminPanel.Core.Model;
using PosCloudAdminPanel.Core.Repository;
using PosCloudAdminPanel.Data;

namespace PosCloudAdminPanel.Persistence.Repository
{
    public class OrderRepository : IOrderRepository
    {
        private ApplicationDbContext _context;

        public OrderRepository(ApplicationDbContext context)
        {
            _context = context;
        }
        public IEnumerable<Order> GetOrders()
        {
            return _context.Orders.Include(a=>a.Client).ToList();
        }

        public Order GetOrderById(int id)
        {
            return _context.Orders.Include(a=>a.Client).Include(a=>a.OrderDetails).FirstOrDefault(a=>a.Id ==id);
        }

        public void AddOrder(Order Order)
        {
           
                _context.Orders.Add(Order);
            

        }

        public void UpdateOrder(int id, Order Order)
        {
            if (Order.Id != id)
            {
                Order.Id = id;
            }
            else { }
            _context.Orders.Attach(Order);
            _context.Entry(Order).State = EntityState.Modified;
        }

        public void ChangeStatus(int id, bool status)
        {
            var Order = _context.Orders.Find(id);
            Order.IsActive = status;
            _context.Orders.Attach(Order);
            _context.Entry(Order).State = EntityState.Modified;
        }
        
    }
}
