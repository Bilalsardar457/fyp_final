﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Internal;
using PosCloudAdminPanel.Core.Model;
using PosCloudAdminPanel.Core.Repository;
using PosCloudAdminPanel.Data;

namespace PosCloudAdminPanel.Persistence.Repository
{
    public class ClientAppKeyRepository:IClientAppKeyRepository
    {
        private ApplicationDbContext _context;

        public ClientAppKeyRepository(ApplicationDbContext context)
        {
            _context = context;
        }
        public IEnumerable<ClientAppKey> GetAllClientAppKeys()
        {
            return _context.ClientAppKeys.Include(a=>a.Client).ToList();
        }
        public IEnumerable<ClientAppKey> GetClientAppKeys(Guid clientId)
        {
            return _context.ClientAppKeys.Include(a => a.Client).Where(a=>a.ClientId==clientId).ToList();
        }

        public ClientAppKey GetClientAppKeyById(Guid appkey, string imei, Guid clientid)
        {
            return _context.ClientAppKeys.Find(appkey, imei, clientid);
        }

        public void AddClientAppKey(ClientAppKey clientAppKey)
        {
            if (!_context.ClientAppKeys.Where(a => a.AppKey == clientAppKey.AppKey && a.IMEI == clientAppKey.IMEI && a.ClientId == clientAppKey.ClientId).Any())
            {
                _context.ClientAppKeys.Add(clientAppKey);
            }

        }

        public void UpdateClientAppKey(Guid appkey, string imei, Guid clientid, ClientAppKey clientAppKey)
        {
            if (clientAppKey.AppKey != appkey)
            {
                clientAppKey.AppKey = appkey;
            }
            else { }
            if (clientAppKey.IMEI != imei)
            {
                clientAppKey.IMEI = imei;
            }
            else { }
            if (clientAppKey.ClientId != clientid)
            {
                clientAppKey.ClientId = clientid;
            }
            else { }
            
            _context.ClientAppKeys.Attach(clientAppKey);
            _context.Entry(clientAppKey).State = EntityState.Modified;
        }

        public void DeleteClientAppKey(Guid appkey, string imei, Guid clientid)
        {
            var clientAppKey = new ClientAppKey { AppKey = appkey, IMEI = imei, ClientId = clientid };
            _context.ClientAppKeys.Attach(clientAppKey);
            _context.Entry(clientAppKey).State = EntityState.Deleted;
        }
    }
}
