﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Internal;
using PosCloudAdminPanel.Core.Model;
using PosCloudAdminPanel.Core.Repository;
using PosCloudAdminPanel.Data;

namespace PosCloudAdminPanel.Persistence.Repository
{
    public class InvoiceRepository : IInvoiceRepository
    {
        private ApplicationDbContext _context;

        public InvoiceRepository(ApplicationDbContext context)
        {
            _context = context;
        }
        public IEnumerable<Invoice> GetInvoices()
        {
            return _context.Invoices.ToList();
        }

        public Invoice GetInvoiceById(int id)
        {
            return _context.Invoices.Include(a=>a.Order).FirstOrDefault(a=>a.Id == id);
        }

        public void AddInvoice(Invoice Invoice)
        {
            
                   _context.Invoices.Add(Invoice);
            

        }

        public void UpdateInvoice(int id, Invoice Invoice)
        {
            if (Invoice.Id != id)
            {
                Invoice.Id = id;
            }
            else { }
            _context.Invoices.Attach(Invoice);
            _context.Entry(Invoice).State = EntityState.Modified;
        }

        public void ChangeStatus(int id, bool status)
        {
            var Invoice = _context.Invoices.Find(id);
            Invoice.IsActive = status;
            _context.Invoices.Attach(Invoice);
            _context.Entry(Invoice).State = EntityState.Modified;
        }
    }
}
