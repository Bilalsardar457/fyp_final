﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using PosCloudAdminPanel.Core.Model;
using PosCloudAdminPanel.Core.Repository;
using PosCloudAdminPanel.Data;

namespace PosCloudAdminPanel.Persistence.Repository
{
    public class PackageRepository : IPackageRepository
    {
        private ApplicationDbContext _context;

        public PackageRepository(ApplicationDbContext context)
        {
            _context = context;
        }
        public IEnumerable<Package> GetPackages()
        {
            return _context.Packages.ToList();
        }

        public Package GetPackageById(int id)
        {
            return _context.Packages.Find(id);
        }

        public Package GetPackageByClientId(Guid id)
        {
            return (from client in _context.Clients join package in _context.Packages on client.PackageId equals package.Id
                    where client.ClientId==id
                          select package).FirstOrDefault();
        }

       
        public void AddPackage(Package Package)
        {
            if (!_context.Packages.Where(a => a.Name == Package.Name).Any())
            {
                _context.Packages.Add(Package);
            }

        }

        public void UpdatePackage(int id, Package Package)
        {
            if (Package.Id != id)
            {
                Package.Id = id;
            }
            else { }
            _context.Packages.Attach(Package);
            _context.Entry(Package).State = EntityState.Modified;
        }

        public void ChangeStatus(int id, bool status)
        {
            var Package = _context.Packages.Find(id);
            Package.IsActive = status;
            _context.Packages.Attach(Package);
            _context.Entry(Package).State = EntityState.Modified;
        }
        public void DeletePackage(int id)
        {
            var package = new Package { Id = id };
            _context.Packages.Attach(package);
            _context.Entry(package).State = EntityState.Deleted;
        }
    }
}
