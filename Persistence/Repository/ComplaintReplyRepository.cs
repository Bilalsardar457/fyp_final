﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using PosCloudAdminPanel.Core.Model;
using PosCloudAdminPanel.Core.Repository;
using PosCloudAdminPanel.Data;

namespace PosCloudAdminPanel.Persistence.Repository
{
    public class ComplaintReplyRepository:IComplaintReplyRepository
    {
        private ApplicationDbContext _context;

        public ComplaintReplyRepository(ApplicationDbContext context)
        {
            _context = context;
        }
        public IEnumerable<ComplaintReply> GetComplaintReplies()
        {
            return _context.ComplaintReplies.ToList();
        }

        public ComplaintReply GetComplaintReplyById(int id)
        {
            //return _context.ComplaintReplies.Find(id);
            var data = _context.ComplaintReplies.Include(a => a.ApplicationUser).Include(a=>a.Complaint).FirstOrDefault(a => a.ComplaintId == id);
            return data;
        }

        public void AddComplaintReply(ComplaintReply complaintReply)
        {

            _context.ComplaintReplies.Add(complaintReply);

        }

        public void UpdateComplaintReply(int id, ComplaintReply complaintReply)
        {
            if (complaintReply.Id != id)
            {
                complaintReply.Id = id;
            }
            else { }
            _context.ComplaintReplies.Attach(complaintReply);
            _context.Entry(complaintReply).State = EntityState.Modified;
        }

        public void DeleteComplaintReply(int id)
        {
            var complaintReply = new ComplaintReply { Id = id };
            _context.ComplaintReplies.Attach(complaintReply);
            _context.Entry(complaintReply).State = EntityState.Deleted;
        }
    }
}
