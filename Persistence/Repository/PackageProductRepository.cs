﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Internal;
using PosCloudAdminPanel.Core.Model;
using PosCloudAdminPanel.Core.Repository;
using PosCloudAdminPanel.Data;

namespace PosCloudAdminPanel.Persistence.Repository
{
    public class PackageProductRepository : IPackageProductRepository
    {
        private ApplicationDbContext _context;

        public PackageProductRepository(ApplicationDbContext context)
        {
            _context = context;
        }
        public IEnumerable<PackageProduct> GetPackageProducts()
        {
            return _context.PackageProducts.Include(a=>a.Package).Include(a=>a.Product).ToList();
        }

        public PackageProduct GetPackageProductById(int id)
        {
            return _context.PackageProducts.Include(a=>a.Package).Include(a=>a.Product).FirstOrDefault(a=>a.Id==id);
        }

        public void AddPackageProduct(PackageProduct PackageProduct)
        {
            
                _context.PackageProducts.Add(PackageProduct);
            
            


        }
        public IEnumerable<PackageProduct> GetPackageProductsByPkgId(int pkgId)
        {
            return _context.PackageProducts.Include(a => a.Product).Where(a => a.PackageId == pkgId && a.IsActive);
        }
        public IEnumerable<PackageProduct> GetPackageProductsByClientId(Guid clientId)
        {
            int pkgId = _context.Clients.Find(clientId).PackageId;
            return _context.PackageProducts.Include(a=>a.Product).Where(a => a.PackageId == pkgId && a.IsActive);
        }
        public void UpdatePackageProduct(int id, PackageProduct PackageProduct)
        {
            if (PackageProduct.Id != id)
            {
                PackageProduct.Id = id;
            }
            else { }
            _context.PackageProducts.Attach(PackageProduct);
            _context.Entry(PackageProduct).State = EntityState.Modified;
        }

        public void ChangeStatus(int id, bool status)
        {
            var PackageProduct = _context.PackageProducts.Find(id);
            PackageProduct.IsActive = status;
            _context.PackageProducts.Attach(PackageProduct);
            _context.Entry(PackageProduct).State = EntityState.Modified;
        }
        public void DeletePackageProduct(int id)
        {
            var packageProduct = new PackageProduct { Id = id };
            _context.PackageProducts.Attach(packageProduct);
            _context.Entry(packageProduct).State = EntityState.Deleted;
        }
    }
}
