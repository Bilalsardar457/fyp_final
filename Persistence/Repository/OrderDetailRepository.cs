﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using PosCloudAdminPanel.Core.Model;
using PosCloudAdminPanel.Core.Repository;
using PosCloudAdminPanel.Data;

namespace PosCloudAdminPanel.Persistence.Repository
{
    public class OrderDetailRepository : IOrderDetailRepository
    {
        private ApplicationDbContext _context;

        public OrderDetailRepository(ApplicationDbContext context)
        {
            _context = context;
        }
        public IEnumerable<OrderDetail> GetOrderDetails()
        {
            return _context.OrderDetails.ToList();
        }

        public OrderDetail GetOrderDetailById(int id)
        {
            return _context.OrderDetails.Find(id);
        }

        public void AddOrderDetail(OrderDetail OrderDetail)
        {

            _context.OrderDetails.Add(OrderDetail);


        }

        public void UpdateOrderDetail(int id, OrderDetail OrderDetail)
        {
            if (OrderDetail.Id != id)
            {
                OrderDetail.Id = id;
            }
            else { }
            _context.OrderDetails.Attach(OrderDetail);
            _context.Entry(OrderDetail).State = EntityState.Modified;
        }

        public void ChangeStatus(int id, bool status)
        {
            var OrderDetail = _context.OrderDetails.Find(id);
            OrderDetail.IsActive = status;
            _context.OrderDetails.Attach(OrderDetail);
            _context.Entry(OrderDetail).State = EntityState.Modified;
        }
    }
}
