﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Internal;
using PosCloudAdminPanel.Core.Model;
using PosCloudAdminPanel.Core.Repository;
using PosCloudAdminPanel.Data;

namespace PosCloudAdminPanel.Persistence.Repository
{
    public class ProductRepository : IProductRepository
    {
        private ApplicationDbContext _context;

        public ProductRepository(ApplicationDbContext context)
        {
            _context = context;
        }
        public IEnumerable<Product> GetProducts()
        {
            return _context.Products.ToList();
        }

        public Product GetProductById(int id)
        {
            return _context.Products.Find(id);
        }

        public void AddProduct(Product Product)
        {
            if (!_context.Products.Where(a => a.Name == Product.Name).Any())
            {
                _context.Products.Add(Product);
            }

        }

        public void UpdateProduct(int id, Product Product)
        {
            if (Product.Id != id)
            {
                Product.Id = id;
            }
            else { }
            _context.Products.Attach(Product);
            _context.Entry(Product).State = EntityState.Modified;
        }

        public void ChangeStatus(int id, bool status)
        {
            var Product = _context.Products.Find(id);
            Product.IsActive = status;
            _context.Products.Attach(Product);
            _context.Entry(Product).State = EntityState.Modified;
        }
        public void DeleteProduct(int id)
        {
            var product = new Product { Id = id };
            _context.Products.Attach(product);
            _context.Entry(product).State = EntityState.Deleted;
        }
    }

}
