﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Rewrite.Internal.UrlActions;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.Web.CodeGeneration.Contracts.Messaging;
using Newtonsoft.Json.Serialization;
using PosCloudAdminPanel.Core.Model;
using PosCloudAdminPanel.Core.Repository;
using PosCloudAdminPanel.Data;

namespace PosCloudAdminPanel.Persistence.Repository
{
    public class ClientRepository : IClientRepository
    {
        private ApplicationDbContext _context;

        public ClientRepository(ApplicationDbContext context)
        {
            _context = context;
        }
        public IEnumerable<Client> GetClients()
        {
            return _context.Clients.Include(a=>a.Package).ToList();
        }
        public async Task<IEnumerable<Client>> GetClientsAsync()
        {
            return await _context.Clients.Include(a => a.Package).ToListAsync();
        }
        public Client GetClientById(Guid id)
        {
            return _context.Clients.Include(a=>a.Package).FirstOrDefault(a=>a.ClientId==id);
        }
        public async Task<Client> GetClientByIdAsync(Guid id)
        {
            return await _context.Clients.Include(a => a.Package).FirstOrDefaultAsync(a => a.ClientId == id);
        }
        public void AddClient(Client Client)
        {
            if (!_context.Clients.Where(a => a.Email == Client.Email).Any())
            {
                _context.Clients.Add(Client);
            }

        }
        public async Task AddClientAsync(Client Client)
        {
            if (!await _context.Clients.Where(a => a.Email == Client.Email).AnyAsync())
            {
                await _context.Clients.AddAsync(Client);
            }

        }
        public void UpdateClient(Guid id, Client Client)
        {
            if (Client.ClientId != id)
            {
                Client.ClientId = id;
            }
            else { }
            _context.Clients.Attach(Client);
            _context.Entry(Client).State = EntityState.Modified;
        }
        public async Task UpdateClientAsync(Guid id, Client Client)
        {
            if (Client.ClientId != id)
            {
                Client.ClientId = id;
            }
            else { }
            _context.Clients.Attach(Client);
            _context.Entry(Client).State = EntityState.Modified;
        }
        public void ChangeStatus(Guid id, bool status)
        {
            var Client = _context.Clients.Find(id);
            Client.IsActive = status;
            _context.Clients.Attach(Client);
            _context.Entry(Client).State = EntityState.Modified;
        }
    }
}
