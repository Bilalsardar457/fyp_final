﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using PosCloudAdminPanel.Core;
using PosCloudAdminPanel.Core.Repository;
using PosCloudAdminPanel.Data;
using PosCloudAdminPanel.Persistence.Repository;

namespace PosCloudAdminPanel.Persistence
{
    public class UnitOfWork:IUnitOfWork
    {
        private readonly ApplicationDbContext _context;
        public IClientRepository ClientRepository { get; private set; }
        public IClientAppKeyRepository ClientAppKeyRepository { get; private set; }
        public IComplaintRepository ComplaintRepository { get; private set; }
        public IComplaintReplyRepository ComplaintReplyRepository { get; private set; }
        public IProductRepository ProductRepository { get; private set; }
        public IPackageRepository PackageRepository { get; private set; }
        public IPackageProductRepository PackageProductRepository { get; private set; }
        public IOrderRepository OrderRepository { get; private set; }
        public IOrderDetailRepository OrderDetailRepository { get; private set; }
        public IInvoiceRepository InvoiceRepository { get; private set; }
        public UnitOfWork(ApplicationDbContext context)
        {
            _context = context;
            ClientRepository = new ClientRepository(context);
            ClientAppKeyRepository = new ClientAppKeyRepository(context);
            ComplaintRepository = new ComplaintRepository(context);
            ComplaintReplyRepository = new ComplaintReplyRepository(context);
            ProductRepository = new ProductRepository(context);
            PackageRepository = new PackageRepository(context);
            OrderRepository = new OrderRepository(context);
            InvoiceRepository = new InvoiceRepository(context);
            PackageProductRepository=new PackageProductRepository(context);
        }
        public void Complete()
        {
            _context.SaveChanges();

        }
    }
}
