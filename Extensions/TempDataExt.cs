﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using Newtonsoft.Json;
using PosCloudAdminPanel.Core.Model;

namespace PosCloudAdminPanel.Extensions
{
    public static class TempDataExt
    {
            public static void Put<T>(this ITempDataDictionary tempData, string key, T value, JsonSerializerSettings jsonSettings = null) where T : class
            {
                tempData[key] = JsonConvert.SerializeObject(value);
        }

            public static T Get<T>(this ITempDataDictionary tempData, string key, JsonSerializerSettings jsonSettings = null) where T : class
            {
                object o;
                tempData.TryGetValue(key, out o);
                return o == null ? null : JsonConvert.DeserializeObject<T>(tempData[key].ToString());
        }
    }
}
