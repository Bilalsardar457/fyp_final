#pragma checksum "E:\Projects\PosCloudAdmin\PosCloudAdminPanel\PosCloudAdminPanel\Views\Home\Index.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "fcac3bdb24b2ec1057ca3af43cf9a9de2f14ea01"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Home_Index), @"mvc.1.0.view", @"/Views/Home/Index.cshtml")]
[assembly:global::Microsoft.AspNetCore.Mvc.Razor.Compilation.RazorViewAttribute(@"/Views/Home/Index.cshtml", typeof(AspNetCore.Views_Home_Index))]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#line 1 "E:\Projects\PosCloudAdmin\PosCloudAdminPanel\PosCloudAdminPanel\Views\_ViewImports.cshtml"
using PosCloudAdminPanel;

#line default
#line hidden
#line 2 "E:\Projects\PosCloudAdmin\PosCloudAdminPanel\PosCloudAdminPanel\Views\_ViewImports.cshtml"
using PosCloudAdminPanel.Models;

#line default
#line hidden
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"fcac3bdb24b2ec1057ca3af43cf9a9de2f14ea01", @"/Views/Home/Index.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"3ef0d6934b8b7db14db5887cfa9f54cd3de07214", @"/Views/_ViewImports.cshtml")]
    public class Views_Home_Index : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<dynamic>
    {
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            BeginContext(0, 2, true);
            WriteLiteral("\r\n");
            EndContext();
#line 2 "E:\Projects\PosCloudAdmin\PosCloudAdminPanel\PosCloudAdminPanel\Views\Home\Index.cshtml"
  
    ViewData["Title"] = "Index";
    Layout = "~/Views/Shared/_Layout.cshtml";

#line default
#line hidden
            BeginContext(90, 30176, true);
            WriteLiteral(@"


    <a id=""main-slider"" class=""in-page-link""></a>
    <section class=""switchable space--sm"">
        <div class=""container"">
            <div class=""row"">
                <div class=""col-md-5 col-sm-7"">
                    <div class=""mt--2"">
                        
                            <h2><span class=""color--primary"">All-in-one Restaurant Management System for both</span><br><span class=""color--primary"">  Apple iPad & Android Tablet</span></h2>
                            <p class=""lead""><span style=""color: #808080;"">Designed to hike revenues and track everything inside your business, with customisable features that cater to the requirements of your restaurant.</span></p>
                        <a class=""btn btn--primary type--uppercase inner-link"" href=""signup.html"" target="""" title="""" rel=""""><span class=""btn__text"">sign up today<br></span></a>
                        
                            <!-- <p><span class=""block type--fine-print"">or <a href=""#"" target=""_blank"" rel=""noope");
            WriteLiteral(@"ner"">view cosmic video</a></span></p> -->
                        
                    </div>
                </div>
                <div class=""col-md-7 col-sm-5 col-xs-12""><img alt=""image of cosmic pos system homepage"" src=""/site/images/banner1.png""></div>
            </div>
     
        </div>
  
    </section>
<!--     <a id=""testimonials"" class=""in-page-link""></a>
    <section class=""space--xs"">
        <div class=""container"">
            <div class=""row"">
                <div class=""logo-carousel slider slider--inline-arrows slider--arrows-hover text-center"" data-timing=""2000"">
                    <ul class=""slides"">
                        <li class=""col-sm-3 col-xs-6""> <img alt=""Wayback Burgers"" class=""image--xxs"" src=""/site/uploads/2017/11/wayback.png""> </li>
                        <li class=""col-sm-3 col-xs-6""> <img alt=""image of hashem logo"" class=""image--xxs"" src=""/site/uploads/2018/10/hashem-logo.png""> </li>
                        <li class=""col-sm-3 col-xs-6""> <img alt=""imag");
            WriteLiteral(@"e of firegrill logo"" class=""image--xxs"" src=""/site/uploads/2018/10/firegrill-logo.png""> </li>
                        <li class=""col-sm-3 col-xs-6""> <img alt=""image of burgerizzr logo"" class=""image--xxs"" src=""/site/uploads/2018/10/burgerzzr-logo.png""> </li>
                        <li class=""col-sm-3 col-xs-6""> <img alt=""image of freshii logo"" class=""image--xxs"" src=""/site/uploads/2018/10/freshii-logo.png""> </li>
                        <li class=""col-sm-3 col-xs-6""> <img alt=""image of shawerma king logo"" class=""image--xxs"" src=""/site/uploads/2018/10/shawerma-king-logo.png""> </li>
                        <li class=""col-sm-3 col-xs-6""> <img alt=""image of firegrill logo"" class=""image--xxs"" src=""/site/uploads/2018/10/firegrill-logo.png""> </li>
                        <li class=""col-sm-3 col-xs-6""> <img alt=""image of caffe vergnano 1882 logo"" class=""image--xxs"" src=""/site/uploads/2018/10/caffe-vergnano-1882-logo.png""> </li>
                    </ul>
                </div>
            </div>
   
        ");
            WriteLiteral(@"</div> -->
      
    <!-- </section> -->
    <section class=""bg-im"" style=""background-image:url(/site/images/banners/banner-14.png); background-size:100% 100%; min-height:360px"">
        <div class=""container"">
            <div class=""row"">
               
                <div class=""col-md-4 col-sm-12""><br><br>
                    <h2 style=""color:white;font-weight:500;margin-bottom: 0.2em;"">Grow Your Business With Us</h2>
					<p style=""color:white""> Refer retailers that are a good fit for Cosmic POS and activate at least 5 new accounts per year. Help retailers scale and grow, while boosting your revenue streams. </p>
					<a class=""btn"" href=""#"">
					<span class=""btn__text"">READ MORE</span>
					</a>
					</div>
            </div>
			
			   <!-- <div class=""row""> -->
                <!-- <div class=""col-md-6 col-sm-12""> -->
                    <!-- <h2>Announcing</h2> -->
                    <!-- <p><img class=""alignnone"" src=""../lamarka.com/nexov8_img/logo_nexov81.png"" width=""521"" hei");
            WriteLiteral(@"ght=""120""></p> -->
                    <!-- <p><span style=""color: #2da8e0;"">November 27th , 2018  -  Four Seasons Hotel, Riyadh</span></p> -->
                <!-- </div> -->
                <!-- <div class=""col-md-6 col-sm-12""><br><br> -->
                    <!-- <h3>Join us at NexoV8. A one day event in Riyadh that will focus on the Food Tech of the future</h3><a class=""btn"" href=""nexov8/index.html""><span class=""btn__text"">READ MORE</span></a></div> -->
            <!-- </div> -->
        
        </div>
      
    </section>
    <a id=""guy-with-cashier"" class=""in-page-link""></a>
    <section class=""switchable feature-large feature-large-13  bg--secondary"">
        <div class=""container"">
            <div class=""row"">
                <div class=""col-sm-6""> <img alt=""image of a guy using cosmic cloud point of sale"" class=""border--round box-shadow-wide"" src=""/site/images/banners/cos1.png""> </div>
                <div class=""col-sm-6 col-md-5"">
                    <div class=""switchable__text");
            WriteLiteral(@""">
                        
                            <h2 class=""color--primary"">Point of Sale in the Cloud</h2>
                            <p class=""lead"">cosmic is smart and reliable and can be used by all through its cloud technology. Manage stocks, schedule staff, increase table turnaround time, get intelligence reports, and much more.</p>
                        
                        <div class=""text-block"">
                            <h5>Over 4000 cosmic Customers</h5>
                            <p>Partnering with food outlets across the Middle East, cosmic is revolutionising the F&amp;B scene.</p>
                        </div>
                        <div class=""text-block"">
                            <h5>Over 1 Billion Orders</h5>
                            <p>Processing over 5 million transactions a minute and solidifying businesses in an ever-competitive environment.</p>
                        </div>
                        <div class=""text-block""> </div>
                  ");
            WriteLiteral(@"  </div>
                </div>
            </div>
       
        </div>
    
    </section>
    <a id=""business-types"" class=""in-page-link""></a>
    <section class=""text-center"">
        <div class=""container"">
            <div class=""row"">
                <div class=""col-sm-12"">
                    <h1><span class=""color--primary"">Food Outlets</span></h1>
                    <p class=""lead"" style=""text-align: center;"">The icing on the cake is that all types of food outlets, no matter the size or model, <br>can easily adopt cosmic and its smart apps.</p>
                </div>
            </div>
   
        </div>
    
    </section>
    <a id=""business-types-icons"" class=""in-page-link""></a>
    <section class=""text-center  space--sm"">
        <div class=""container"">
            <div class=""row"">
			   <div class=""col-sm-4"">
                    <div class=""feature feature-3 boxed boxed--lg boxed--border"">
                        
                            <h4><img class=""wp-imag");
            WriteLiteral(@"e-236 aligncenter"" src=""/site/images/icons/franchise.png"" alt=""image of franchise icon"" width=""150"" height=""100""></h4>
                            <h4 class=""color--primary"">Franchise</h4>
                            <p>Know the ins and outs of every branch from any location</p><a href=""#""> Learn More </a>
                    </div>
                 
                </div>
           
                <div class=""col-sm-4"">
                    <div class=""feature feature-3 boxed boxed--lg boxed--border"">
                        
                            <h4><img class=""size-full wp-image-541 aligncenter"" src=""/site/images/icons/quick-service.png"" alt=""image of quick service restaurant icon"" width=""150"" height=""100""></h4>
                            <h4 class=""color--primary"">Quick Service Restaurant</h4>
                            <p>Improve your speed of service and transform customer experience.</p><a href=""#""> Learn More </a>
                    </div>
                  
                <");
            WriteLiteral(@"/div>
                <div class=""col-sm-4"">
                    <div class=""feature feature-3 boxed boxed--lg boxed--border"">
                        
                            <h4><img class=""aligncenter wp-image-235"" src=""/site/images/icons/coffee-shop.png"" alt=""image of coffee shop icon"" width=""150"" height=""100""></h4>
                            <h4 class=""color--primary"">Coffee Shop</h4>
                            <p>Whether it’s to go or not you’ll keep customers smiling with every visit</p><a href=""#""> Learn More </a>
                    </div>
                 
                </div>
				   <div class=""col-sm-4"">
                    <div class=""feature feature-3 boxed boxed--lg boxed--border"">
                        
                            <h4><img class=""size-full wp-image-528 aligncenter"" src=""/site/images/icons/food-court.png"" alt=""image of food court icon"" width=""150"" height=""100""></h4>
                            <h4 class=""color--primary"">Food Court</h4>
                 ");
            WriteLiteral(@"           <p>Stand out amongst the rest by providing a simple and fast service</p><a href=""#""> Learn More </a>
                    </div>
                   
                </div>
				  <div class=""col-sm-4"">
                    <div class=""feature feature-3 boxed boxed--lg boxed--border"">
                        
                            <h4><img class=""size-full wp-image-532 aligncenter"" src=""/site/images/icons/cup-cake.png"" alt=""image of cup cake icon"" width=""150"" height=""100""></h4>
                            <h4 class=""color--primary"">Bakery</h4>
                            <p>Whether it’s croissants or rainbow bagels you’ll be spreading the joy</p><a href=""#""> Learn More </a>
                    </div>
                
                </div>
                <div class=""col-sm-4"">
                    <div class=""feature feature-3 boxed boxed--lg boxed--border"">
                        
                            <h4><img class=""size-full wp-image-531 aligncenter"" src=""/site/images/i");
            WriteLiteral(@"cons/pizzeria.png"" alt=""image of pizzeria icon"" width=""150"" height=""100""></h4>
                            <h4 class=""color--primary"">Pizzeria</h4>
                            <p>Slice away your worries and add some cosmic toppings</p><a href=""#""> Learn More </a>
                    </div>
                
                </div>
				     <div class=""col-sm-4"">
                    <div class=""feature feature-3 boxed boxed--lg boxed--border"">
                        
                            <h4><img class=""aligncenter wp-image-231"" src=""/site/images/icons/food-truck.png"" alt=""image of food truck icon"" width=""150"" height=""100""></h4>
                            <h4 class=""color--primary"">Food Truck</h4>
                            <p>Be the favourite bite for the on-the-road customer to keep growing your business</p><a href=""#""> Learn More </a>
                    </div>
                  
                </div>
              
                <div class=""col-sm-4"">
                    <div cl");
            WriteLiteral(@"ass=""feature feature-3 boxed boxed--lg boxed--border"">
                        
                            <h4><img class=""size-full wp-image-530 aligncenter"" src=""/site/images/icons/drive-thru.png"" alt=""image of drive thru icon"" width=""150"" height=""100""></h4>
                            <h4 class=""color--primary"">Drive-Thru</h4>
                            <p>Let every window lead to another smoothly and swiftly</p><a href=""#""> Learn More </a>
                    </div>
                 
                </div>
               
             
				 <div class=""col-sm-4"">
                    <div class=""feature feature-3 boxed boxed--lg boxed--border"">
                        
                            <h4><img class=""size-full wp-image-529 aligncenter"" src=""/site/images/icons/saloon.png"" alt=""image of Saloon icon"" width=""150"" height=""100""></h4>
                            <h4 class=""color--primary"">Saloon</h4>
                            <p>Text Pending Text Pending Text Pending Text Pending </");
            WriteLiteral(@"p><a href=""#""> Learn More </a>
                    </div>
                   
                </div>
             
            </div>
            
        </div>
      
    </section>
    <a id=""apps-title"" class=""in-page-link""></a>
    <section class=""text-center"">
        <div class=""container"">
            <div class=""row"">
                <div class=""col-sm-10 col-md-8"">
                    <h1><span class=""color--primary"">Apps</span></h1>
                    <p class=""lead""><span style=""color: #808080;"">Every cosmic app is built to meet the needs of today’s restaurateurs</span></p>
                </div>
            </div>
      
        </div>
 
    </section>
    <a id=""apps-i"" class=""in-page-link""></a>
    <section class=""text-center  space--sm"">
        <div class=""container"">
            <div class=""row"">
                <div class=""col-sm-6 col-md-3"">
                    <div class=""text-block"">
                        
                            <h4><img class=""wp-im");
            WriteLiteral(@"age-273 aligncenter"" src=""/site/images/icons/pos.png"" alt=""image of point of sale icon"" ></h4>
                            <h4 class=""color--primary"">Point-of-Sale</h4>
                            <p>Install and update easily, and automatically syncs offline data<br><br><a href=""#"">Learn More</a></p>
                        
                    </div>
               
                </div>
                <div class=""col-sm-6 col-md-3"">
                    <div class=""text-block"">
                        
                            <h4><img class=""wp-image-233 aligncenter"" src=""/site/images/icons/timesheet.png"" alt=""image of employee time sheet icon"" ></h4>
                            <h4 class=""color--primary"">Employee Timesheet</h4>
                            <p>Improve staff productivity and reduce customer complaints<br><br><a href=""#"">Learn More</a></p>
                        
                    </div>
                  
                </div>
                <div class=""col-sm-6 col");
            WriteLiteral(@"-md-3"">
                    <div class=""text-block"">
                        
                            <h4><img class=""wp-image-278 aligncenter"" src=""/site/images/icons/waiter.png"" alt=""image of waiter digital menu icon"" ></h4>
                            <h4 class=""color--primary"">Waiter Digital Menu</h4>
                            <p>Go green and display a vivid and engaging menu list<br><br><a href=""#"">Learn More</a></p>
                        
                    </div>
            
                </div>
                <div class=""col-sm-6 col-md-3"">
                    <div class=""text-block"">
                        
                            <h4><img class=""wp-image-237 aligncenter"" src=""/site/images/icons/inventory.png"" alt=""image of inventory management icon"" ></h4>
                            <h4 class=""color--primary"">Inventory Management</h4>
                            <p>Monitor inventory levels and never lose track of your stock<br><br><a href=""#"">Learn More</a></p>
   ");
            WriteLiteral(@"                     
                    </div>
              
                </div>
            </div>
      
        </div>
     
    </section>
    <a id=""apps-ii"" class=""in-page-link""></a>
    <section class=""text-center"">
        <div class=""container"">
            <div class=""row"">
                <div class=""col-sm-6 col-md-3"">
                    <div class=""text-block"">
                        
                            <h4><img class=""wp-image-277 aligncenter"" src=""/site/images/icons/call.png"" alt=""image of call center management icon"" ></h4>
                            <h4 class=""color--primary"">Call Center Management</h4>
                            <p>Increase sales and store valuable customer information<br><br><a href=""#"">Learn More</a></p>
                        
                    </div>
             
                </div>
                <div class=""col-sm-6 col-md-3"">
                    <div class=""text-block"">
                        
                     ");
            WriteLiteral(@"       <h4><img class=""wp-image-239 aligncenter"" src=""/site/images/icons/kitchen.png"" alt=""image of kitchen display system icon"" ></h4>
                            <h4 class=""color--primary"">Kitchen Display System</h4>
                            <p><span style=""font-weight: 400;"">Avoid order mistakes and delays and increase efficiency in the kitchen</span><br><br><a href=""#"">Learn More</a></p>
                        
                    </div>
                
                </div>
                <div class=""col-sm-6 col-md-3"">
                    <div class=""text-block"">
                        
                            <h4><img class=""wp-image-240 aligncenter"" src=""/site/images/icons/table.png"" alt=""image of table management icon"" ></h4>
                            <h4 class=""color--primary"">Table Management</h4>
                            <p><span style=""font-weight: 400;"">Increase table turnaround time and customer experience<br><br><a href=""#"">Learn More</a><br></span></p>
          ");
            WriteLiteral(@"              
                    </div>
             
                </div>
                <div class=""col-sm-6 col-md-3"">
                    <div class=""text-block"">
                        
                            <h4><img class=""wp-image-241 aligncenter"" src=""/site/images/icons/report.png"" alt=""image of smart analytics and reports icon"" ></h4>
                            <h4 class=""color--primary"">Smart Analytics and Reports</h4>
                            <p><span style=""font-weight: 400;"">Make the most of Big Data with<br>real-time analytics and reports<br><br><a href=""smart-analytics-reporting/index.html"">Learn More</a> </span></p>
                        
                    </div>
                
                </div>
            </div>
     
        </div>
       
    </section>
    <a id=""tip-bar"" class=""in-page-link""></a>
    <section class=""text-center cta cta-4 border--bottom  space--xxs bg--dark"">
        <div class=""container"">
            <div class=""row"">
  ");
            WriteLiteral(@"              <div class=""col-sm-12""> <span class=""label label--inline"">TIP</span> <span>78% of restaurateurs&nbsp;check their sales data and metrics daily. Discover cosmic intelligence reports today.</span> </div>
            </div>
     
        </div>
  
    </section>
    <a id=""key-benefits"" class=""in-page-link""></a>
    <section class=""text-center bg--secondary"">
        <div class=""tabs-container"" data-content-align=""left"">
            <ul class=""tabs tabs--spaced"">
                <li class=""active"">
                    <div class=""tab__title text-center""> <i class=""icon block icon-Cloud-Secure icon--sm""></i> <span class=""h5""><p>Secure Cloud</p></span> </div>
                    <div class=""tab__content"">
                        <div class=""container switchable switchable--switch"">
                            <div class=""row"">
                                <div class=""col-sm-7""> <img alt=""image of regular cosmic system updates"" src=""/site/uploads/2018/10/regular-cosmic-system-updates.");
            WriteLiteral(@"jpg""> </div>
                                <div class=""col-sm-5 col-md-4"">
                                    <div class=""mt--1"">
                                        <h3 class=""color--primary"">Cloud You Can Trust</h3>
                                        <p class=""lead""><span style=""font-weight: 400;"">Simple to set up, easy to update, and no software to maintain. </span></p>
                                        <p class=""lead""><span style=""font-weight: 400;"">The cosmic team manages it behind the scenes and makes sure you are protected all the time.</span></p>
                                        <p>&nbsp;</p>
                                    </div>
                                </div>
                            </div>
                     
                        </div>
                  
                    </div>
                </li>
                <li class=""active"">
                    <div class=""tab__title text-center""> <i class=""icon block material-icons icon--sm");
            WriteLiteral(@""">signal_wifi_off</i> <span class=""h5""><p>Offline Mode<br></p></span> </div>
                    <div class=""tab__content"">
                        <div class=""container switchable switchable--switch"">
                            <div class=""row"">
                                <div class=""col-sm-7""> <img alt="""" src=""/site/uploads/2018/03/home-offline-mode.jpg""> </div>
                                <div class=""col-sm-5 col-md-4"">
                                    <div class=""mt--1"">
                                        <h3 class=""color--primary"">No WiFi, No Problem</h3>
                                        <p class=""lead"">Don't worry if there is a glitch in your WiFi or you're in a remote location with no internet access.</p>
                                        <p class=""lead"">cosmic will keep powering through, connecting all the data and syncing it once you're back online.</p>
                                    </div>
                                </div>
                        ");
            WriteLiteral(@"    </div>
                         
                        </div>
                     
                    </div>
                </li>
                <li class=""active"">
                    <div class=""tab__title text-center""> <i class=""icon block icon-Download-2 icon--sm""></i> <span class=""h5""><p>Regular Updates</p></span> </div>
                    <div class=""tab__content"">
                        <div class=""container switchable switchable--switch"">
                            <div class=""row"">
                                <div class=""col-sm-7""> <img alt="""" src=""/site/uploads/2018/10/627.jpg""> </div>
                                <div class=""col-sm-5 col-md-4"">
                                    <div class=""mt--1"">
                                        <h3 class=""color--primary"">Regular System Updates</h3>
                                        <p class=""lead""><span>Our updates keep the system current for your experience and improved performance. </span></p>
                 ");
            WriteLiteral(@"                       <p class=""lead""><span>We listen to our clients and keep a close eye on trends so that our system works best for your business.</span></p>
                                    </div>
                                </div>
                            </div>
                       
                        </div>
                   
                    </div>
                </li>
                <li>
                    <div class=""tab__title text-center""> <i class=""icon block icon-Statistic icon--sm""></i> <span class=""h5""><p>Big Data Analysis</p></span> </div>
                    <div class=""tab__content"">
                        <div class=""container"">
                            <div class=""row"">
                                <div class=""col-sm-7""> <img alt=""image of business owner checking cosmic big data analysis feature"" src=""/site/uploads/2018/10/big-data-analysis.jpg""> </div>
                                <div class=""col-sm-5 col-md-4"">
                          ");
            WriteLiteral(@"          <div class=""mt--1"">
                                        <h3 class=""color--primary"">Uncover Patterns and Trends</h3>
                                        <p class=""lead""><span>cosmic has the ability to uncover patterns and correlations in the market trends that would be highly beneficial for your businesses. </span></p>
                                        <p class=""lead""><span>You will never have to wonder when the right time for a promotion or menu update is again.</span></p>
                                    </div>
                                </div>
                            </div>
                         
                        </div>
                       
                    </div>
                </li>
                <li>
                    <div class=""tab__title text-center""> <i class=""icon block material-icons icon--sm"">apps</i> <span class=""h5""><p>Third Party Apps</p></span> </div>
                    <div class=""tab__content"">
                        <");
            WriteLiteral(@"div class=""container"">
                            <div class=""row"">
                                <div class=""col-sm-7""> <img alt="""" src=""/site/uploads/2018/02/third-party-apps-3.jpg""> </div>
                                <div class=""col-sm-5 col-md-4"">
                                    <div class=""mt--1"">
                                        <h3 class=""color--primary"">Integrate with Any App</h3>
                                        <p class=""lead"">Take it to the next level with flexible means of integrating third party apps to help you run your business smoother and faster.</p>
                                        <p class=""lead"">cosmic integrations include online delivery services, accounting, business intelligence and much more.</p>
                                    </div>
                                </div>
                            </div>
                        
                        </div>
                       
                    </div>
                </li>
");
            WriteLiteral(@"                <li>
                    <div class=""tab__title text-center""> <i class=""icon block material-icons icon--sm"">import_export</i> <span class=""h5""><p>Data Import / Export</p></span> </div>
                    <div class=""tab__content"">
                        <div class=""container"">
                            <div class=""row"">
                                <div class=""col-sm-7""> <img alt="""" src=""/site/uploads/2018/03/data-import-home.jpg""> </div>
                                <div class=""col-sm-5 col-md-4"">
                                    <div class=""mt--1"">
                                        <h3 class=""color--primary"">Take Control of Your Data</h3>
                                        <p class=""lead""><span>All data collected by cosmic can be imported and exported into multiple data formats.&nbsp;</span></p>
                                        <p class=""lead""><span>Anyone in your team will easily understand all the important analytics and statistics of your business ");
            WriteLiteral(@"and know what to do with the information.</span></p>
                                    </div>
                                </div>
                            </div>
                        
                        </div>
                   
                    </div>
                </li>
            </ul>
        </div>
  
    </section>
    <a id=""key-benefits"" class=""in-page-link""></a>
    <section class=""switchable feature-large"">
        <div class=""container"">
            <div class=""row"">
                <div class=""col-sm-6 col-md-5"">
                    <div class="""">
                        <p><img class=""alignnone  wp-image-631"" src=""/site/images/men1.jpg"" alt=""""></p>
                    </div>
                </div>
                <div class=""col-sm-6"">
                    <div class=""boxed boxed--md boxed--border"">
                        <div class=""feature feature-2""> <i class=""icon color--primary icon-Clock-Forward""></i>
                            <div class=""fe");
            WriteLiteral(@"ature__body"">
                                <h4>24/7 Support</h4>
                                <p><span>Our team works around the clock to offer constant support whenever needed. Don’t be shy to ask for help, they’re super friendly</span></p>
                            </div>
                        </div>
                    
                        <div class=""feature feature-2""> <i class=""icon color--primary icon-Globe""></i>
                            <div class=""feature__body"">
                                <h4>Multi-Language English and Arabic</h4>
                                <p><span>Our apps support multiple languages and allow users to interact with more than one language</span></p>
                            </div>
                        </div>
                   
                        <div class=""feature feature-2""> <i class=""icon color--primary icon-Teacher""></i>
                            <div class=""feature__body"">
                                <h4>Customer Trai");
            WriteLiteral(@"ning</h4>
                                <p><span>We offer one-to-one training for our customers with our own professionally trained experts</span></p>
                            </div>
                        </div>
                  
                    </div>
                </div>
            </div>
         
        </div>
      
    </section>
    <a id=""get-started-with-cosmic"" class=""in-page-link""></a>
    <section class=""switchable text-right bg--secondary switchable--switch"">
        <div class=""container"">
            <div class=""row"">
                <div class=""col-md-5 col-sm-7"">
                    <div class=""mt--2"">
                        
                            <h1><span class=""color--primary"" style=""letter-spacing:0;"">Get started with 
							cosmic today</span></h1>
                            <p class=""lead""><span style=""color: #808080;"">Reach out to one of our Solution Specialists who will be happy to walk you through a customized demo for your restaurant bus");
            WriteLiteral(@"iness.</span></p>
                        <a class=""btn btn--primary type--uppercase inner-link"" href=""#"" target="""" title="""" rel=""""><span class=""btn__text"">Request demo</span></a>
                         
                    </div>
                </div>
                <div class=""col-md-7 col-sm-5 col-xs-12""><img alt=""image of cosmic pos for ipad"" src=""/site/images/banners/cos2.png""></div>
            </div>
          
        </div>
   
    </section>
    

	");
            EndContext();
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<dynamic> Html { get; private set; }
    }
}
#pragma warning restore 1591
