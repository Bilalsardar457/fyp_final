#pragma checksum "E:\Projects\PosCloudAdmin\PosCloudAdminPanel\PosCloudAdminPanel\Areas\Administration\Views\Product\ProductList.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "5d7bdb3b0ef261c33299b01c81649913d4bc21ca"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Areas_Administration_Views_Product_ProductList), @"mvc.1.0.view", @"/Areas/Administration/Views/Product/ProductList.cshtml")]
[assembly:global::Microsoft.AspNetCore.Mvc.Razor.Compilation.RazorViewAttribute(@"/Areas/Administration/Views/Product/ProductList.cshtml", typeof(AspNetCore.Areas_Administration_Views_Product_ProductList))]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#line 1 "E:\Projects\PosCloudAdmin\PosCloudAdminPanel\PosCloudAdminPanel\Areas\Administration\Views\_ViewImports.cshtml"
using PosCloudAdminPanel;

#line default
#line hidden
#line 2 "E:\Projects\PosCloudAdmin\PosCloudAdminPanel\PosCloudAdminPanel\Areas\Administration\Views\_ViewImports.cshtml"
using PosCloudAdminPanel.Models;

#line default
#line hidden
#line 1 "E:\Projects\PosCloudAdmin\PosCloudAdminPanel\PosCloudAdminPanel\Areas\Administration\Views\Product\ProductList.cshtml"
using PosCloudAdminPanel.Core.Model;

#line default
#line hidden
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"5d7bdb3b0ef261c33299b01c81649913d4bc21ca", @"/Areas/Administration/Views/Product/ProductList.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"3ef0d6934b8b7db14db5887cfa9f54cd3de07214", @"/Areas/Administration/Views/_ViewImports.cshtml")]
    public class Areas_Administration_Views_Product_ProductList : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<IEnumerable<PosCloudAdminPanel.Core.ViewModel.ProductViewModel>>
    {
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            BeginContext(110, 2, true);
            WriteLiteral("\r\n");
            EndContext();
#line 4 "E:\Projects\PosCloudAdmin\PosCloudAdminPanel\PosCloudAdminPanel\Areas\Administration\Views\Product\ProductList.cshtml"
      
        ViewData["Title"] = "ProductList";
    

#line default
#line hidden
            BeginContext(171, 217, true);
            WriteLiteral("\r\n    <h2 class=\"text-center font-weight-bold\">Product List</h2>\r\n    <div class=\"row\">\r\n        <div class=\"col-md-12\">\r\n            <div class=\"card\">\r\n                <div class=\"card-body\">\r\n                    <a");
            EndContext();
            BeginWriteAttribute("href", " href=\"", 388, "\"", 431, 1);
#line 13 "E:\Projects\PosCloudAdmin\PosCloudAdminPanel\PosCloudAdminPanel\Areas\Administration\Views\Product\ProductList.cshtml"
WriteAttributeValue("", 395, Url.Action("AddProduct", "Product"), 395, 36, false);

#line default
#line hidden
            EndWriteAttribute();
            BeginContext(432, 637, true);
            WriteLiteral(@" style=""flex-grow: 0.2"">
                        <button type=""button"" class=""btn btn-danger btn-sm""><i class=""fa fa-plus pr-sm""></i> Add Product</button>

                    </a>
                    <div class=""table-responsive"">
                        <table class=""table "" width=""100%"">
                            <tr>
                                <th class=""text-center"">ID</th>
                                <th class=""text-center"">Name</th>
                                <th class=""text-center"">Price</th>
                                <th class=""text-center"">Actions</th>
                            </tr>
");
            EndContext();
#line 25 "E:\Projects\PosCloudAdmin\PosCloudAdminPanel\PosCloudAdminPanel\Areas\Administration\Views\Product\ProductList.cshtml"
                               int i = 1;

#line default
#line hidden
            BeginContext(1113, 28, true);
            WriteLiteral("                            ");
            EndContext();
#line 26 "E:\Projects\PosCloudAdmin\PosCloudAdminPanel\PosCloudAdminPanel\Areas\Administration\Views\Product\ProductList.cshtml"
                             foreach (var product in Model)
                            {

#line default
#line hidden
            BeginContext(1205, 98, true);
            WriteLiteral("                                <tr>\r\n                                    <td class=\"text-center\">");
            EndContext();
            BeginContext(1305, 3, false);
#line 29 "E:\Projects\PosCloudAdmin\PosCloudAdminPanel\PosCloudAdminPanel\Areas\Administration\Views\Product\ProductList.cshtml"
                                                        Write(i++);

#line default
#line hidden
            EndContext();
            BeginContext(1309, 67, true);
            WriteLiteral("</td>\r\n                                    <td class=\"text-center\">");
            EndContext();
            BeginContext(1377, 12, false);
#line 30 "E:\Projects\PosCloudAdmin\PosCloudAdminPanel\PosCloudAdminPanel\Areas\Administration\Views\Product\ProductList.cshtml"
                                                       Write(product.Name);

#line default
#line hidden
            EndContext();
            BeginContext(1389, 67, true);
            WriteLiteral("</td>\r\n                                    <td class=\"text-center\">");
            EndContext();
            BeginContext(1457, 13, false);
#line 31 "E:\Projects\PosCloudAdmin\PosCloudAdminPanel\PosCloudAdminPanel\Areas\Administration\Views\Product\ProductList.cshtml"
                                                       Write(product.Price);

#line default
#line hidden
            EndContext();
            BeginContext(1470, 182, true);
            WriteLiteral("</td>\r\n\r\n\r\n                                    <td class=\"text-center\">\r\n                                        <div class=\"btn-group\">\r\n                                            ");
            EndContext();
            BeginContext(1653, 200, false);
#line 36 "E:\Projects\PosCloudAdmin\PosCloudAdminPanel\PosCloudAdminPanel\Areas\Administration\Views\Product\ProductList.cshtml"
                                       Write(Html.ActionLink(" ", "UpdateProduct", "Product", new { id = @product.Id }, new { @class = "btn btn-default text-success btn-xs glyphicon glyphicon-edit  ", data_toggle = "tooltip", title = "Edit !" }));

#line default
#line hidden
            EndContext();
            BeginContext(1853, 46, true);
            WriteLiteral("\r\n                                            ");
            EndContext();
            BeginContext(1900, 201, false);
#line 37 "E:\Projects\PosCloudAdmin\PosCloudAdminPanel\PosCloudAdminPanel\Areas\Administration\Views\Product\ProductList.cshtml"
                                       Write(Html.ActionLink(" ", "DeleteProduct", "Product", new { id = @product.Id }, new { @class = "btn btn-danger text-white btn-xs glyphicon glyphicon-remove  ", data_toggle = "tooltip", title = "Delete !" }));

#line default
#line hidden
            EndContext();
            BeginContext(2101, 2, true);
            WriteLiteral("\r\n");
            EndContext();
#line 38 "E:\Projects\PosCloudAdmin\PosCloudAdminPanel\PosCloudAdminPanel\Areas\Administration\Views\Product\ProductList.cshtml"
                                             if (product.IsActive)
                                            {
                                                

#line default
#line hidden
            BeginContext(2267, 201, false);
#line 40 "E:\Projects\PosCloudAdmin\PosCloudAdminPanel\PosCloudAdminPanel\Areas\Administration\Views\Product\ProductList.cshtml"
                                           Write(Html.ActionLink(" ", "DeActivateProduct", "Product", new { id = @product.Id }, new { @class = "btn btn-warning  btn-xs glyphicon glyphicon-refresh  ", data_toggle = "tooltip", title = "Deactivate !" }));

#line default
#line hidden
            EndContext();
#line 40 "E:\Projects\PosCloudAdmin\PosCloudAdminPanel\PosCloudAdminPanel\Areas\Administration\Views\Product\ProductList.cshtml"
                                                                                                                                                                                                                                                          
                                            }
                                            else
                                            {
                                                

#line default
#line hidden
            BeginContext(2663, 192, false);
#line 44 "E:\Projects\PosCloudAdmin\PosCloudAdminPanel\PosCloudAdminPanel\Areas\Administration\Views\Product\ProductList.cshtml"
                                           Write(Html.ActionLink(" ", "ActivateProduct", "Product", new { id = @product.Id }, new { @class = "btn btn-success  btn-xs glyphicon glyphicon-ok  ", data_toggle = "tooltip", title = "Activate !" }));

#line default
#line hidden
            EndContext();
#line 44 "E:\Projects\PosCloudAdmin\PosCloudAdminPanel\PosCloudAdminPanel\Areas\Administration\Views\Product\ProductList.cshtml"
                                                                                                                                                                                                                                                 
                                            }

#line default
#line hidden
            BeginContext(2904, 130, true);
            WriteLiteral("                                        </div>\r\n                                    </td>\r\n                                </tr>\r\n");
            EndContext();
#line 49 "E:\Projects\PosCloudAdmin\PosCloudAdminPanel\PosCloudAdminPanel\Areas\Administration\Views\Product\ProductList.cshtml"
                            }

#line default
#line hidden
            BeginContext(3065, 136, true);
            WriteLiteral("\r\n                        </table>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </div>\r\n");
            EndContext();
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<IEnumerable<PosCloudAdminPanel.Core.ViewModel.ProductViewModel>> Html { get; private set; }
    }
}
#pragma warning restore 1591
