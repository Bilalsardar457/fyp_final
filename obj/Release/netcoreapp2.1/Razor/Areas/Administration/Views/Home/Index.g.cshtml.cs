#pragma checksum "E:\Projects\PosCloudAdmin\PosCloudAdminPanel\PosCloudAdminPanel\Areas\Administration\Views\Home\Index.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "c05a47bb1f4881f24a02a6dcbbdd0aae36fa4ded"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Areas_Administration_Views_Home_Index), @"mvc.1.0.view", @"/Areas/Administration/Views/Home/Index.cshtml")]
[assembly:global::Microsoft.AspNetCore.Mvc.Razor.Compilation.RazorViewAttribute(@"/Areas/Administration/Views/Home/Index.cshtml", typeof(AspNetCore.Areas_Administration_Views_Home_Index))]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#line 1 "E:\Projects\PosCloudAdmin\PosCloudAdminPanel\PosCloudAdminPanel\Areas\Administration\Views\_ViewImports.cshtml"
using PosCloudAdminPanel;

#line default
#line hidden
#line 2 "E:\Projects\PosCloudAdmin\PosCloudAdminPanel\PosCloudAdminPanel\Areas\Administration\Views\_ViewImports.cshtml"
using PosCloudAdminPanel.Models;

#line default
#line hidden
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"c05a47bb1f4881f24a02a6dcbbdd0aae36fa4ded", @"/Areas/Administration/Views/Home/Index.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"3ef0d6934b8b7db14db5887cfa9f54cd3de07214", @"/Areas/Administration/Views/_ViewImports.cshtml")]
    public class Areas_Administration_Views_Home_Index : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<dynamic>
    {
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_0 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("selected", new global::Microsoft.AspNetCore.Html.HtmlString("selected"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_1 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("action", new global::Microsoft.AspNetCore.Html.HtmlString("#"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_2 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("src", new global::Microsoft.AspNetCore.Html.HtmlString("~/js/dashboard/dashboard-3.js"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        #line hidden
        #pragma warning disable 0169
        private string __tagHelperStringValueBuffer;
        #pragma warning restore 0169
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperExecutionContext __tagHelperExecutionContext;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner __tagHelperRunner = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner();
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __backed__tagHelperScopeManager = null;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __tagHelperScopeManager
        {
            get
            {
                if (__backed__tagHelperScopeManager == null)
                {
                    __backed__tagHelperScopeManager = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager(StartTagHelperWritingScope, EndTagHelperWritingScope);
                }
                return __backed__tagHelperScopeManager;
            }
        }
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.FormTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper;
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.RenderAtEndOfFormTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_RenderAtEndOfFormTagHelper;
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.OptionTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_OptionTagHelper;
        private global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper;
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
#line 1 "E:\Projects\PosCloudAdmin\PosCloudAdminPanel\PosCloudAdminPanel\Areas\Administration\Views\Home\Index.cshtml"
  
    ViewData["Title"] = "Home Page";

#line default
#line hidden
            BeginContext(45, 2, true);
            WriteLiteral("\r\n");
            EndContext();
            BeginContext(429, 3720, true);
            WriteLiteral(@"<div class=""row"">
    <h1 style=""text-align: center"">Dashboard</h1>
</div>
<div class=""row"">
    <div class=""col-xl-3 col-sm-6"">
        <div class=""card stat-widget-five"" style=""background-color:transparent"">
            <div class=""card-body"">
                <div class=""media"">
                    <div class=""icon mr-3 bg-success"">
                        <i class=""fa fa-shopping-cart""></i>
                    </div>
                    <div class=""media-body text-right"">
                        <h2 class=""mt-0 mb-2 text-success"">15310</h2><span class=""text-pale-sky"">Total Sales</span>
                    </div>
                </div>
                <div class=""bottom-text mt-0"">
                    <p class=""m-0"">$30,820</p>
                    <span><i class=""fa fa-caret-up""></i> 35.45%</span>
                </div>
            </div>
        </div>
    </div>
    <div class=""col-xl-3 col-sm-6"">
        <div class=""card stat-widget-five"">
            <div class=""card-body"">
    ");
            WriteLiteral(@"            <div class=""media"">
                    <div class=""icon mr-3 bg-info"">
                        <i class=""fa fa-usd""></i>
                    </div>
                    <div class=""media-body text-right"">
                        <h2 class=""mt-0 mb-2 text-info"">984</h2><span class=""text-pale-sky"">New Orders</span>
                    </div>
                </div>
                <div class=""bottom-text mt-0"">
                    <p class=""m-0"">$30,820</p>
                    <span><i class=""fa fa-caret-up""></i> 35.45%</span>
                </div>
            </div>
        </div>
    </div>
    <div class=""col-xl-6 col-lg-12"">
        <div class=""card stat-widget-progress"">
            <div class=""card-body"">
                <h5 class=""text-pale-sky"">Total Receivable: $12,570</h5>
                <div class=""progress mb-4 mt-3"">
                    <div class=""progress-bar bg-success"" role=""progressbar"" style=""width: 30%"" aria-valuenow=""15"" aria-valuemin=""0"" aria-valuemax=""100""");
            WriteLiteral(@"></div>
                    <div class=""progress-bar bg-warning"" role=""progressbar"" style=""width: 30%"" aria-valuenow=""30"" aria-valuemin=""0"" aria-valuemax=""100""></div>
                    <div class=""progress-bar bg-danger"" role=""progressbar"" style=""width: 40%"" aria-valuenow=""20"" aria-valuemin=""0"" aria-valuemax=""100""></div>
                </div>
                <div class=""row"">
                    <div class=""col-lg-12"">
                        <div class=""d-flex justify-content-between flex-wrap"">
                            <div class=""item"">
                                <h5 class=""text-warning"">Current</h5>
                                <h5 class=""mb-0 text-pale-sky"">$2840</h5>
                            </div>
                            <div class=""item"">
                                <h5 class=""d-inline-block text-warning"">Override</h5>
                                <span>(15-30 days)</span>
                                <h5 class=""mb-0 text-pale-sky"">$2840</h5>
             ");
            WriteLiteral(@"               </div>
                            <div class=""item"">
                                <span>Above 30 days</span>
                                <h5 class=""mb-0 text-pale-sky"">$2840</h5>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class=""row"">
    <div class=""col-xl-9"">
        <div class=""card"">
            <div class=""card-header pb-0"">
                <h4 class=""card-title m-t-10"">Sales and Expenses</h4>
                <div class=""table-action float-right"">
                    ");
            EndContext();
            BeginContext(4149, 641, false);
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("form", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "de63cf6bca0f48c5891613d9cf68bc58", async() => {
                BeginContext(4166, 237, true);
                WriteLiteral("\r\n                        <div class=\"form-row\">\r\n                            <div class=\"form-group m-b-0\">\r\n                                <select class=\"selectpicker show-tick\" data-width=\"auto\">\r\n                                    ");
                EndContext();
                BeginContext(4403, 49, false);
                __tagHelperExecutionContext = __tagHelperScopeManager.Begin("option", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "f59835ef93144683b7739b488489d1d1", async() => {
                    BeginContext(4431, 12, true);
                    WriteLiteral("Last 30 Days");
                    EndContext();
                }
                );
                __Microsoft_AspNetCore_Mvc_TagHelpers_OptionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.OptionTagHelper>();
                __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_OptionTagHelper);
                __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_0);
                await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
                if (!__tagHelperExecutionContext.Output.IsContentModified)
                {
                    await __tagHelperExecutionContext.SetOutputContentAsync();
                }
                Write(__tagHelperExecutionContext.Output);
                __tagHelperExecutionContext = __tagHelperScopeManager.End();
                EndContext();
                BeginContext(4452, 38, true);
                WriteLiteral("\r\n                                    ");
                EndContext();
                BeginContext(4490, 29, false);
                __tagHelperExecutionContext = __tagHelperScopeManager.Begin("option", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "619dfcc03b4f46d4929fef6118bb48a7", async() => {
                    BeginContext(4498, 12, true);
                    WriteLiteral("Last 1 MOnth");
                    EndContext();
                }
                );
                __Microsoft_AspNetCore_Mvc_TagHelpers_OptionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.OptionTagHelper>();
                __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_OptionTagHelper);
                await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
                if (!__tagHelperExecutionContext.Output.IsContentModified)
                {
                    await __tagHelperExecutionContext.SetOutputContentAsync();
                }
                Write(__tagHelperExecutionContext.Output);
                __tagHelperExecutionContext = __tagHelperScopeManager.End();
                EndContext();
                BeginContext(4519, 38, true);
                WriteLiteral("\r\n                                    ");
                EndContext();
                BeginContext(4557, 29, false);
                __tagHelperExecutionContext = __tagHelperScopeManager.Begin("option", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "af9ac20189fe4a188c350569943af872", async() => {
                    BeginContext(4565, 12, true);
                    WriteLiteral("Last 6 MOnth");
                    EndContext();
                }
                );
                __Microsoft_AspNetCore_Mvc_TagHelpers_OptionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.OptionTagHelper>();
                __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_OptionTagHelper);
                await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
                if (!__tagHelperExecutionContext.Output.IsContentModified)
                {
                    await __tagHelperExecutionContext.SetOutputContentAsync();
                }
                Write(__tagHelperExecutionContext.Output);
                __tagHelperExecutionContext = __tagHelperScopeManager.End();
                EndContext();
                BeginContext(4586, 38, true);
                WriteLiteral("\r\n                                    ");
                EndContext();
                BeginContext(4624, 26, false);
                __tagHelperExecutionContext = __tagHelperScopeManager.Begin("option", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "a14c330520774c8d9e4ed248507f2671", async() => {
                    BeginContext(4632, 9, true);
                    WriteLiteral("Last Year");
                    EndContext();
                }
                );
                __Microsoft_AspNetCore_Mvc_TagHelpers_OptionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.OptionTagHelper>();
                __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_OptionTagHelper);
                await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
                if (!__tagHelperExecutionContext.Output.IsContentModified)
                {
                    await __tagHelperExecutionContext.SetOutputContentAsync();
                }
                Write(__tagHelperExecutionContext.Output);
                __tagHelperExecutionContext = __tagHelperScopeManager.End();
                EndContext();
                BeginContext(4650, 133, true);
                WriteLiteral("\r\n                                </select>\r\n                            </div>\r\n                        </div>\r\n                    ");
                EndContext();
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.FormTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper);
            __Microsoft_AspNetCore_Mvc_TagHelpers_RenderAtEndOfFormTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.RenderAtEndOfFormTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_RenderAtEndOfFormTagHelper);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_1);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            EndContext();
            BeginContext(4790, 10116, true);
            WriteLiteral(@"
                </div>
            </div>
            <div class=""card-body"">
                <div class=""row"">
                    <div class=""col-xl-10"">
                        <div id=""morris-bar-chart""></div>
                    </div>
                    <div class=""col-xl-2"">
                        <div class=""d-flex d-xl-block flex-wrap justify-content-around mt-5 mt-xl-0"">
                            <div class=""mb-5 text-xl-right"">
                                <h5 class=""text-success"">Total Sell</h5>
                                <h6>$30,000</h6>
                            </div>
                            <div class=""mb-5 text-xl-right"">
                                <h5 class=""text-info"">Total Receipts</h5>
                                <h6>$18,250</h6>
                            </div>
                            <div class=""mb-5 text-xl-right"">
                                <h5 class=""text-danger"">Total Expenses</h5>
                                <h6>$30,000");
            WriteLiteral(@"</h6>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class=""col-xl-3"">
        <div class=""card"">
            <div class=""card-body card-body-2"">
                <h4 class=""card-title"">Monthly Earning</h4>
                <div class=""chart"">
                    <div id=""morris-donut-chart""></div>
                </div>
                <div class=""earning-chart-data"">
                    <ul class=""d-flex flex-wrap justify-content-between"">
                        <li class=""d-flex justify-content-between mb-3"">
                            <span class=""name"">
                                <span class=""mr-2 text-warning""><i class=""fa fa-circle""></i></span> Chrome
                            </span>
                            <span class=""value"">6520</span>
                        </li>
                        <li class=""d-flex justify-content-between mb-3"">
    ");
            WriteLiteral(@"                        <span class=""name"">
                                <span class=""mr-2 text-info""><i class=""fa fa-circle""></i></span> Firefox
                            </span>
                            <span class=""value"">5520</span>
                        </li>
                        <li class=""d-flex justify-content-between mb-3"">
                            <span class=""name"">
                                <span class=""mr-2 text-success""><i class=""fa fa-circle""></i></span> Safari
                            </span>
                            <span class=""value"">1520</span>
                        </li>
                        <li class=""d-flex justify-content-between mb-3"">
                            <span class=""name"">
                                <span class=""mr-2 text-primary""><i class=""fa fa-circle""></i></span> Others
                            </span>
                            <span class=""value"">520</span>
                        </li>
                    </ul>");
            WriteLiteral(@"
                </div>
                <!-- <div class=""row"">
                    <div class=""col-6"">
                        <ul>
                            <li class=""d-flex"">
                                <div>
                                    <span class=""diffrent-color""></span> Chrome
                                </div>
                                <span class=""text-dark"">6520</span>
                            </li>
                            <li class=""d-flex"">
                                <div>
                                    <span class=""text-success""><i class=""fa fa-circle""></i></span> Safari
                                </div>
                                <span class=""text-dark"">3750</span>
                            </li>

                        </ul>
                    </div>
                    <div class=""col-6"">
                        <ul>
                            <li class=""d-flex"">
                                <div>
               ");
            WriteLiteral(@"                     <span class=""text-warning""><i class=""fa fa-circle""></i></span> Firefox
                                </div>
                                <span class=""text-dark"">2150</span>
                            </li>
                            <li class=""d-flex"">
                                <div>
                                    <span class=""text-info""><i class=""fa fa-circle""></i></span> Others
                                </div>
                                <span class=""text-dark"">750</span>
                            </li>
                        </ul>
                    </div>

                </div> -->
            </div>
        </div>
    </div>
</div>
<div class=""row"">
    <div class=""col-xl-12"">
        <div class=""card transparent-card m-b-0"">
            <div class=""card-header"">
                <h4 class=""card-title"">Recent Clients</h4>
            </div>
            <div class=""card-body p-0"">
                <div class=""table-responsive"">
");
            WriteLiteral(@"                    <table class=""table table-padded market-capital table-responsive-fix-big"">
                        <thead>
                            <tr>
                                <th>Inv. No</th>
                                <th>Client Name</th>
                                <th>Product</th>
                                <th>Date</th>
                                <th>Amount</th>
                                <th>Status</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>
                                    <span class=""text-pale-sky"">#125</span>
                                </td>
                                <td>
                                    <span class=""text-pale-sky"">Bailey Wonger</span>
                                </td>
                                <td class=""text-primary"">UX/UI Design Services</td>
                        ");
            WriteLiteral(@"        <td>20/05/2018</td>
                                <td>
                                    <span class=""text-pale-sky"">$15,250</span>
                                </td>
                                <td>
                                    <span class=""label label-fixed label-success"">Paid</span>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <span class=""text-pale-sky"">#134</span>
                                </td>
                                <td>
                                    <span class=""text-pale-sky"">Linguina Nettlewater</span>
                                </td>
                                <td class=""text-primary"">.Net Consultancy Services</td>
                                <td>29/05/2018</td>
                                <td>
                                    <span class=""text-pale-sky"">$13,450</span>
             ");
            WriteLiteral(@"                   </td>
                                <td>
                                    <span class=""label label-fixed label-success"">Paid</span>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <span class=""text-pale-sky"">#139</span>
                                </td>
                                <td>
                                    <span class=""text-pale-sky"">Benjamin Evalent</span>
                                </td>
                                <td class=""text-primary"">Art Direction</td>
                                <td>06/06/2018</td>
                                <td>
                                    <span class=""text-pale-sky"">$12,346</span>
                                </td>
                                <td>
                                    <span class=""label label-fixed label-light"">Draft</span>
                  ");
            WriteLiteral(@"              </td>
                            </tr>
                            <tr>
                                <td>
                                    <span class=""text-pale-sky"">#141</span>
                                </td>
                                <td>
                                    <span class=""text-pale-sky"">Ingredia Nutrisha</span>
                                </td>
                                <td class=""text-primary"">Software Testings and QA</td>
                                <td>18/06/2018</td>
                                <td>
                                    <span class=""text-pale-sky"">$10,452</span>
                                </td>
                                <td>
                                    <span class=""label label-fixed label-light"">Draft</span>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <");
            WriteLiteral(@"span class=""text-pale-sky"">#145</span>
                                </td>
                                <td>
                                    <span class=""text-pale-sky"">Hermann P. Schnitzel</span>
                                </td>
                                <td class=""text-primary"">App Application Development</td>
                                <td>08/07/2018</td>
                                <td>
                                    <span class=""text-pale-sky"">$8,752</span>
                                </td>
                                <td>
                                    <span class=""label label-fixed label-success"">Paid</span>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
");
            EndContext();
            DefineSection("Scripts", async() => {
                BeginContext(14925, 8, true);
                WriteLiteral("\r\n    \r\n");
                EndContext();
                BeginContext(14933, 53, false);
                __tagHelperExecutionContext = __tagHelperScopeManager.Begin("script", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "e9cbb41b22eb4be2b40d82233b3a7eea", async() => {
                }
                );
                __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper>();
                __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper);
                __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_2);
                await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
                if (!__tagHelperExecutionContext.Output.IsContentModified)
                {
                    await __tagHelperExecutionContext.SetOutputContentAsync();
                }
                Write(__tagHelperExecutionContext.Output);
                __tagHelperExecutionContext = __tagHelperScopeManager.End();
                EndContext();
                BeginContext(14986, 2, true);
                WriteLiteral("\r\n");
                EndContext();
            }
            );
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<dynamic> Html { get; private set; }
    }
}
#pragma warning restore 1591
