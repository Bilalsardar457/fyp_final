#pragma checksum "E:\Projects\PosCloudAdmin\PosCloudAdminPanel\PosCloudAdminPanel\Areas\Client\Views\Complaint\ViewComplaint.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "9e968ff40380546222d2874c1a8a16db2db3dd1f"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Areas_Client_Views_Complaint_ViewComplaint), @"mvc.1.0.view", @"/Areas/Client/Views/Complaint/ViewComplaint.cshtml")]
[assembly:global::Microsoft.AspNetCore.Mvc.Razor.Compilation.RazorViewAttribute(@"/Areas/Client/Views/Complaint/ViewComplaint.cshtml", typeof(AspNetCore.Areas_Client_Views_Complaint_ViewComplaint))]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#line 1 "E:\Projects\PosCloudAdmin\PosCloudAdminPanel\PosCloudAdminPanel\Areas\Client\Views\_ViewImports.cshtml"
using PosCloudAdminPanel;

#line default
#line hidden
#line 2 "E:\Projects\PosCloudAdmin\PosCloudAdminPanel\PosCloudAdminPanel\Areas\Client\Views\_ViewImports.cshtml"
using PosCloudAdminPanel.Models;

#line default
#line hidden
#line 1 "E:\Projects\PosCloudAdmin\PosCloudAdminPanel\PosCloudAdminPanel\Areas\Client\Views\Complaint\ViewComplaint.cshtml"
using PosCloudAdminPanel.Core.Model;

#line default
#line hidden
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"9e968ff40380546222d2874c1a8a16db2db3dd1f", @"/Areas/Client/Views/Complaint/ViewComplaint.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"3ef0d6934b8b7db14db5887cfa9f54cd3de07214", @"/Areas/Client/Views/_ViewImports.cshtml")]
    public class Areas_Client_Views_Complaint_ViewComplaint : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<PosCloudAdminPanel.Core.ViewModel.ComplaintDisplayViewModel>
    {
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
#line 3 "E:\Projects\PosCloudAdmin\PosCloudAdminPanel\PosCloudAdminPanel\Areas\Client\Views\Complaint\ViewComplaint.cshtml"
  
    ViewData["Title"] = "ViewComplaint";

#line default
#line hidden
            BeginContext(170, 541, true);
            WriteLiteral(@"

<div class=""container col-md-8 col-md-offset-2"">
    
    <h3><strong class=""font-weight-bold"">View Complaint</strong></h3>
    <hr/>
    <div class=""row"" style=""background-color: #def6de; padding: 8px; border-bottom: #c5e8c5 solid"">
        <div class=""col-lg-12"">

            <span class=""fa fa-pencil font-weight-bold text-success""> Reply</span>
            <a class=""pull-right fa fa-plus"" data-toggle=""collapse"" data-target=""#cml"" style=""padding: 5px""></a>
        </div>
    </div>
    <div class=""collapse"" id=""cml"">
");
            EndContext();
#line 20 "E:\Projects\PosCloudAdmin\PosCloudAdminPanel\PosCloudAdminPanel\Areas\Client\Views\Complaint\ViewComplaint.cshtml"
           Html.RenderPartial("AddComplaintPartial",Model.Model);

#line default
#line hidden
            BeginContext(779, 8, true);
            WriteLiteral("        ");
            EndContext();
            BeginContext(867, 456, true);
            WriteLiteral(@"
    </div>
                       
                   
                
            
        
</div>
<div class=""container col-md-8 col-md-offset-2"" style=""border:#c5e8c5 1px solid"">
    
    <div class=""row "">        
        <div class=""col-xs-8 p-l-10"">
            <div class=""col-xs-4 col-md-1 pl-0"">
                <i class=""fa fa-user fa-4x""></i>
            </div>
            <div class=""col-md-11 col-xs-8"">
                <h5>");
            EndContext();
            BeginContext(1324, 29, false);
#line 37 "E:\Projects\PosCloudAdmin\PosCloudAdminPanel\PosCloudAdminPanel\Areas\Client\Views\Complaint\ViewComplaint.cshtml"
               Write(Model.ComplaintViewModel.User);

#line default
#line hidden
            EndContext();
            BeginContext(1353, 167, true);
            WriteLiteral("</h5>\r\n                <h5 class=\"font-weight-bold\">Staff</h5>\r\n            </div>\r\n        </div>\r\n        <div class=\"col-xs-4\">\r\n            <h5 class=\"text-right\">");
            EndContext();
            BeginContext(1521, 60, false);
#line 42 "E:\Projects\PosCloudAdmin\PosCloudAdminPanel\PosCloudAdminPanel\Areas\Client\Views\Complaint\ViewComplaint.cshtml"
                              Write(Model.ComplaintViewModel.Date.ToString("dd-MMM-yy hh-mm-ss"));

#line default
#line hidden
            EndContext();
            BeginContext(1581, 135, true);
            WriteLiteral("</h5>\r\n        </div>\r\n\r\n        <div class=\"col-xs-12\">\r\n                      \r\n            <h3 class=\"text-danger font-weight-bold\">");
            EndContext();
            BeginContext(1717, 32, false);
#line 47 "E:\Projects\PosCloudAdmin\PosCloudAdminPanel\PosCloudAdminPanel\Areas\Client\Views\Complaint\ViewComplaint.cshtml"
                                                Write(Model.ComplaintViewModel.Subject);

#line default
#line hidden
            EndContext();
            BeginContext(1749, 22, true);
            WriteLiteral("</h3>\r\n            <p>");
            EndContext();
            BeginContext(1772, 32, false);
#line 48 "E:\Projects\PosCloudAdmin\PosCloudAdminPanel\PosCloudAdminPanel\Areas\Client\Views\Complaint\ViewComplaint.cshtml"
          Write(Model.ComplaintViewModel.Message);

#line default
#line hidden
            EndContext();
            BeginContext(1804, 289, true);
            WriteLiteral(@"</p>

        </div>
    </div>
</div>

<div class=""container col-md-8 col-md-offset-2"">
    <div class=""row"">
        <div class=""col-xs-12 text-center"">
            <h3 class=""font-weight-bold""><strong>Replies</strong></h3>
        </div>
        </div>
    
   
        
");
            EndContext();
#line 63 "E:\Projects\PosCloudAdmin\PosCloudAdminPanel\PosCloudAdminPanel\Areas\Client\Views\Complaint\ViewComplaint.cshtml"
     foreach (var reply in Model.ComplaintReplies.OrderByDescending(a => a.Date))
    {

#line default
#line hidden
            BeginContext(2183, 526, true);
            WriteLiteral(@"        <div class=""row"">
            <div class=""col-xs-12"">
                <div class=""panel"">
                    <div class=""row"" style=""background-color: #def6de;border-bottom: 1px #c5e8c5 solid"">
                    <div class=""col-xs-8"">
                        <div class=""col-xs-4 col-md-1  pl-0 pr-0"">
                            <i class=""fa fa-user fa-2x""></i>
                        </div>
                        <div class=""col-md-11 col-xs-8 pl-0"">
                            <h5 class=""mt-0 mb-0"">");
            EndContext();
            BeginContext(2710, 10, false);
#line 74 "E:\Projects\PosCloudAdmin\PosCloudAdminPanel\PosCloudAdminPanel\Areas\Client\Views\Complaint\ViewComplaint.cshtml"
                                             Write(reply.User);

#line default
#line hidden
            EndContext();
            BeginContext(2720, 238, true);
            WriteLiteral("</h5>\r\n                            <h5 class=\" mt-0 mb-0 font-weight-bold\">Waqar</h5>\r\n                        </div>\r\n                    </div>\r\n                    <div class=\"col-xs-4\">\r\n                        <h5 class=\"text-right\">");
            EndContext();
            BeginContext(2959, 41, false);
#line 79 "E:\Projects\PosCloudAdmin\PosCloudAdminPanel\PosCloudAdminPanel\Areas\Client\Views\Complaint\ViewComplaint.cshtml"
                                          Write(reply.Date.ToString("dd-MMM-yy hh:mm tt"));

#line default
#line hidden
            EndContext();
            BeginContext(3000, 160, true);
            WriteLiteral("</h5>\r\n                    </div>\r\n                    </div>\r\n                    <div class=\"col-xs-12\">\r\n                        <strong class=\"text-danger\">");
            EndContext();
            BeginContext(3161, 13, false);
#line 83 "E:\Projects\PosCloudAdmin\PosCloudAdminPanel\PosCloudAdminPanel\Areas\Client\Views\Complaint\ViewComplaint.cshtml"
                                               Write(reply.Subject);

#line default
#line hidden
            EndContext();
            BeginContext(3174, 40, true);
            WriteLiteral("</strong>\r\n                        <div>");
            EndContext();
            BeginContext(3215, 13, false);
#line 84 "E:\Projects\PosCloudAdmin\PosCloudAdminPanel\PosCloudAdminPanel\Areas\Client\Views\Complaint\ViewComplaint.cshtml"
                        Write(reply.Message);

#line default
#line hidden
            EndContext();
            BeginContext(3228, 108, true);
            WriteLiteral("</div>\r\n                    </div>\r\n\r\n                </div>\r\n\r\n\r\n\r\n\r\n\r\n            </div>\r\n        </div>\r\n");
            EndContext();
#line 95 "E:\Projects\PosCloudAdmin\PosCloudAdminPanel\PosCloudAdminPanel\Areas\Client\Views\Complaint\ViewComplaint.cshtml"
    }

#line default
#line hidden
            BeginContext(3343, 8, true);
            WriteLiteral("\r\n</div>");
            EndContext();
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<PosCloudAdminPanel.Core.ViewModel.ComplaintDisplayViewModel> Html { get; private set; }
    }
}
#pragma warning restore 1591
