﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using PosCloudAdminPanel.Core.Repository;

namespace PosCloudAdminPanel.Core
{
    public interface IUnitOfWork
    {
        IClientRepository ClientRepository { get; }
        IClientAppKeyRepository ClientAppKeyRepository { get; }
        IComplaintRepository ComplaintRepository { get; }
        IComplaintReplyRepository ComplaintReplyRepository { get; }
        IProductRepository ProductRepository { get; }
        IPackageRepository PackageRepository { get; }
        IPackageProductRepository PackageProductRepository { get; }
        IOrderRepository OrderRepository { get; }
        IOrderDetailRepository OrderDetailRepository { get; }
        IInvoiceRepository InvoiceRepository { get; }
        void Complete();
    }
}
