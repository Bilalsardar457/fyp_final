﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace PosCloudAdminPanel.Core.ViewModel
{
    public class PackageProductViewModel
    {
        public int? Id { get; set; }
        [DisplayName("Package")]
        public int PackageId { get; set; }
        [DisplayName("Product")]
        public int ProductId { get; set; }
        public decimal Quantity { get; set; }
        public IEnumerable<SelectListItem> PackageDdl { get; set; }
        public string PackageName { get; set; }
        public IEnumerable<SelectListItem> ProductDdl { get; set; }
        public string ProductName { get; set; }
        public decimal ProductPrice { get; set; }

        public DateTime? CreatedOn { get; set; }
        public string CreatedBy { get; set; }
        public bool IsActive { get; set; }
    }
}
