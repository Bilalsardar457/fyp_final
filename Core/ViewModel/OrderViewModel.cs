﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace PosCloudAdminPanel.Core.ViewModel
{
    public class OrderViewModel
    {
        public int Id { get; set; }
        [DisplayName("Client")]
        public Guid ClientId { get; set; }
        [Required]
        [DisplayName("Package Name")]
        public string PackageName { get; set; }
        [Required]
        public decimal Price { get; set; }
        public decimal Discount { get; set; }
        public string PaymentTerms { get; set; }
        [Required]
        [DisplayName("Order Date")]
        [DataType(DataType.DateTime)]
        public DateTime OrderDate { get; set; }
        public bool IsInvoiced { get; set; }
        public string Status { get; set; }
        public IEnumerable<SelectListItem> ClientDdl { get; set; }
        public string ClientName { get; set; }
        public DateTime? CreatedOn { get; set; }
        public string CreatedBy { get; set; }
        public bool IsActive { get; set; }
    }
}
