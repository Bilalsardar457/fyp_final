﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace PosCloudAdminPanel.Core.ViewModel
{
    public class ClientApiViewModel
    {
        public int? Id { get; set; }
        public string Name { get; set; }
        public string ArabicName { get; set; }
        public string Address { get; set; }
        public string Contact { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Currency { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public bool IsOperational { get; set; }
        public DateTime BusinessStartTime { get; set; }
        public string VatNumber { get; set; }


    }
}
