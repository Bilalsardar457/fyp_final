﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore.ValueGeneration.Internal;
using PosCloudAdminPanel.Core.Model;

namespace PosCloudAdminPanel.Core.ViewModel
{
    public class ClientViewModel
    {
        public Guid ClientId { get; set; }
        [Required]
        public string FirstName { get; set; }
        [Required]
        public string LastName { get; set; }
        
        [Required]
        [RegularExpression(@"^([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})$", ErrorMessage = "Entered email format like (Example@abc.com)")]
        public string Email { get; set; }
        [Required]
        [RegularExpression("^[0-9]{1,6}$", ErrorMessage = "Must Enter Digits in password")]
        //[StringLength(6, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }
        public string Address { get; set; }
        [DisplayName("Primary Contact")]
        
        public string PrimaryContact { get; set; }
        [DisplayName("Secondary Contact")]
      
        public string SecondaryContact { get; set; }
       
        public string FaxNo { get; set; }
        [DisplayName("Package")]
        public int PackageId { get; set; }
        public string PackageName { get; set; }
        public IEnumerable<SelectListItem> PackageDdl { get; set; } 
        public string VatNumber { get; set; }

        public string PostalCode { get; set; }
        public string City { get; set; }

        public string State { get; set; }
        public string Currency { get; set; }
        public bool IsOperational { get; set; }
        public DateTime BusinessStartTime { get; set; }
        public string Country { get; set; }
        [DisplayName("Company Name")]
        [Required]
        public string CompanyName { get; set; }
        [DisplayName("Company Email")]
        [Required]
        [RegularExpression(@"^([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})$", ErrorMessage = "Entered email format like (Example@abc.com)")]
        public string CompanyEmail { get; set; }
        [DisplayName("Company Address")]
        public string CompanyAddress { get; set; }
        [Required]
        [DisplayName("Company Contact")]
        

        
        public string CompanyContact { get; set; }
        [DisplayName("Company City")]
        public string CompanyCity { get; set; }
        [DisplayName("Company State")]
        public string CompanyState { get; set; }
        [DisplayName("Company Country")]
        public string CompanyCountry { get; set; }
        [DisplayName("NTN No.")]
        [Required]
        public string CompanyNtn { get; set; }
        [DisplayName("Company Registration No.")]
        [Required]
        public string CompanyRegistrationNumber { get; set; }
        [DisplayName("Company Logo")]
        
        public byte[] CompanyLogo { get; set; }
        public DateTime? CreatedOn { get; set; }
        public string CreatedBy { get; set; }
        public bool IsActive { get; set; }
        [Required]
        public IFormFile ImageUpload { get; set; }
    }
}
