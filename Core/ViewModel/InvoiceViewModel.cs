﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PosCloudAdminPanel.Core.ViewModel
{
    public class InvoiceViewModel
    {
        public int Id { get; set; }
        public int OrderId { get; set; }
        public decimal DueAmount { get; set; }
        public decimal PaidAmount { get; set; }
        public decimal TotalAmount { get; set; }
        public DateTime PaymentDate { get; set; }
        public DateTime? CreatedOn { get; set; }
        public string CreatedBy { get; set; }
        public bool IsActive { get; set; }
        public ClientViewModel ClientViewModel { get; set; }
        public OrderViewModel OrderViewModel { get; set; }
        public IEnumerable<OrderViewModel> OrderViewModels { get; set; }

    }
}
