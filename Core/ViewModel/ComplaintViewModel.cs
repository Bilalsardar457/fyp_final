﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Rendering;
using Newtonsoft.Json;

namespace PosCloudAdminPanel.Core.ViewModel
{
    public class ComplaintViewModel
    {
        public int ComplaintId { get; set; }
        public string User { get; set; }
        public string UserId { get; set; }
        public string Subject { get; set; }
        public string Message { get; set; }
        public string ComplaintStatus { get; set; }
        public string ComplaintType { get; set; }
        public int? ParentComplaintId { get; set; }
        [Required]
        public DateTime Date { get; set; }
        public byte[] Image { get; set; }
        public IFormFile ImageUpload { get; set; }
        [Required]
        [DefaultValue(false)]
        public bool IsReply { get; set; }
        
        public DateTime? CreatedOn { get; set; }
        public string CreatedBy { get; set; }
        
    }

    public class ComplaintDisplayViewModel
    {
        public ComplaintViewModel ComplaintViewModel { get; set; }=new ComplaintViewModel();
        
        public List<ComplaintViewModel> ComplaintReplies { get; set; }=new List<ComplaintViewModel>();

        public ComplaintViewModel Model { get; set; }=new ComplaintViewModel();
        
    }


 
}
