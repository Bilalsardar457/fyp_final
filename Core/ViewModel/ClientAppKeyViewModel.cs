﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace PosCloudAdminPanel.Core.ViewModel
{
    public class ClientAppKeyViewModel
    {
        public string AppKey { get; set; }
        public bool IsUsed { get; set; }
        [DisplayName("Client")]
        public string ClientId { get; set; }
        [Required]
        [RegularExpression(@"^\d{15}(,\d{15})*$", ErrorMessage = "You can enter maximum 15 digits")]
        public string IMEI { get; set; }
        public DateTime? CreatedOn { get; set; }
        public string CreatedBy { get; set; }
        public IEnumerable<SelectListItem> ClientDdl { get; set; }
        public string ClientName { get; set; }
    }

    
}
