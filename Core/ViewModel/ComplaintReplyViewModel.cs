﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace PosCloudAdminPanel.Core.ViewModel
{
    public class ComplaintReplyViewModel 
    {
        public int Id { get; set; }
        [DisplayName("Complaint ID")]
        public int ComplaintId { get; set; }
        [DisplayName("User")]
        public string ApplicationUserId { get; set; }
        [Required]
        public string Subject { get; set; }
        [Required]
        public string Description { get; set; }
        public DateTime? CreatedOn { get; set; }
        public string CreatedBy { get; set; }
       
    }
  
    public  class ComplaintReplyDisplayViewModel
    {
        public ComplaintViewModel ComplaintViewModel { get; set; } = new ComplaintViewModel();
        public ComplaintReplyViewModel ComplaintReplyViewModel { get; set; } = new ComplaintReplyViewModel();
        public List<ComplaintViewModel> ComplaintReplies { get; set; } = new List<ComplaintViewModel>();
        public List<ComplaintReplyViewModel> AdminComplaintReplies { get; set; } = new List<ComplaintReplyViewModel>();
        public ComplaintReplyViewModel Model { get; set; } = new ComplaintReplyViewModel();

    }
}
