﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PosCloudAdminPanel.Core.ViewModel
{
    public class RequestDemo
    {
        public string First { get; set; }
        public string Last { get; set; }
        public string Org { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string UserName { get; set; }
        public string Home { get; set; }
        public string Cell { get; set; }
        
    }
}
