﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;

namespace PosCloudAdminPanel.Core.ViewModel
{
    public class ClientDashboardViewModel
    {
        [DefaultValue(0)]
        public int Invoices { get; set; }
        [DefaultValue(0)]
        public int Products { get; set; }
        [DefaultValue(0)]
        public int AppKeys { get; set; }
        [DefaultValue(0)]
        public int Complaints { get; set; }
    }
}
