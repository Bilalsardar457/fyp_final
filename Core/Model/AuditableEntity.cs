﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace PosCloudAdminPanel.Core.Model
{
    public abstract class AuditableEntity
    {
        [Column(TypeName = "DateTime2")]
        public DateTime CreatedOn { get; set; }

        public string CreatedById { get; set; }
        //   public ApplicationUser CreatedBy { get; set; }

        public string UpdatedById { get; set; }
        //  public ApplicationUser UpdatedBy { get; set; }

        [Column(TypeName = "DateTime2")]
        public DateTime UpdatedOn { get; set; }
        [DefaultValue(true)]
        public bool IsActive { get; set; }
    }
}
