﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.CodeAnalysis.Operations;

namespace PosCloudAdminPanel.Core.Model
{
    public class Complaint : AuditableEntity
    {
        public int ComplaintId { get; set; }
        public string UserId { get; set; }
        public ApplicationUser ApplicationUser { get; set; }
        public string Subject { get; set; }
        public string Message { get; set; }
        public string ComplaintStatus { get; set; }
        public string ComplaintType { get; set; }
        public int? ParentComplaintId { get; set; }
        public Complaint ParentComplaint { get; set; }
        public DateTime Date { get; set; }
        public byte[] Image { get; set; }
        public bool IsReply { get; set; }
        public ICollection<Complaint> ComplaintReplies { get; set; }=new List<Complaint>();
    }
}


