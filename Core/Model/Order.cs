﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PosCloudAdminPanel.Core.Model
{
    public class Order : AuditableEntity
    {
        public int Id { get; set; }
        public Guid ClientId { get; set; }
        public Client Client { get; set; }
        public string PackageName { get; set; }
        public decimal Price { get; set; }
        public decimal Discount { get; set; }
        public string PaymentTerms { get; set; }
        public DateTime OrderDate { get; set; }
        public bool IsInvoiced { get; set; }
        public string Status { get; set; }
        public virtual ICollection<OrderDetail> OrderDetails { get; set; }=new List<OrderDetail>();
        public virtual ICollection<Invoice> Invoices { get; set; }=new List<Invoice>();
    }
}
