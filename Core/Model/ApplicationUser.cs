﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;

namespace PosCloudAdminPanel.Core.Model
{
    public class ApplicationUser:IdentityUser
    {
        public ICollection<Complaint> Complaints { get; set; }
        public ICollection<ComplaintReply> ComplaintReplies { get; set; }
    }
}
