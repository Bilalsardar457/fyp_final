﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace PosCloudAdminPanel.Core.Model
{
    public class Client:AuditableEntity
    {
        public Guid ClientId { get; set; }
        public int PackageId { get; set; }
        public Package Package { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        
        public string Password { get; set; }
        public string VatNumber { get; set; }   
        public string ConfirmPassword { get; set; }
        public string Address { get; set; }
        
        public string PrimaryContact { get; set; }
        
        public string SecondaryContact { get; set; }
        
        public string City { get; set; }
        
        public string State { get; set; }

        public string Country { get; set; }
        public string CompanyName { get; set; }
        public string CompanyEmail { get; set; }
        public string FaxNo { get; set; }
        public string PostalCode { get; set; }

        public string CompanyAddress { get; set; }
        
        public string CompanyContact { get; set; }
        
        public string CompanyCity { get; set; }
        
        public string CompanyState { get; set; }

        public string CompanyCountry { get; set; }
        public string CompanyNtn { get; set; }

        public string CompanyRegistrationNumber { get; set; }
        public string Currency { get; set; }
        [DefaultValue(0)]
        public bool IsOperational { get; set; }
        public DateTime BusinessStartTime { get; set; }
        public byte[] CompanyLogo { get; set; }
       
        public virtual ICollection<ClientAppKey> ClientAppKeys { get; set; }
        public virtual ICollection<Complaint> Complaints { get; set; }
        public virtual ICollection<Order> Orders { get; set; }
         
    }
}
