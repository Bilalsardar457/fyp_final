﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PosCloudAdminPanel.Core.Model
{
    public class Package : AuditableEntity
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public decimal Price { get; set; }
        public decimal Discount { get; set; }
        public string PaymentTerms { get; set; }
        public virtual ICollection<Client> Clients { get; set; }
        public virtual ICollection<PackageProduct> PackageProducts { get; set; }
    }
}
