﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PosCloudAdminPanel.Core.Model
{
    public class ClientAppKey : AuditableEntity
    {
        
        public Guid AppKey { get; set; }
        public bool IsUsed { get; set; }
        public Guid ClientId { get; set; }
        public Client Client { get; set; }
        public string IMEI { get; set; }

    }
}
