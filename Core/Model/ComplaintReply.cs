﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;

namespace PosCloudAdminPanel.Core.Model
{
    public class ComplaintReply:AuditableEntity
    {
        public int Id { get; set; }
        public int ComplaintId { get; set; }
        public Complaint Complaint { get; set; }
        public string ApplicationUserId { get; set; }
        public string Subject { get; set; }
        public string Description { get; set; }
        public ApplicationUser ApplicationUser { get; set; }
        
    }
}
