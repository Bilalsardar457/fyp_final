﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using PosCloudAdminPanel.Core.Model;

namespace PosCloudAdminPanel.Core.Repository
{
    public interface IPackageRepository
    {
        IEnumerable<Package> GetPackages();
        Package GetPackageById(int id);
        void AddPackage(Package Package);
        void UpdatePackage(int id, Package Package);
        void ChangeStatus(int id, bool status);
        Package GetPackageByClientId(Guid id);
        void DeletePackage(int id);
    }
}
