﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using PosCloudAdminPanel.Core.Model;

namespace PosCloudAdminPanel.Core.Repository
{
    public interface IComplaintReplyRepository
    {
        IEnumerable<ComplaintReply> GetComplaintReplies();
        ComplaintReply GetComplaintReplyById(int id);
        void AddComplaintReply(ComplaintReply complaintReply);
        void UpdateComplaintReply(int id, ComplaintReply complaintReply);
        void DeleteComplaintReply(int id);
    }
}
