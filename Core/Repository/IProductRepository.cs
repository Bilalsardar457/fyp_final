﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using PosCloudAdminPanel.Core.Model;

namespace PosCloudAdminPanel.Core.Repository
{
    public interface IProductRepository
    {
        IEnumerable<Product> GetProducts();
        Product GetProductById(int id);
        void AddProduct(Product Product);
        void UpdateProduct(int id, Product Product);
        void ChangeStatus(int id, bool status);
        void DeleteProduct(int id);
    }
}
