﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using PosCloudAdminPanel.Core.Model;

namespace PosCloudAdminPanel.Core.Repository
{
    public interface IClientAppKeyRepository
    {
        IEnumerable<ClientAppKey> GetAllClientAppKeys();
        IEnumerable<ClientAppKey> GetClientAppKeys(Guid clientId);
        ClientAppKey GetClientAppKeyById(Guid appkey, string imei, Guid clientid);
        void AddClientAppKey(ClientAppKey clientAppKey);
        void UpdateClientAppKey(Guid appkey, string imei, Guid clientid, ClientAppKey clientAppKey);
        void DeleteClientAppKey(Guid appkey, string imei, Guid clientid);
    }
}
