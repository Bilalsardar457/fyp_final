﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using PosCloudAdminPanel.Core.Model;

namespace PosCloudAdminPanel.Core.Repository
{
    public interface IComplaintRepository
    {
        IEnumerable<Complaint> GetComplaints();
        Complaint GetComplaintById(int id);
        void AddComplaint(Complaint complaint);
        void UpdateComplaint(int id, Complaint complaint);
        void DeleteComplaint(int id);
    }
}
