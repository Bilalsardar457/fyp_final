﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using PosCloudAdminPanel.Core.Model;

namespace PosCloudAdminPanel.Core.Repository
{
    public interface IOrderRepository
    {
        IEnumerable<Order> GetOrders();
        Order GetOrderById(int id);
        void AddOrder(Order Order);
        void UpdateOrder(int id, Order Order);
        void ChangeStatus(int id, bool status);
    }
}
