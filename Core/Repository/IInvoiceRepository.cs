﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using PosCloudAdminPanel.Core.Model;

namespace PosCloudAdminPanel.Core.Repository
{
    public interface IInvoiceRepository
    {
        IEnumerable<Invoice> GetInvoices();
        Invoice GetInvoiceById(int id);
        void AddInvoice(Invoice Invoice);
        void UpdateInvoice(int id, Invoice Invoice);
        void ChangeStatus(int id, bool status);
    }
}
