﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using PosCloudAdminPanel.Core.Model;

namespace PosCloudAdminPanel.Core.Repository
{
    public interface IClientRepository
    {
        IEnumerable<Client> GetClients();
        Client GetClientById(Guid id);
        void AddClient(Client Client);
        void UpdateClient(Guid id, Client Client);
        void ChangeStatus(Guid id, bool status);
        Task UpdateClientAsync(Guid id, Client Client);
        Task<IEnumerable<Client>> GetClientsAsync();
        Task<Client> GetClientByIdAsync(Guid id);
        Task AddClientAsync(Client Client);
    }
}
