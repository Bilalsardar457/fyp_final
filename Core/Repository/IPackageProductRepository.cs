﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using PosCloudAdminPanel.Core.Model;

namespace PosCloudAdminPanel.Core.Repository
{
    public interface IPackageProductRepository
    {
        IEnumerable<PackageProduct> GetPackageProducts();
        PackageProduct GetPackageProductById(int id);
        void AddPackageProduct(PackageProduct PackageProduct);
        void UpdatePackageProduct(int id, PackageProduct PackageProduct);
        void ChangeStatus(int id, bool status);
        IEnumerable<PackageProduct> GetPackageProductsByPkgId(int pkgId);
        IEnumerable<PackageProduct> GetPackageProductsByClientId(Guid clientId);
        void DeletePackageProduct(int id);
    }
}
