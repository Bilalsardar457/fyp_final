﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using PosCloudAdminPanel.Core;
using PosCloudAdminPanel.Core.Model;
using PosCloudAdminPanel.Core.ViewModel;
using PosCloudAdminPanel.Extensions;

namespace PosCloudAdminPanel.Controllers
{
    public class AccountController : Controller
    {
        private IUnitOfWork _unitOfWork;

        public AccountController(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }
        public ActionResult SignUp()
        {
            ClientViewModel clientVm = new ClientViewModel();
            ViewBag.edit = "SignUp";
            return View(clientVm);
        }

        [HttpPost]
        public ActionResult SignUp(ClientViewModel clientVm)
        {
            ViewBag.edit = "SignUp";
            try
            {
                if (clientVm.ImageUpload != null)
                {
                    clientVm.CompanyLogo = ImageToByte.GetByteArrayFromImage(clientVm.ImageUpload);
                }
                if (!ModelState.IsValid)
                {
                    var message = string.Join(" | ", ModelState.Values
                        .SelectMany(v => v.Errors)
                        .Select(e => e.ErrorMessage));
                    TempData.Put("Alert", new AlertModel("ModelState Failure, try again. " + message, AlertType.Error));
                    return RedirectToAction("SingUp", "Account");
                }
                else
                {

                    Core.Model.Client client = Mapper.Map<Core.Model.Client>(clientVm);
                    _unitOfWork.ClientRepository.AddClient(client);
                    _unitOfWork.Complete();
                    TempData.Put("Alert", new AlertModel("The client added successfully", AlertType.Success));
                    return RedirectToAction("SingUpList", "Account");

                }
            }
            catch (DbException e)
            {
                TempData.Put("Alert", new AlertModel("Exception Error", AlertType.Error));
                if (e.InnerException != null)
                {
                    if (!string.IsNullOrWhiteSpace(e.InnerException.Message))
                    {
                        if (e.InnerException.InnerException != null)
                        {

                            if (!string.IsNullOrWhiteSpace(e.InnerException.InnerException.Message))
                            {
                                TempData.Put("Alert", new AlertModel(e.InnerException.InnerException.Message, AlertType.Error));
                            }
                        }
                        else
                        {
                            TempData.Put("Alert", new AlertModel(e.InnerException.Message, AlertType.Error));
                        }
                    }
                }
                else
                {
                    TempData.Put("Alert", new AlertModel(e.Message, AlertType.Error));
                }
            }
            catch (Exception e)
            {
                TempData.Put("Alert", new AlertModel("Exception Error", AlertType.Error));
                if (e.InnerException != null)
                {
                    if (!string.IsNullOrWhiteSpace(e.InnerException.Message))
                    {
                        if (e.InnerException.InnerException != null)
                        {

                            if (!string.IsNullOrWhiteSpace(e.InnerException.InnerException.Message))
                            {
                                TempData.Put("Alert", new AlertModel(e.InnerException.InnerException.Message, AlertType.Error));
                            }
                        }
                        else
                        {
                            TempData.Put("Alert", new AlertModel(e.InnerException.Message, AlertType.Error));
                        }
                    }
                }
                else
                {
                    TempData.Put("Alert", new AlertModel(e.Message, AlertType.Error));
                }
            }

            return RedirectToAction("SingUpList", "Account");


        }

    }
}