﻿using System;
using System.Data.Common;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Mvc.ViewEngines;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using Microsoft.Extensions.Configuration;
using PosCloudAdminPanel.Core.Model;
using PosCloudAdminPanel.Core.ViewModel;
using PosCloudAdminPanel.Extensions;
using PosCloudAdminPanel.Models;

namespace PosCloudAdminPanel.Controllers
{

    public class HomeController : Controller
    {
    protected ICompositeViewEngine viewEngine;
        private readonly IConfiguration _configuration;
        public HomeController(ICompositeViewEngine viewEngine, IConfiguration configuration)
        {
            this.viewEngine = viewEngine;
            _configuration = configuration;
        }
        
        public IActionResult Index()
        {
           
            return View();
        }
        public IActionResult signup()
        {

            return View();
        }
        public IActionResult demo()
        {

            return View();
        }
        [HttpPost]
        public async Task<IActionResult> demo(RequestDemo demo)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    var message = string.Join(" | ", ModelState.Values
                        .SelectMany(v => v.Errors)
                        .Select(e => e.ErrorMessage));
                    TempData.Put("Alert", new AlertModel("ModelState Failure, try again. " + message, AlertType.Error));
                    return View(demo);
                }
                string html = RenderViewAsString(demo, "Email");
                string send = RenderViewAsString(demo, "send");
               
                SmtpClient client = null;
                using (client = new SmtpClient(_configuration["Email:Host"], int.Parse(_configuration["Email:Port"])))
                {
                    client.EnableSsl = Convert.ToBoolean(_configuration["Email:EnableSsl"]);
                    client.UseDefaultCredentials = false;
                    client.Credentials = new NetworkCredential(_configuration["Email:Email"], _configuration["Email:Password"]);
                    MailMessage mailMessage = new MailMessage();
                    mailMessage.To.Add(new MailAddress("sales@poscosmic.com"));
                    mailMessage.From = new MailAddress(_configuration["Email:Email"]);
                    mailMessage.Subject = demo.First + " " + demo.Last + " Requested Demo";
                    mailMessage.Body = html;
                    mailMessage.IsBodyHtml = true;
                    await client.SendMailAsync(mailMessage);
                }
                TempData.Put("Alert", new AlertModel("Your request for demo successfully proceeded", AlertType.Success));
                using (client = new SmtpClient(_configuration["Email:Host"], int.Parse(_configuration["Email:Port"])))
                {
                    client.EnableSsl = Convert.ToBoolean(_configuration["Email:EnableSsl"]);
                    client.UseDefaultCredentials = false;
                    client.Credentials = new NetworkCredential(_configuration["Email:Email"], _configuration["Email:Password"]);
                    MailMessage mailMessage = new MailMessage();
                    mailMessage.To.Add(new MailAddress(demo.Email));
                    mailMessage.From = new MailAddress("sales@poscosmic.com");
                    mailMessage.Subject =  "Thank you for requesting a demo of PosCosmic";
                    mailMessage.Body =send;
                    mailMessage.IsBodyHtml = true;
                    await client.SendMailAsync(mailMessage);
                }
                return RedirectToAction("demo");

                
            }
            catch (DbException e)
            {
                TempData.Put("Alert", new AlertModel("Exception Error", AlertType.Error));
                if (e.InnerException != null)
                {
                    if (!string.IsNullOrWhiteSpace(e.InnerException.Message))
                    {
                        if (e.InnerException.InnerException != null)
                        {

                            if (!string.IsNullOrWhiteSpace(e.InnerException.InnerException.Message))
                            {
                                TempData.Put("Alert", new AlertModel(e.InnerException.InnerException.Message, AlertType.Error));
                            }
                        }
                        else
                        {
                            TempData.Put("Alert", new AlertModel(e.InnerException.Message, AlertType.Error));
                        }
                    }
                }
                else
                {
                    TempData.Put("Alert", new AlertModel(e.Message, AlertType.Error));
                }
            }
            catch (Exception e)
            {
                TempData.Put("Alert", new AlertModel("Exception Error", AlertType.Error));
                if (e.InnerException != null)
                {
                    if (!string.IsNullOrWhiteSpace(e.InnerException.Message))
                    {
                        if (e.InnerException.InnerException != null)
                        {

                            if (!string.IsNullOrWhiteSpace(e.InnerException.InnerException.Message))
                            {
                                TempData.Put("Alert", new AlertModel(e.InnerException.InnerException.Message, AlertType.Error));
                            }
                        }
                        else
                        {
                            TempData.Put("Alert", new AlertModel(e.InnerException.Message, AlertType.Error));
                        }
                    }
                }
                else
                {
                    TempData.Put("Alert", new AlertModel(e.Message, AlertType.Error));
                }
            }

            return View(demo);






        }

        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
        protected string RenderViewAsString(object model, string viewName = null)
        {
            viewName = viewName ?? ControllerContext.ActionDescriptor.ActionName;
            ViewData.Model = model;

            using (StringWriter sw = new StringWriter())
            {
                IView view = viewEngine.FindView(ControllerContext, viewName, true).View;
                ViewContext viewContext = new ViewContext(ControllerContext, view, ViewData, TempData, sw, new HtmlHelperOptions());

                view.RenderAsync(viewContext).Wait();

                return sw.GetStringBuilder().ToString();
            }
        }


    }
}
