﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using PosCloudAdminPanel.Core;
using PosCloudAdminPanel.Core.Model;
using PosCloudAdminPanel.Core.ViewModel;
using PosCloudAdminPanel.Extensions;
using System.Security.Claims;
namespace PosCloudAdminPanel.Areas.Client.Controllers
{
    [Authorize]
    public class ComplaintController : ClientBaseController
    {
        private IUnitOfWork _unitOfWork;

        public ComplaintController(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }
        public ActionResult ComplaintList()
        {
            return View(Mapper.Map<ComplaintViewModel[]>(_unitOfWork.ComplaintRepository.GetComplaints().Where(a=>a.ParentComplaintId==null)));
        }

        public ActionResult ViewComplaint(int id)
        {
            ComplaintDisplayViewModel model=new ComplaintDisplayViewModel();
            var data = _unitOfWork.ComplaintRepository.GetComplaintById(id);
            model.ComplaintViewModel = Mapper.Map<ComplaintViewModel>(data);
            model.ComplaintReplies = Mapper.Map<List<ComplaintViewModel>>(data.ComplaintReplies);
            model.Model.ParentComplaintId = model.ComplaintViewModel.ComplaintId;
            model.Model.IsReply = true;
            model.Model.ComplaintType = model.ComplaintViewModel.ComplaintType;
            model.Model.UserId = HttpContext.User.FindFirstValue(ClaimTypes.NameIdentifier);
            model.Model.ComplaintStatus = model.ComplaintViewModel.ComplaintStatus;
            return View(model);
        }
        //Add Complaint Partial
        public ActionResult AddComplaintPartial()
        {
            ComplaintViewModel complaintVm = new ComplaintViewModel();
            complaintVm.UserId = HttpContext.User.FindFirstValue(ClaimTypes.NameIdentifier);
            ViewBag.edit = "AddComplaintPartial";
            return View("AddComplaintPartial",complaintVm);
        }

        [HttpPost]
        public ActionResult AddComplaintPartial(ComplaintViewModel complaintVm)
        {
            ViewBag.edit = "AddComplaintPartial";
            try
            {

                if (complaintVm.ImageUpload != null)
                {
                    complaintVm.Image = ImageToByte.GetByteArrayFromImage(complaintVm.ImageUpload);
                }
                if (!ModelState.IsValid)
                {
                    var message = string.Join(" | ", ModelState.Values
                        .SelectMany(v => v.Errors)
                        .Select(e => e.ErrorMessage));
                    TempData.Put("Alert", new AlertModel("ModelState Failure, try again. " + message, AlertType.Error));
                    return RedirectToAction("ViewComplaint",new{id=complaintVm.ParentComplaintId});
                }
                else

                {

                    Complaint complaint = Mapper.Map<Complaint>(complaintVm);
                    complaint.Date = DateTime.Now;

                    _unitOfWork.ComplaintRepository.AddComplaint(complaint);
                    _unitOfWork.Complete();
                    TempData.Put("Alert", new AlertModel("The complaint added successfully", AlertType.Success));
                    return RedirectToAction("ViewComplaint", new { id = complaintVm.ParentComplaintId });


                }
            }
            catch (DbException e)
            {
                TempData.Put("Alert", new AlertModel("Exception Error", AlertType.Error));
                if (e.InnerException != null)
                {
                    if (!string.IsNullOrWhiteSpace(e.InnerException.Message))
                    {
                        if (e.InnerException.InnerException != null)
                        {

                            if (!string.IsNullOrWhiteSpace(e.InnerException.InnerException.Message))
                            {
                                TempData.Put("Alert", new AlertModel(e.InnerException.InnerException.Message, AlertType.Error));
                            }
                        }
                        else
                        {
                            TempData.Put("Alert", new AlertModel(e.InnerException.Message, AlertType.Error));
                        }
                    }
                }
                else
                {
                    TempData.Put("Alert", new AlertModel(e.Message, AlertType.Error));
                }
            }
            catch (Exception e)
            {
                TempData.Put("Alert", new AlertModel("Exception Error", AlertType.Error));
                if (e.InnerException != null)
                {
                    if (!string.IsNullOrWhiteSpace(e.InnerException.Message))
                    {
                        if (e.InnerException.InnerException != null)
                        {

                            if (!string.IsNullOrWhiteSpace(e.InnerException.InnerException.Message))
                            {
                                TempData.Put("Alert", new AlertModel(e.InnerException.InnerException.Message, AlertType.Error));
                            }
                        }
                        else
                        {
                            TempData.Put("Alert", new AlertModel(e.InnerException.Message, AlertType.Error));
                        }
                    }
                }
                else
                {
                    TempData.Put("Alert", new AlertModel(e.Message, AlertType.Error));
                }
            }

            return RedirectToAction("ViewComplaint", new { id = complaintVm.ParentComplaintId });



        }

        //Add Complaint
        public ActionResult AddComplaint()
        {
            ComplaintViewModel complaintVm = new ComplaintViewModel();
            complaintVm.UserId = HttpContext.User.FindFirstValue(ClaimTypes.NameIdentifier);
            ViewBag.edit = "AddComplaint";
            return View(complaintVm);
        }

        [HttpPost]
        public ActionResult AddComplaint(ComplaintViewModel complaintVm)
        {
            ViewBag.edit = "AddComplaint";
            try
            {
                
                if (complaintVm.ImageUpload != null)
                {
                    complaintVm.Image = ImageToByte.GetByteArrayFromImage(complaintVm.ImageUpload);
                }
                if (!ModelState.IsValid)
                {
                    var message = string.Join(" | ", ModelState.Values
                        .SelectMany(v => v.Errors)
                        .Select(e => e.ErrorMessage));
                    TempData.Put("Alert", new AlertModel("ModelState Failure, try again. " + message, AlertType.Error));
                    return RedirectToAction("AddComplaint", "Complaint");
                }
                else
               
                {

                    Complaint complaint = Mapper.Map<Complaint>(complaintVm);
                    complaint.ComplaintStatus = "Open";
                    complaint.ComplaintType = "Support";
                    complaint.Date = DateTime.Now;

                    _unitOfWork.ComplaintRepository.AddComplaint(complaint);
                    _unitOfWork.Complete();
                    TempData.Put("Alert", new AlertModel("The complaint added successfully", AlertType.Success));
                    return RedirectToAction("ComplaintList", "Complaint");

                }
            }
            catch (DbException e)
            {
                TempData.Put("Alert", new AlertModel("Exception Error", AlertType.Error));
                if (e.InnerException != null)
                {
                    if (!string.IsNullOrWhiteSpace(e.InnerException.Message))
                    {
                        if (e.InnerException.InnerException != null)
                        {

                            if (!string.IsNullOrWhiteSpace(e.InnerException.InnerException.Message))
                            {
                                TempData.Put("Alert", new AlertModel(e.InnerException.InnerException.Message, AlertType.Error));
                            }
                        }
                        else
                        {
                            TempData.Put("Alert", new AlertModel(e.InnerException.Message, AlertType.Error));
                        }
                    }
                }
                else
                {
                    TempData.Put("Alert", new AlertModel(e.Message, AlertType.Error));
                }
            }
            catch (Exception e)
            {
                TempData.Put("Alert", new AlertModel("Exception Error", AlertType.Error));
                if (e.InnerException != null)
                {
                    if (!string.IsNullOrWhiteSpace(e.InnerException.Message))
                    {
                        if (e.InnerException.InnerException != null)
                        {

                            if (!string.IsNullOrWhiteSpace(e.InnerException.InnerException.Message))
                            {
                                TempData.Put("Alert", new AlertModel(e.InnerException.InnerException.Message, AlertType.Error));
                            }
                        }
                        else
                        {
                            TempData.Put("Alert", new AlertModel(e.InnerException.Message, AlertType.Error));
                        }
                    }
                }
                else
                {
                    TempData.Put("Alert", new AlertModel(e.Message, AlertType.Error));
                }
            }

            return RedirectToAction("ComplaintList", "Complaint");


        }


        public ActionResult UpdateComplaint(int id)
        {
            ViewBag.edit = "UpdateComplaint";
            ComplaintViewModel complaintVm =
                Mapper.Map<ComplaintViewModel>(_unitOfWork.ComplaintRepository.GetComplaintById(id));
           
            return View("AddComplaint", complaintVm);
        }
        [HttpPost]
        public ActionResult UpdateComplaint(ComplaintViewModel complaintVm, int id)
        {
            ViewBag.edit = "UpdateComplaint";
            try
            {
                if (complaintVm.ImageUpload != null)
                {
                    complaintVm.Image = ImageToByte.GetByteArrayFromImage(complaintVm.ImageUpload);
                }
                if (!ModelState.IsValid)
                {
                    var message = string.Join(" | ", ModelState.Values
                        .SelectMany(v => v.Errors)
                        .Select(e => e.ErrorMessage));
                    TempData.Put("Alert", new AlertModel("ModelState Failure, try again. " + message, AlertType.Error));

                }
                else
                {
                    Complaint complaint = Mapper.Map<Complaint>(complaintVm);
                    _unitOfWork.ComplaintRepository.UpdateComplaint(id, complaint);
                    _unitOfWork.Complete();
                    TempData.Put("Alert", new AlertModel("The complaint update successfully", AlertType.Success));
                    return RedirectToAction("ComplaintList", "Complaint");
                }
            }
            catch (DbException e)
            {
                TempData.Put("Alert", new AlertModel("Exception Error", AlertType.Error));
                if (e.InnerException != null)
                {
                    if (!string.IsNullOrWhiteSpace(e.InnerException.Message))
                    {
                        if (e.InnerException.InnerException != null)
                        {

                            if (!string.IsNullOrWhiteSpace(e.InnerException.InnerException.Message))
                            {
                                TempData.Put("Alert", new AlertModel(e.InnerException.InnerException.Message, AlertType.Error));
                            }
                        }
                        else
                        {
                            TempData.Put("Alert", new AlertModel(e.InnerException.Message, AlertType.Error));
                        }
                    }
                }
                else
                {
                    TempData.Put("Alert", new AlertModel(e.Message, AlertType.Error));
                }
            }
            catch (Exception e)
            {
                TempData.Put("Alert", new AlertModel("Exception Error", AlertType.Error));
                if (e.InnerException != null)
                {
                    if (!string.IsNullOrWhiteSpace(e.InnerException.Message))
                    {
                        if (e.InnerException.InnerException != null)
                        {

                            if (!string.IsNullOrWhiteSpace(e.InnerException.InnerException.Message))
                            {
                                TempData.Put("Alert", new AlertModel(e.InnerException.InnerException.Message, AlertType.Error));
                            }
                        }
                        else
                        {
                            TempData.Put("Alert", new AlertModel(e.InnerException.Message, AlertType.Error));
                        }
                    }
                }
                else
                {
                    TempData.Put("Alert", new AlertModel(e.Message, AlertType.Error));
                }
            }

            return RedirectToAction("ComplaintList", "Complaint");
        }
        //Complaint reply 
        public ActionResult ComplaintReplyList()
        {
            return View(Mapper.Map<ComplaintReplyViewModel[]>(_unitOfWork.ComplaintReplyRepository.GetComplaintReplies()));
        }

        public ActionResult AddComplaintReply(int id)
        {
            ComplaintReplyViewModel complaintReplyVm = new ComplaintReplyViewModel();
            ViewBag.edit = "AddComplaintReply";
            complaintReplyVm.ComplaintId = id;
            return View(complaintReplyVm);
        }

        [HttpPost]
        public ActionResult AddComplaintReply(ComplaintReplyViewModel complaintReplyVm)
        {
            ViewBag.edit = "AddComplaintReply";
            try
            {

                if (!ModelState.IsValid)
                {
                    var message = string.Join(" | ", ModelState.Values
                        .SelectMany(v => v.Errors)
                        .Select(e => e.ErrorMessage));
                    TempData.Put("Alert", new AlertModel("ModelState Failure, try again. " + message, AlertType.Error));
                }
                else
                {

                    ComplaintReply complaintReply = Mapper.Map<ComplaintReply>(complaintReplyVm);
                    _unitOfWork.ComplaintReplyRepository.AddComplaintReply(complaintReply);
                    _unitOfWork.Complete();
                    TempData.Put("Alert", new AlertModel("The complaintReply added successfully", AlertType.Success));
                    return RedirectToAction("ComplaintReplyList", "ComplaintReply");

                }
            }
            catch (DbException e)
            {
                TempData.Put("Alert", new AlertModel("Exception Error", AlertType.Error));
                if (e.InnerException != null)
                {
                    if (!string.IsNullOrWhiteSpace(e.InnerException.Message))
                    {
                        if (e.InnerException.InnerException != null)
                        {

                            if (!string.IsNullOrWhiteSpace(e.InnerException.InnerException.Message))
                            {
                                TempData.Put("Alert", new AlertModel(e.InnerException.InnerException.Message, AlertType.Error));
                            }
                        }
                        else
                        {
                            TempData.Put("Alert", new AlertModel(e.InnerException.Message, AlertType.Error));
                        }
                    }
                }
                else
                {
                    TempData.Put("Alert", new AlertModel(e.Message, AlertType.Error));
                }
            }
            catch (Exception e)
            {
                TempData.Put("Alert", new AlertModel("Exception Error", AlertType.Error));
                if (e.InnerException != null)
                {
                    if (!string.IsNullOrWhiteSpace(e.InnerException.Message))
                    {
                        if (e.InnerException.InnerException != null)
                        {

                            if (!string.IsNullOrWhiteSpace(e.InnerException.InnerException.Message))
                            {
                                TempData.Put("Alert", new AlertModel(e.InnerException.InnerException.Message, AlertType.Error));
                            }
                        }
                        else
                        {
                            TempData.Put("Alert", new AlertModel(e.InnerException.Message, AlertType.Error));
                        }
                    }
                }
                else
                {
                    TempData.Put("Alert", new AlertModel(e.Message, AlertType.Error));
                }
            }

            return RedirectToAction("ComplaintReplyList", "ComplaintReply");


        }

        
        public ActionResult UpdateComplaintReply(int id)
        {
            ViewBag.edit = "UpdateComplaintReply";
            ComplaintReplyViewModel complaintReplyVm =
                Mapper.Map<ComplaintReplyViewModel>(_unitOfWork.ComplaintReplyRepository.GetComplaintReplyById(id));
            return View("AddComplaintReply", complaintReplyVm);
        }
        [HttpPost]
        public ActionResult UpdateComplaintReply(ComplaintReplyViewModel complaintReplyVm, int id)
        {
            ViewBag.edit = "UpdateComplaintReply";
            try
            {
                if (!ModelState.IsValid)
                {
                    var message = string.Join(" | ", ModelState.Values
                        .SelectMany(v => v.Errors)
                        .Select(e => e.ErrorMessage));
                    TempData.Put("Alert", new AlertModel("ModelState Failure, try again. " + message, AlertType.Error));

                }
                else
                {
                    ComplaintReply complaintReply = Mapper.Map<ComplaintReply>(complaintReplyVm);
                    _unitOfWork.ComplaintReplyRepository.UpdateComplaintReply(id, complaintReply);
                    _unitOfWork.Complete();
                    TempData.Put("Alert", new AlertModel("The complaintReply update successfully", AlertType.Success));
                    return RedirectToAction("ComplaintReplyList", "ComplaintReply");
                }
            }
            catch (DbException e)
            {
                TempData.Put("Alert", new AlertModel("Exception Error", AlertType.Error));
                if (e.InnerException != null)
                {
                    if (!string.IsNullOrWhiteSpace(e.InnerException.Message))
                    {
                        if (e.InnerException.InnerException != null)
                        {

                            if (!string.IsNullOrWhiteSpace(e.InnerException.InnerException.Message))
                            {
                                TempData.Put("Alert", new AlertModel(e.InnerException.InnerException.Message, AlertType.Error));
                            }
                        }
                        else
                        {
                            TempData.Put("Alert", new AlertModel(e.InnerException.Message, AlertType.Error));
                        }
                    }
                }
                else
                {
                    TempData.Put("Alert", new AlertModel(e.Message, AlertType.Error));
                }
            }
            catch (Exception e)
            {
                TempData.Put("Alert", new AlertModel("Exception Error", AlertType.Error));
                if (e.InnerException != null)
                {
                    if (!string.IsNullOrWhiteSpace(e.InnerException.Message))
                    {
                        if (e.InnerException.InnerException != null)
                        {

                            if (!string.IsNullOrWhiteSpace(e.InnerException.InnerException.Message))
                            {
                                TempData.Put("Alert", new AlertModel(e.InnerException.InnerException.Message, AlertType.Error));
                            }
                        }
                        else
                        {
                            TempData.Put("Alert", new AlertModel(e.InnerException.Message, AlertType.Error));
                        }
                    }
                }
                else
                {
                    TempData.Put("Alert", new AlertModel(e.Message, AlertType.Error));
                }
            }

            return RedirectToAction("ComplaintReplyList", "ComplaintReply");
        }

    }

    }
