﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using PosCloudAdminPanel.Core;
using PosCloudAdminPanel.Core.Model;
using PosCloudAdminPanel.Core.ViewModel;

namespace PosCloudAdminPanel.Areas.Client.Controllers
{
    [Authorize]
    public class HomeController : ClientBaseController
    {
        private IUnitOfWork _unitOfWork;

        public HomeController(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public ActionResult Index()
        {
            return View();
        }
    }
}