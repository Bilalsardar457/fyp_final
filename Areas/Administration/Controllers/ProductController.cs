﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using PosCloudAdminPanel.Core;
using PosCloudAdminPanel.Core.Model;
using PosCloudAdminPanel.Core.ViewModel;
using PosCloudAdminPanel.Extensions;

namespace PosCloudAdminPanel.Areas.Administration.Controllers
{
    [Authorize]
    public class ProductController : AdminBaseController
    {
        private IUnitOfWork _unitOfWork;

        public ProductController(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }
        public ActionResult ProductList()
        {
            return View(Mapper.Map<ProductViewModel[]>(_unitOfWork.ProductRepository.GetProducts()));
        }

        public ActionResult AddProduct()
        {
            ProductViewModel productVm = new ProductViewModel();
            ViewBag.edit = "AddProduct";
            return View(productVm);
        }

        [HttpPost]
        public ActionResult AddProduct(ProductViewModel productVm)
        {
            ViewBag.edit = "AddProduct";
            try
            {
               
                if (!ModelState.IsValid)
                {
                    var message = string.Join(" | ", ModelState.Values
                        .SelectMany(v => v.Errors)
                        .Select(e => e.ErrorMessage));
                    TempData.Put("Alert", new AlertModel("ModelState Failure, try again. " + message, AlertType.Error));
                    return View(productVm);
                }
                else
                {

                    Core.Model.Product product = Mapper.Map<Core.Model.Product>(productVm);
                    _unitOfWork.ProductRepository.AddProduct(product);
                    _unitOfWork.Complete();
                    TempData.Put("Alert", new AlertModel("The product added successfully", AlertType.Success));
                    return RedirectToAction("ProductList", "Product");

                }
            }
            catch (DbException e)
            {
                TempData.Put("Alert", new AlertModel("Exception Error", AlertType.Error));
                if (e.InnerException != null)
                {
                    if (!string.IsNullOrWhiteSpace(e.InnerException.Message))
                    {
                        if (e.InnerException.InnerException != null)
                        {

                            if (!string.IsNullOrWhiteSpace(e.InnerException.InnerException.Message))
                            {
                                TempData.Put("Alert", new AlertModel(e.InnerException.InnerException.Message, AlertType.Error));
                            }
                        }
                        else
                        {
                            TempData.Put("Alert", new AlertModel(e.InnerException.Message, AlertType.Error));
                        }
                    }
                }
                else
                {
                    TempData.Put("Alert", new AlertModel(e.Message, AlertType.Error));
                }
            }
            catch (Exception e)
            {
                TempData.Put("Alert", new AlertModel("Exception Error", AlertType.Error));
                if (e.InnerException != null)
                {
                    if (!string.IsNullOrWhiteSpace(e.InnerException.Message))
                    {
                        if (e.InnerException.InnerException != null)
                        {

                            if (!string.IsNullOrWhiteSpace(e.InnerException.InnerException.Message))
                            {
                                TempData.Put("Alert", new AlertModel(e.InnerException.InnerException.Message, AlertType.Error));
                            }
                        }
                        else
                        {
                            TempData.Put("Alert", new AlertModel(e.InnerException.Message, AlertType.Error));
                        }
                    }
                }
                else
                {
                    TempData.Put("Alert", new AlertModel(e.Message, AlertType.Error));
                }
            }

            return View(productVm);


        }


        public ActionResult UpdateProduct(int id)
        {
            ViewBag.edit = "UpdateProduct";
            ProductViewModel productVm =
                Mapper.Map<ProductViewModel>(_unitOfWork.ProductRepository.GetProductById(id));
            return View("AddProduct", productVm);
        }
        [HttpPost]
        public ActionResult UpdateProduct(ProductViewModel productVm, int id)
        {
            ViewBag.edit = "UpdateProduct";
            try
            {
               
                if (!ModelState.IsValid)
                {
                    var message = string.Join(" | ", ModelState.Values
                        .SelectMany(v => v.Errors)
                        .Select(e => e.ErrorMessage));
                    TempData.Put("Alert", new AlertModel("ModelState Failure, try again. " + message, AlertType.Error));
                    return View("AddProduct", productVm);
                }
                else
                {
                    Core.Model.Product product = Mapper.Map<Core.Model.Product>(productVm);
                    _unitOfWork.ProductRepository.UpdateProduct(id, product);
                    _unitOfWork.Complete();
                    TempData.Put("Alert", new AlertModel("The product update successfully", AlertType.Success));
                    return RedirectToAction("ProductList", "Product");
                }
            }
            catch (DbException e)
            {
                TempData.Put("Alert", new AlertModel("Exception Error", AlertType.Error));
                if (e.InnerException != null)
                {
                    if (!string.IsNullOrWhiteSpace(e.InnerException.Message))
                    {
                        if (e.InnerException.InnerException != null)
                        {

                            if (!string.IsNullOrWhiteSpace(e.InnerException.InnerException.Message))
                            {
                                TempData.Put("Alert", new AlertModel(e.InnerException.InnerException.Message, AlertType.Error));
                            }
                        }
                        else
                        {
                            TempData.Put("Alert", new AlertModel(e.InnerException.Message, AlertType.Error));
                        }
                    }
                }
                else
                {
                    TempData.Put("Alert", new AlertModel(e.Message, AlertType.Error));
                }
            }
            catch (Exception e)
            {
                TempData.Put("Alert", new AlertModel("Exception Error", AlertType.Error));
                if (e.InnerException != null)
                {
                    if (!string.IsNullOrWhiteSpace(e.InnerException.Message))
                    {
                        if (e.InnerException.InnerException != null)
                        {

                            if (!string.IsNullOrWhiteSpace(e.InnerException.InnerException.Message))
                            {
                                TempData.Put("Alert", new AlertModel(e.InnerException.InnerException.Message, AlertType.Error));
                            }
                        }
                        else
                        {
                            TempData.Put("Alert", new AlertModel(e.InnerException.Message, AlertType.Error));
                        }
                    }
                }
                else
                {
                    TempData.Put("Alert", new AlertModel(e.Message, AlertType.Error));
                }
            }

            return View("AddProduct", productVm);

        }

        public ActionResult ActivateProduct(int id)
        {
            try
            {
                _unitOfWork.ProductRepository.ChangeStatus(id, true);
                _unitOfWork.Complete();
                TempData.Put("Alert", new AlertModel("The product Activated successfully", AlertType.Success));
                return RedirectToAction("ProductList", "Product");
            }
            catch (DbException e)
            {
                TempData.Put("Alert", new AlertModel("Exception Error", AlertType.Error));
                if (e.InnerException != null)
                {
                    if (!string.IsNullOrWhiteSpace(e.InnerException.Message))
                    {
                        if (e.InnerException.InnerException != null)
                        {

                            if (!string.IsNullOrWhiteSpace(e.InnerException.InnerException.Message))
                            {
                                TempData.Put("Alert", new AlertModel(e.InnerException.InnerException.Message, AlertType.Error));
                            }
                        }
                        else
                        {
                            TempData.Put("Alert", new AlertModel(e.InnerException.Message, AlertType.Error));
                        }
                    }
                }
                else
                {
                    TempData.Put("Alert", new AlertModel(e.Message, AlertType.Error));
                }
            }
            catch (Exception e)
            {
                TempData.Put("Alert", new AlertModel("Exception Error", AlertType.Error));
                if (e.InnerException != null)
                {
                    if (!string.IsNullOrWhiteSpace(e.InnerException.Message))
                    {
                        if (e.InnerException.InnerException != null)
                        {

                            if (!string.IsNullOrWhiteSpace(e.InnerException.InnerException.Message))
                            {
                                TempData.Put("Alert", new AlertModel(e.InnerException.InnerException.Message, AlertType.Error));
                            }
                        }
                        else
                        {
                            TempData.Put("Alert", new AlertModel(e.InnerException.Message, AlertType.Error));
                        }
                    }
                }
                else
                {
                    TempData.Put("Alert", new AlertModel(e.Message, AlertType.Error));
                }
            }

            return RedirectToAction("ProductList", "Product");
        }
        public ActionResult DeActivateProduct(int id)
        {
            try
            {
                _unitOfWork.ProductRepository.ChangeStatus(id, false);
                _unitOfWork.Complete();
                TempData.Put("Alert", new AlertModel("The product DeActivated successfully", AlertType.Success));
                return RedirectToAction("ProductList", "Product");
            }
            catch (DbException e)
            {
                TempData.Put("Alert", new AlertModel("Exception Error", AlertType.Error));
                if (e.InnerException != null)
                {
                    if (!string.IsNullOrWhiteSpace(e.InnerException.Message))
                    {
                        if (e.InnerException.InnerException != null)
                        {

                            if (!string.IsNullOrWhiteSpace(e.InnerException.InnerException.Message))
                            {
                                TempData.Put("Alert", new AlertModel(e.InnerException.InnerException.Message, AlertType.Error));
                            }
                        }
                        else
                        {
                            TempData.Put("Alert", new AlertModel(e.InnerException.Message, AlertType.Error));
                        }
                    }
                }
                else
                {
                    TempData.Put("Alert", new AlertModel(e.Message, AlertType.Error));
                }
            }
            catch (Exception e)
            {
                TempData.Put("Alert", new AlertModel("Exception Error", AlertType.Error));
                if (e.InnerException != null)
                {
                    if (!string.IsNullOrWhiteSpace(e.InnerException.Message))
                    {
                        if (e.InnerException.InnerException != null)
                        {

                            if (!string.IsNullOrWhiteSpace(e.InnerException.InnerException.Message))
                            {
                                TempData.Put("Alert", new AlertModel(e.InnerException.InnerException.Message, AlertType.Error));
                            }
                        }
                        else
                        {
                            TempData.Put("Alert", new AlertModel(e.InnerException.Message, AlertType.Error));
                        }
                    }
                }
                else
                {
                    TempData.Put("Alert", new AlertModel(e.Message, AlertType.Error));
                }
            }

            return RedirectToAction("ProductList", "Product");
        }

        public ActionResult DeleteProduct(int id)
        {
            try
            {
                _unitOfWork.ProductRepository.DeleteProduct(id);
                _unitOfWork.Complete();
                TempData.Put("Alert", new AlertModel("The product Deleted successfully", AlertType.Success));
                return RedirectToAction("ProductList", "Product");
            }
            catch (DbException e)
            {
                TempData.Put("Alert", new AlertModel("Exception Error", AlertType.Error));
                if (e.InnerException != null)
                {
                    if (!string.IsNullOrWhiteSpace(e.InnerException.Message))
                    {
                        if (e.InnerException.InnerException != null)
                        {

                            if (!string.IsNullOrWhiteSpace(e.InnerException.InnerException.Message))
                            {
                                TempData.Put("Alert", new AlertModel(e.InnerException.InnerException.Message, AlertType.Error));
                            }
                        }
                        else
                        {
                            TempData.Put("Alert", new AlertModel(e.InnerException.Message, AlertType.Error));
                        }
                    }
                }
                else
                {
                    TempData.Put("Alert", new AlertModel(e.Message, AlertType.Error));
                }
            }
            catch (Exception e)
            {
                TempData.Put("Alert", new AlertModel("Exception Error", AlertType.Error));
                if (e.InnerException != null)
                {
                    if (!string.IsNullOrWhiteSpace(e.InnerException.Message))
                    {
                        if (e.InnerException.InnerException != null)
                        {

                            if (!string.IsNullOrWhiteSpace(e.InnerException.InnerException.Message))
                            {
                                TempData.Put("Alert", new AlertModel(e.InnerException.InnerException.Message, AlertType.Error));
                            }
                        }
                        else
                        {
                            TempData.Put("Alert", new AlertModel(e.InnerException.Message, AlertType.Error));
                        }
                    }
                }
                else
                {
                    TempData.Put("Alert", new AlertModel(e.Message, AlertType.Error));
                }
            }

            return RedirectToAction("ProductList", "Product");
        }

    }
}