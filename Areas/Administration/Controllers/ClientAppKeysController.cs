﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using PosCloudAdminPanel.Core;
using PosCloudAdminPanel.Core.Model;
using PosCloudAdminPanel.Core.ViewModel;
using PosCloudAdminPanel.Data;
using PosCloudAdminPanel.Extensions;

namespace PosCloudAdminPanel.Areas.Administration.Controllers
{
    [Authorize]
    public class ClientAppKeysController : AdminBaseController
    {
        private IUnitOfWork _unitOfWork;

        public ClientAppKeysController(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }
        public ActionResult ClientAppKeyList()
        {
            return View(Mapper.Map<ClientAppKeyViewModel[]>(_unitOfWork.ClientAppKeyRepository.GetAllClientAppKeys().OrderBy(a=>a.ClientId).ToList()));
        }

        public ActionResult AddClientAppKey()
        {
            ViewBag.edit = "AddClientAppKey";
            ClientAppKeyViewModel clientAppVm = new ClientAppKeyViewModel();
            clientAppVm.ClientDdl =
                _unitOfWork.ClientRepository.GetClients().Select(a=> new SelectListItem{Value = a.ClientId.ToString(), Text = a.FirstName});
            return View(clientAppVm);
        }

        [HttpPost]
        public ActionResult AddClientAppKey(ClientAppKeyViewModel ClientAppKeyVm)
        {
            ViewBag.edit = "AddClientAppKey";
            try
            {
                if (!ModelState.IsValid)
                {
                    var message = string.Join(" | ", ModelState.Values
                        .SelectMany(v => v.Errors)
                        .Select(e => e.ErrorMessage));
                    TempData.Put("Alert", new AlertModel("ModelState Failure, try again. " + message, AlertType.Error));
                    return RedirectToAction("AddClientAppKey", "ClientAppKeys");
                }
                else
                {
                    ClientAppKey ClientAppKey = Mapper.Map<ClientAppKey>(ClientAppKeyVm);
                    _unitOfWork.ClientAppKeyRepository.AddClientAppKey(ClientAppKey);
                    _unitOfWork.Complete();
                    TempData.Put("Alert", new AlertModel("The ClientAppKey added successfully", AlertType.Success));
                    return RedirectToAction("ClientAppKeyList", "ClientAppKeys");

                }
            }
            catch (DbException e)
            {
                TempData.Put("Alert", new AlertModel("Exception Error", AlertType.Error));
                if (e.InnerException != null)
                {
                    if (!string.IsNullOrWhiteSpace(e.InnerException.Message))
                    {
                        if (e.InnerException.InnerException != null)
                        {

                            if (!string.IsNullOrWhiteSpace(e.InnerException.InnerException.Message))
                            {
                                TempData.Put("Alert", new AlertModel(e.InnerException.InnerException.Message, AlertType.Error));
                            }
                        }
                        else
                        {
                            TempData.Put("Alert", new AlertModel(e.InnerException.Message, AlertType.Error));
                        }
                    }
                }
                else
                {
                    TempData.Put("Alert", new AlertModel(e.Message, AlertType.Error));
                }
            }
            catch (Exception e)
            {
                TempData.Put("Alert", new AlertModel("Exception Error", AlertType.Error));
                if (e.InnerException != null)
                {
                    if (!string.IsNullOrWhiteSpace(e.InnerException.Message))
                    {
                        if (e.InnerException.InnerException != null)
                        {

                            if (!string.IsNullOrWhiteSpace(e.InnerException.InnerException.Message))
                            {
                                TempData.Put("Alert", new AlertModel(e.InnerException.InnerException.Message, AlertType.Error));
                            }
                        }
                        else
                        {
                            TempData.Put("Alert", new AlertModel(e.InnerException.Message, AlertType.Error));
                        }
                    }
                }
                else
                {
                    TempData.Put("Alert", new AlertModel(e.Message, AlertType.Error));
                }
            }

            return RedirectToAction("ClientAppKeyList", "ClientAppKeys");


        }


        public ActionResult UpdateClientAppKey(Guid appkey, string imei, Guid clientid)
        {
            ViewBag.edit = "UpdateClientAppKey";
            ClientAppKeyViewModel ClientAppKeyVm =
                Mapper.Map<ClientAppKeyViewModel>(_unitOfWork.ClientAppKeyRepository.GetClientAppKeyById(appkey, imei, clientid));
            ClientAppKeyVm.ClientDdl =
                _unitOfWork.ClientRepository.GetClients().Select(a => new SelectListItem { Value = a.ClientId.ToString(), Text = a.FirstName });
            return RedirectToAction("AddClientAppKey", "ClientAppKeys");
        }
        [HttpPost]
        public ActionResult UpdateClientAppKey(ClientAppKeyViewModel ClientAppKeyVm, Guid appkey, string imei, Guid clientid)
        {
            ViewBag.edit = "UpdateClientAppKey";
            try
            {
                if (!ModelState.IsValid)
                {
                    var message = string.Join(" | ", ModelState.Values
                        .SelectMany(v => v.Errors)
                        .Select(e => e.ErrorMessage));
                    TempData.Put("Alert", new AlertModel("ModelState Failure, try again. " + message, AlertType.Error));
                }
                else
                {
                    ClientAppKey ClientAppKey = Mapper.Map<ClientAppKey>(ClientAppKeyVm);
                    _unitOfWork.ClientAppKeyRepository.UpdateClientAppKey(appkey, imei, clientid, ClientAppKey);
                    _unitOfWork.Complete();
                    TempData.Put("Alert", new AlertModel("The App Key Activated successfully", AlertType.Success));

                    return RedirectToAction("ClientAppKeyList", "ClientAppKeys");
                }
            }
            catch (DbException e)
            {
                TempData.Put("Alert", new AlertModel("Exception Error", AlertType.Error));
                if (e.InnerException != null)
                {
                    if (!string.IsNullOrWhiteSpace(e.InnerException.Message))
                    {
                        if (e.InnerException.InnerException != null)
                        {

                            if (!string.IsNullOrWhiteSpace(e.InnerException.InnerException.Message))
                            {
                                TempData.Put("Alert", new AlertModel(e.InnerException.InnerException.Message, AlertType.Error));
                            }
                        }
                        else
                        {
                            TempData.Put("Alert", new AlertModel(e.InnerException.Message, AlertType.Error));
                        }
                    }
                }
                else
                {
                    TempData.Put("Alert", new AlertModel(e.Message, AlertType.Error));
                }
            }
            catch (Exception e)
            {
                TempData.Put("Alert", new AlertModel("Exception Error", AlertType.Error));
                if (e.InnerException != null)
                {
                    if (!string.IsNullOrWhiteSpace(e.InnerException.Message))
                    {
                        if (e.InnerException.InnerException != null)
                        {

                            if (!string.IsNullOrWhiteSpace(e.InnerException.InnerException.Message))
                            {
                                TempData.Put("Alert", new AlertModel(e.InnerException.InnerException.Message, AlertType.Error));
                            }
                        }
                        else
                        {
                            TempData.Put("Alert", new AlertModel(e.InnerException.Message, AlertType.Error));
                        }
                    }
                }
                else
                {
                    TempData.Put("Alert", new AlertModel(e.Message, AlertType.Error));
                }
            }

            return RedirectToAction("ClientAppKeyList", "ClientAppKeys");
        }

        public ActionResult DeleteClientAppKey(Guid appkey, string imei, Guid clientid)
        {
            try
            {
                _unitOfWork.ClientAppKeyRepository.DeleteClientAppKey(appkey, imei, clientid);
                _unitOfWork.Complete();
                TempData["Alert"] = new AlertModel("The ClientAppKey deleted successfully", AlertType.Success);
                return RedirectToAction("ClientAppKeyList", "ClientAppKeys");
            }
            catch (DbException e)
            {
                TempData.Put("Alert", new AlertModel("Exception Error", AlertType.Error));
                if (e.InnerException != null)
                {
                    if (!string.IsNullOrWhiteSpace(e.InnerException.Message))
                    {
                        if (e.InnerException.InnerException != null)
                        {

                            if (!string.IsNullOrWhiteSpace(e.InnerException.InnerException.Message))
                            {
                                TempData.Put("Alert", new AlertModel(e.InnerException.InnerException.Message, AlertType.Error));
                            }
                        }
                        else
                        {
                            TempData.Put("Alert", new AlertModel(e.InnerException.Message, AlertType.Error));
                        }
                    }
                }
                else
                {
                    TempData.Put("Alert", new AlertModel(e.Message, AlertType.Error));
                }
            }
            catch (Exception e)
            {
                TempData.Put("Alert", new AlertModel("Exception Error", AlertType.Error));
                if (e.InnerException != null)
                {
                    if (!string.IsNullOrWhiteSpace(e.InnerException.Message))
                    {
                        if (e.InnerException.InnerException != null)
                        {

                            if (!string.IsNullOrWhiteSpace(e.InnerException.InnerException.Message))
                            {
                                TempData.Put("Alert", new AlertModel(e.InnerException.InnerException.Message, AlertType.Error));
                            }
                        }
                        else
                        {
                            TempData.Put("Alert", new AlertModel(e.InnerException.Message, AlertType.Error));
                        }
                    }
                }
                else
                {
                    TempData.Put("Alert", new AlertModel(e.Message, AlertType.Error));
                }
            }

            return RedirectToAction("ClientAppKeyList", "ClientAppKeys");
        }
    }
}