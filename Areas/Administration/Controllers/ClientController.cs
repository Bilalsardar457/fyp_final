﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Net.Mail;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Mvc.ViewEngines;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using PosCloudAdminPanel.Core;
using PosCloudAdminPanel.Core.Model;
using PosCloudAdminPanel.Core.ViewModel;
using PosCloudAdminPanel.Extensions;

namespace PosCloudAdminPanel.Areas.Administration.Controllers
{
    [Authorize]
    public class ClientController : AdminBaseController
    {
        private IUnitOfWork _unitOfWork;
        private readonly IConfiguration _configuration;
        protected ICompositeViewEngine viewEngine;

        public ClientController(IUnitOfWork unitOfWork, ICompositeViewEngine viewEngine, IConfiguration configuration)
        {
            _unitOfWork = unitOfWork;
            this.viewEngine = viewEngine;
            _configuration = configuration;
        }
        public ActionResult ClientList()
        {
            return View(Mapper.Map<ClientViewModel[]>(_unitOfWork.ClientRepository.GetClients()));


        }
        public async Task<ActionResult> AddClientData(Guid id)
        {

            ClientViewModel clientVm = Mapper.Map<ClientViewModel>(_unitOfWork.ClientRepository.GetClientById(id));
            //--------------------------------------------------------------
            string apiUrl = _configuration["apiUrl"];

            using (HttpClient client1 = new HttpClient())
            {
                client1.BaseAddress = new Uri(apiUrl);
               
                // HTTP POST
                var sendData = new ClientApiViewModel
                {
                    Name = clientVm.CompanyName,
                    Address = clientVm.Address,
                    ArabicName = clientVm.CompanyName,
                    City = clientVm.CompanyCity,
                    Contact = clientVm.CompanyContact,
                    State = clientVm.CompanyState,
                    BusinessStartTime = clientVm.BusinessStartTime,
                    Currency = clientVm.Currency,
                    Email = clientVm.CompanyEmail,
                    IsOperational = clientVm.IsOperational,
                    Password = clientVm.Password,
                    VatNumber = clientVm.VatNumber
                };
                var json = JsonConvert.SerializeObject(sendData);
                var stringContent = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
                var postTask = client1.PostAsync("clients", stringContent);
                postTask.Wait();

                var result = postTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    string html = RenderViewAsString(clientVm, "Credential");

                    SmtpClient client = null;

                    using (client = new SmtpClient(_configuration["Email:Host"], int.Parse(_configuration["Email:Port"])))
                    {
                        client.EnableSsl = Convert.ToBoolean(_configuration["Email:EnableSsl"]);
                        client.UseDefaultCredentials = false;
                        client.Credentials = new NetworkCredential(_configuration["Email:Email"], _configuration["Email:Password"]);
                        MailMessage mailMessage = new MailMessage();
                        mailMessage.To.Add(new MailAddress(clientVm.Email));
                        mailMessage.From = new MailAddress("Pehnawacloths@gmail.com");
                        mailMessage.Subject = "POSCloud Credentials";
                        mailMessage.Body = html;
                        mailMessage.IsBodyHtml = true;
                        await client.SendMailAsync(mailMessage);
                    }
                    TempData.Put("Alert", new AlertModel("The client added successfully on cloud", AlertType.Success));
                    return RedirectToAction("ClientList", "Client");

                }

            }
            //---------------------------------------------------------------------------------
            TempData.Put("Alert", new AlertModel("The client adding failed on cloud", AlertType.Error));
            return RedirectToAction("ClientList", "Client");



           
        }
        protected string RenderViewAsString(object model, string viewName = null)
        {
            viewName = viewName ?? ControllerContext.ActionDescriptor.ActionName;
            ViewData.Model = model;

            using (StringWriter sw = new StringWriter())
            {
                IView view = viewEngine.FindView(ControllerContext, viewName, true).View;
                ViewContext viewContext = new ViewContext(ControllerContext, view, ViewData, TempData, sw, new HtmlHelperOptions());

                view.RenderAsync(viewContext).Wait();

                return sw.GetStringBuilder().ToString();
            }
        }
        public ActionResult AddClient()
        {
            ClientViewModel clientVm = new ClientViewModel();
            clientVm.PackageDdl = _unitOfWork.PackageRepository.GetPackages()
                .Select(a => new SelectListItem {Text = a.Name, Value = a.Id.ToString()});
            ViewBag.edit = "AddClient";
            return View(clientVm);
        }

        [HttpPost]
       
            public async Task<IActionResult>AddClient(ClientViewModel clientVm)
            {
                ViewBag.edit = "AddClient";
                clientVm.PackageDdl = _unitOfWork.PackageRepository.GetPackages()
                    .Select(a => new SelectListItem { Text = a.Name, Value = a.Id.ToString() });
                try
                {
                    if (clientVm.ImageUpload != null)
                    {
                        clientVm.CompanyLogo = ImageToByte.GetByteArrayFromImage(clientVm.ImageUpload);
                    }
                    if (!ModelState.IsValid)
                    {
                        var message = string.Join(" | ", ModelState.Values
                            .SelectMany(v => v.Errors)
                            .Select(e => e.ErrorMessage));
                        TempData.Put("Alert", new AlertModel("ModelState Failure, try again. " + message, AlertType.Error));
                        return View(clientVm);
                    }
                    else
                    {

                        Core.Model.Client client = Mapper.Map<Core.Model.Client>(clientVm);
                        _unitOfWork.ClientRepository.AddClient(client);
                        _unitOfWork.Complete();
                        TempData.Put("Alert", new AlertModel("The client added successfully", AlertType.Success));
                    


                    return RedirectToAction("ClientList", "Client");

                    }
                }
                catch (DbException e)
                {
                    TempData.Put("Alert", new AlertModel("Exception Error", AlertType.Error));
                    if (e.InnerException != null)
                    {
                        if (!string.IsNullOrWhiteSpace(e.InnerException.Message))
                        {
                            if (e.InnerException.InnerException != null)
                            {

                                if (!string.IsNullOrWhiteSpace(e.InnerException.InnerException.Message))
                                {
                                    TempData.Put("Alert", new AlertModel(e.InnerException.InnerException.Message, AlertType.Error));
                                }
                            }
                            else
                            {
                                TempData.Put("Alert", new AlertModel(e.InnerException.Message, AlertType.Error));
                            }
                        }
                    }
                    else
                    {
                        TempData.Put("Alert", new AlertModel(e.Message, AlertType.Error));
                    }
                }
                catch (Exception e)
                {
                    TempData.Put("Alert", new AlertModel("Exception Error", AlertType.Error));
                    if (e.InnerException != null)
                    {
                        if (!string.IsNullOrWhiteSpace(e.InnerException.Message))
                        {
                            if (e.InnerException.InnerException != null)
                            {

                                if (!string.IsNullOrWhiteSpace(e.InnerException.InnerException.Message))
                                {
                                    TempData.Put("Alert", new AlertModel(e.InnerException.InnerException.Message, AlertType.Error));
                                }
                            }
                            else
                            {
                                TempData.Put("Alert", new AlertModel(e.InnerException.Message, AlertType.Error));
                            }
                        }
                    }
                    else
                    {
                        TempData.Put("Alert", new AlertModel(e.Message, AlertType.Error));
                    }
                }

                return View(clientVm);


            }
        


    

        
        public ActionResult UpdateClient(Guid id)
        {
            ViewBag.edit = "UpdateClient";
            ClientViewModel clientVm =
                Mapper.Map<ClientViewModel>(_unitOfWork.ClientRepository.GetClientById(id));
            clientVm.PackageDdl = _unitOfWork.PackageRepository.GetPackages()
                .Select(a => new SelectListItem { Text = a.Name, Value = a.Id.ToString() });
            return View("AddClient", clientVm);
        }
        [HttpPost]
        public ActionResult UpdateClient(ClientViewModel clientVm, Guid id)
        {
            ViewBag.edit = "UpdateClient";
            clientVm.PackageDdl = _unitOfWork.PackageRepository.GetPackages()
                .Select(a => new SelectListItem { Text = a.Name, Value = a.Id.ToString() });
            try
            {
                if (clientVm.ImageUpload != null)
                {
                    clientVm.CompanyLogo = ImageToByte.GetByteArrayFromImage(clientVm.ImageUpload);
                }
                if (!ModelState.IsValid)
                {
                    var message = string.Join(" | ", ModelState.Values
                        .SelectMany(v => v.Errors)
                        .Select(e => e.ErrorMessage));
                    TempData.Put("Alert", new AlertModel("ModelState Failure, try again. " + message, AlertType.Error));
                    return View("AddClient", clientVm);
                }
                else
                {
                    Core.Model.Client client = Mapper.Map<Core.Model.Client>(clientVm);
                    _unitOfWork.ClientRepository.UpdateClient(id,client);
                    _unitOfWork.Complete();
                    TempData.Put("Alert", new AlertModel("The client update successfully", AlertType.Success));
                    return RedirectToAction("ClientList", "Client");
                }
            }
            catch (DbException e)
            {
                TempData.Put("Alert", new AlertModel("Exception Error", AlertType.Error));
                if (e.InnerException != null)
                {
                    if (!string.IsNullOrWhiteSpace(e.InnerException.Message))
                    {
                        if (e.InnerException.InnerException != null)
                        {

                            if (!string.IsNullOrWhiteSpace(e.InnerException.InnerException.Message))
                            {
                                TempData.Put("Alert", new AlertModel(e.InnerException.InnerException.Message, AlertType.Error));
                            }
                        }
                        else
                        {
                            TempData.Put("Alert", new AlertModel(e.InnerException.Message, AlertType.Error));
                        }
                    }
                }
                else
                {
                    TempData.Put("Alert", new AlertModel(e.Message, AlertType.Error));
                }
            }
            catch (Exception e)
            {
                TempData.Put("Alert", new AlertModel("Exception Error", AlertType.Error));
                if (e.InnerException != null)
                {
                    if (!string.IsNullOrWhiteSpace(e.InnerException.Message))
                    {
                        if (e.InnerException.InnerException != null)
                        {

                            if (!string.IsNullOrWhiteSpace(e.InnerException.InnerException.Message))
                            {
                                TempData.Put("Alert", new AlertModel(e.InnerException.InnerException.Message, AlertType.Error));
                            }
                        }
                        else
                        {
                            TempData.Put("Alert", new AlertModel(e.InnerException.Message, AlertType.Error));
                        }
                    }
                }
                else
                {
                    TempData.Put("Alert", new AlertModel(e.Message, AlertType.Error));
                }
            }

            return View("AddClient", clientVm);

        }

        public ActionResult ActivateClient(Guid id)
        {
            try
            {
                _unitOfWork.ClientRepository.ChangeStatus(id,true);
                _unitOfWork.Complete();
                TempData.Put("Alert", new AlertModel("The client Activated successfully", AlertType.Success));
                return RedirectToAction("ClientList", "Client");
            }
            catch (DbException e)
            {
                TempData.Put("Alert", new AlertModel("Exception Error", AlertType.Error));
                if (e.InnerException != null)
                {
                    if (!string.IsNullOrWhiteSpace(e.InnerException.Message))
                    {
                        if (e.InnerException.InnerException != null)
                        {

                            if (!string.IsNullOrWhiteSpace(e.InnerException.InnerException.Message))
                            {
                                TempData.Put("Alert", new AlertModel(e.InnerException.InnerException.Message, AlertType.Error));
                            }
                        }
                        else
                        {
                            TempData.Put("Alert", new AlertModel(e.InnerException.Message, AlertType.Error));
                        }
                    }
                }
                else
                {
                    TempData.Put("Alert", new AlertModel(e.Message, AlertType.Error));
                }
            }
            catch (Exception e)
            {
                TempData.Put("Alert", new AlertModel("Exception Error", AlertType.Error));
                if (e.InnerException != null)
                {
                    if (!string.IsNullOrWhiteSpace(e.InnerException.Message))
                    {
                        if (e.InnerException.InnerException != null)
                        {

                            if (!string.IsNullOrWhiteSpace(e.InnerException.InnerException.Message))
                            {
                                TempData.Put("Alert", new AlertModel(e.InnerException.InnerException.Message, AlertType.Error));
                            }
                        }
                        else
                        {
                            TempData.Put("Alert", new AlertModel(e.InnerException.Message, AlertType.Error));
                        }
                    }
                }
                else
                {
                    TempData.Put("Alert", new AlertModel(e.Message, AlertType.Error));
                }
            }

            return RedirectToAction("ClientList", "Client");
        }
        public ActionResult DeActivateClient(Guid id)
        {
            try
            {
                _unitOfWork.ClientRepository.ChangeStatus(id, false);
                _unitOfWork.Complete();
                TempData.Put("Alert", new AlertModel("The client DeActivated successfully", AlertType.Success));
                return RedirectToAction("ClientList", "Client");
            }
            catch (DbException e)
            {
                TempData.Put("Alert", new AlertModel("Exception Error", AlertType.Error));
                if (e.InnerException != null)
                {
                    if (!string.IsNullOrWhiteSpace(e.InnerException.Message))
                    {
                        if (e.InnerException.InnerException != null)
                        {

                            if (!string.IsNullOrWhiteSpace(e.InnerException.InnerException.Message))
                            {
                                TempData.Put("Alert", new AlertModel(e.InnerException.InnerException.Message, AlertType.Error));
                            }
                        }
                        else
                        {
                            TempData.Put("Alert", new AlertModel(e.InnerException.Message, AlertType.Error));
                        }
                    }
                }
                else
                {
                    TempData.Put("Alert", new AlertModel(e.Message, AlertType.Error));
                }
            }
            catch (Exception e)
            {
                TempData.Put("Alert", new AlertModel("Exception Error", AlertType.Error));
                if (e.InnerException != null)
                {
                    if (!string.IsNullOrWhiteSpace(e.InnerException.Message))
                    {
                        if (e.InnerException.InnerException != null)
                        {

                            if (!string.IsNullOrWhiteSpace(e.InnerException.InnerException.Message))
                            {
                                TempData.Put("Alert", new AlertModel(e.InnerException.InnerException.Message, AlertType.Error));
                            }
                        }
                        else
                        {
                            TempData.Put("Alert", new AlertModel(e.InnerException.Message, AlertType.Error));
                        }
                    }
                }
                else
                {
                    TempData.Put("Alert", new AlertModel(e.Message, AlertType.Error));
                }
            }

            return RedirectToAction("ClientList", "Client");
        }

   

    }

}