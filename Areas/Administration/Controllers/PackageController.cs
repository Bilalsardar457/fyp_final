﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using PosCloudAdminPanel.Core;
using PosCloudAdminPanel.Core.Model;
using PosCloudAdminPanel.Core.ViewModel;
using PosCloudAdminPanel.Extensions;

namespace PosCloudAdminPanel.Areas.Administration.Controllers
{
    [Authorize]
    public class PackageController : AdminBaseController
    {
        private IUnitOfWork _unitOfWork;

        public PackageController(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }
        public ActionResult PackageList()
        {
            return View(Mapper.Map<PackageViewModel[]>(_unitOfWork.PackageRepository.GetPackages()));
        }

        public ActionResult AddPackage()
        {
            PackageViewModel packageVm = new PackageViewModel();
            ViewBag.edit = "AddPackage";
            return View(packageVm);
        }

        [HttpPost]
        public ActionResult AddPackage(PackageViewModel packageVm)
        {
            ViewBag.edit = "AddPackage";
            try
            {

                if (!ModelState.IsValid)
                {
                    var message = string.Join(" | ", ModelState.Values
                        .SelectMany(v => v.Errors)
                        .Select(e => e.ErrorMessage));
                    TempData.Put("Alert", new AlertModel("ModelState Failure, try again. " + message, AlertType.Error));
                    return View(packageVm);
                }
                else
                {

                    Core.Model.Package package = Mapper.Map<Core.Model.Package>(packageVm);
                    _unitOfWork.PackageRepository.AddPackage(package);
                    _unitOfWork.Complete();
                    TempData.Put("Alert", new AlertModel("The package added successfully", AlertType.Success));
                    return RedirectToAction("PackageList", "Package");

                }
            }
            catch (DbException e)
            {
                TempData.Put("Alert", new AlertModel("Exception Error", AlertType.Error));
                if (e.InnerException != null)
                {
                    if (!string.IsNullOrWhiteSpace(e.InnerException.Message))
                    {
                        if (e.InnerException.InnerException != null)
                        {

                            if (!string.IsNullOrWhiteSpace(e.InnerException.InnerException.Message))
                            {
                                TempData.Put("Alert", new AlertModel(e.InnerException.InnerException.Message, AlertType.Error));
                            }
                        }
                        else
                        {
                            TempData.Put("Alert", new AlertModel(e.InnerException.Message, AlertType.Error));
                        }
                    }
                }
                else
                {
                    TempData.Put("Alert", new AlertModel(e.Message, AlertType.Error));
                }
            }
            catch (Exception e)
            {
                TempData.Put("Alert", new AlertModel("Exception Error", AlertType.Error));
                if (e.InnerException != null)
                {
                    if (!string.IsNullOrWhiteSpace(e.InnerException.Message))
                    {
                        if (e.InnerException.InnerException != null)
                        {

                            if (!string.IsNullOrWhiteSpace(e.InnerException.InnerException.Message))
                            {
                                TempData.Put("Alert", new AlertModel(e.InnerException.InnerException.Message, AlertType.Error));
                            }
                        }
                        else
                        {
                            TempData.Put("Alert", new AlertModel(e.InnerException.Message, AlertType.Error));
                        }
                    }
                }
                else
                {
                    TempData.Put("Alert", new AlertModel(e.Message, AlertType.Error));
                }
            }

            return View(packageVm);


        }


        public ActionResult UpdatePackage(int id)
        {
            ViewBag.edit = "UpdatePackage";
            PackageViewModel packageVm =
                Mapper.Map<PackageViewModel>(_unitOfWork.PackageRepository.GetPackageById(id));
            return View("AddPackage", packageVm);
        }
        [HttpPost]
        public ActionResult UpdatePackage(PackageViewModel packageVm, int id)
        {
            ViewBag.edit = "UpdatePackage";
            try
            {

                if (!ModelState.IsValid)
                {
                    var message = string.Join(" | ", ModelState.Values
                        .SelectMany(v => v.Errors)
                        .Select(e => e.ErrorMessage));
                    TempData.Put("Alert", new AlertModel("ModelState Failure, try again. " + message, AlertType.Error));
                    return View("AddPackage", packageVm);
                }
                else
                {
                    Core.Model.Package package = Mapper.Map<Core.Model.Package>(packageVm);
                    _unitOfWork.PackageRepository.UpdatePackage(id, package);
                    _unitOfWork.Complete();
                    TempData.Put("Alert", new AlertModel("The package update successfully", AlertType.Success));
                    return RedirectToAction("PackageList", "Package");
                }
            }
            catch (DbException e)
            {
                TempData.Put("Alert", new AlertModel("Exception Error", AlertType.Error));
                if (e.InnerException != null)
                {
                    if (!string.IsNullOrWhiteSpace(e.InnerException.Message))
                    {
                        if (e.InnerException.InnerException != null)
                        {

                            if (!string.IsNullOrWhiteSpace(e.InnerException.InnerException.Message))
                            {
                                TempData.Put("Alert", new AlertModel(e.InnerException.InnerException.Message, AlertType.Error));
                            }
                        }
                        else
                        {
                            TempData.Put("Alert", new AlertModel(e.InnerException.Message, AlertType.Error));
                        }
                    }
                }
                else
                {
                    TempData.Put("Alert", new AlertModel(e.Message, AlertType.Error));
                }
            }
            catch (Exception e)
            {
                TempData.Put("Alert", new AlertModel("Exception Error", AlertType.Error));
                if (e.InnerException != null)
                {
                    if (!string.IsNullOrWhiteSpace(e.InnerException.Message))
                    {
                        if (e.InnerException.InnerException != null)
                        {

                            if (!string.IsNullOrWhiteSpace(e.InnerException.InnerException.Message))
                            {
                                TempData.Put("Alert", new AlertModel(e.InnerException.InnerException.Message, AlertType.Error));
                            }
                        }
                        else
                        {
                            TempData.Put("Alert", new AlertModel(e.InnerException.Message, AlertType.Error));
                        }
                    }
                }
                else
                {
                    TempData.Put("Alert", new AlertModel(e.Message, AlertType.Error));
                }
            }

            return View("AddPackage", packageVm);

        }

        public ActionResult ActivatePackage(int id)
        {
            try
            {
                _unitOfWork.PackageRepository.ChangeStatus(id, true);
                _unitOfWork.Complete();
                TempData.Put("Alert", new AlertModel("The package Activated successfully", AlertType.Success));
                return RedirectToAction("PackageList", "Package");
            }
            catch (DbException e)
            {
                TempData.Put("Alert", new AlertModel("Exception Error", AlertType.Error));
                if (e.InnerException != null)
                {
                    if (!string.IsNullOrWhiteSpace(e.InnerException.Message))
                    {
                        if (e.InnerException.InnerException != null)
                        {

                            if (!string.IsNullOrWhiteSpace(e.InnerException.InnerException.Message))
                            {
                                TempData.Put("Alert", new AlertModel(e.InnerException.InnerException.Message, AlertType.Error));
                            }
                        }
                        else
                        {
                            TempData.Put("Alert", new AlertModel(e.InnerException.Message, AlertType.Error));
                        }
                    }
                }
                else
                {
                    TempData.Put("Alert", new AlertModel(e.Message, AlertType.Error));
                }
            }
            catch (Exception e)
            {
                TempData.Put("Alert", new AlertModel("Exception Error", AlertType.Error));
                if (e.InnerException != null)
                {
                    if (!string.IsNullOrWhiteSpace(e.InnerException.Message))
                    {
                        if (e.InnerException.InnerException != null)
                        {

                            if (!string.IsNullOrWhiteSpace(e.InnerException.InnerException.Message))
                            {
                                TempData.Put("Alert", new AlertModel(e.InnerException.InnerException.Message, AlertType.Error));
                            }
                        }
                        else
                        {
                            TempData.Put("Alert", new AlertModel(e.InnerException.Message, AlertType.Error));
                        }
                    }
                }
                else
                {
                    TempData.Put("Alert", new AlertModel(e.Message, AlertType.Error));
                }
            }

            return RedirectToAction("PackageList", "Package");
        }
        public ActionResult DeActivatePackage(int id)
        {
            try
            {
                _unitOfWork.PackageRepository.ChangeStatus(id, false);
                _unitOfWork.Complete();
                TempData.Put("Alert", new AlertModel("The package DeActivated successfully", AlertType.Success));
                return RedirectToAction("PackageList", "Package");
            }
            catch (DbException e)
            {
                TempData.Put("Alert", new AlertModel("Exception Error", AlertType.Error));
                if (e.InnerException != null)
                {
                    if (!string.IsNullOrWhiteSpace(e.InnerException.Message))
                    {
                        if (e.InnerException.InnerException != null)
                        {

                            if (!string.IsNullOrWhiteSpace(e.InnerException.InnerException.Message))
                            {
                                TempData.Put("Alert", new AlertModel(e.InnerException.InnerException.Message, AlertType.Error));
                            }
                        }
                        else
                        {
                            TempData.Put("Alert", new AlertModel(e.InnerException.Message, AlertType.Error));
                        }
                    }
                }
                else
                {
                    TempData.Put("Alert", new AlertModel(e.Message, AlertType.Error));
                }
            }
            catch (Exception e)
            {
                TempData.Put("Alert", new AlertModel("Exception Error", AlertType.Error));
                if (e.InnerException != null)
                {
                    if (!string.IsNullOrWhiteSpace(e.InnerException.Message))
                    {
                        if (e.InnerException.InnerException != null)
                        {

                            if (!string.IsNullOrWhiteSpace(e.InnerException.InnerException.Message))
                            {
                                TempData.Put("Alert", new AlertModel(e.InnerException.InnerException.Message, AlertType.Error));
                            }
                        }
                        else
                        {
                            TempData.Put("Alert", new AlertModel(e.InnerException.Message, AlertType.Error));
                        }
                    }
                }
                else
                {
                    TempData.Put("Alert", new AlertModel(e.Message, AlertType.Error));
                }
            }

            return RedirectToAction("PackageList", "Package");
        }
        public ActionResult DeletePackage(int id)
        {
            try
            {
                _unitOfWork.PackageRepository.DeletePackage(id);
                _unitOfWork.Complete();
                TempData.Put("Alert", new AlertModel("The package Deleted successfully", AlertType.Success));
                return RedirectToAction("PackageList", "Package");
            }
            catch (DbException e)
            {
                TempData.Put("Alert", new AlertModel("Exception Error", AlertType.Error));
                if (e.InnerException != null)
                {
                    if (!string.IsNullOrWhiteSpace(e.InnerException.Message))
                    {
                        if (e.InnerException.InnerException != null)
                        {

                            if (!string.IsNullOrWhiteSpace(e.InnerException.InnerException.Message))
                            {
                                TempData.Put("Alert", new AlertModel(e.InnerException.InnerException.Message, AlertType.Error));
                            }
                        }
                        else
                        {
                            TempData.Put("Alert", new AlertModel(e.InnerException.Message, AlertType.Error));
                        }
                    }
                }
                else
                {
                    TempData.Put("Alert", new AlertModel(e.Message, AlertType.Error));
                }
            }
            catch (Exception e)
            {
                TempData.Put("Alert", new AlertModel("Exception Error", AlertType.Error));
                if (e.InnerException != null)
                {
                    if (!string.IsNullOrWhiteSpace(e.InnerException.Message))
                    {
                        if (e.InnerException.InnerException != null)
                        {

                            if (!string.IsNullOrWhiteSpace(e.InnerException.InnerException.Message))
                            {
                                TempData.Put("Alert", new AlertModel(e.InnerException.InnerException.Message, AlertType.Error));
                            }
                        }
                        else
                        {
                            TempData.Put("Alert", new AlertModel(e.InnerException.Message, AlertType.Error));
                        }
                    }
                }
                else
                {
                    TempData.Put("Alert", new AlertModel(e.Message, AlertType.Error));
                }
            }

            return RedirectToAction("PackageList", "Package");
        }
        //Package Product
        public ActionResult PackageProductList()
        {
            return View(Mapper.Map<PackageProductViewModel[]>(_unitOfWork.PackageProductRepository.GetPackageProducts()));
        }

        public ActionResult AddPackageProduct()
        {
            PackageProductViewModel packageProductVm = new PackageProductViewModel();
            packageProductVm.PackageDdl = _unitOfWork.PackageRepository.GetPackages()
                .Select(a => new SelectListItem {Text = a.Name, Value = a.Id.ToString()});
            packageProductVm.ProductDdl = _unitOfWork.ProductRepository.GetProducts()
                .Select(a => new SelectListItem {Text = a.Name, Value = a.Id.ToString()});
            ViewBag.edit = "AddPackageProduct";
            return View(packageProductVm);
        }

        [HttpPost]
        public ActionResult AddPackageProduct(PackageProductViewModel packageProductVm)
        {
            ViewBag.edit = "AddPackageProduct";
            packageProductVm.PackageDdl = _unitOfWork.PackageRepository.GetPackages()
                .Select(a => new SelectListItem { Text = a.Name, Value = a.Id.ToString() });
            packageProductVm.ProductDdl = _unitOfWork.ProductRepository.GetProducts()
                .Select(a => new SelectListItem { Text = a.Name, Value = a.Id.ToString() });
            try
            {

                if (!ModelState.IsValid)
                {
                    var message = string.Join(" | ", ModelState.Values
                        .SelectMany(v => v.Errors)
                        .Select(e => e.ErrorMessage));
                    TempData.Put("Alert", new AlertModel("ModelState Failure, try again. " + message, AlertType.Error));
                    return View(packageProductVm);
                }
                else
                {

                    PackageProduct packageProduct = Mapper.Map<PackageProduct>(packageProductVm);
                    _unitOfWork.PackageProductRepository.AddPackageProduct(packageProduct);
                    _unitOfWork.Complete();
                    TempData.Put("Alert", new AlertModel("The packageProduct added successfully", AlertType.Success));
                    return RedirectToAction("PackageProductList", "Package");

                }
            }
            catch (DbException e)
            {
                TempData.Put("Alert", new AlertModel("Exception Error", AlertType.Error));
                if (e.InnerException != null)
                {
                    if (!string.IsNullOrWhiteSpace(e.InnerException.Message))
                    {
                        if (e.InnerException.InnerException != null)
                        {

                            if (!string.IsNullOrWhiteSpace(e.InnerException.InnerException.Message))
                            {
                                TempData.Put("Alert", new AlertModel(e.InnerException.InnerException.Message, AlertType.Error));
                            }
                        }
                        else
                        {
                            TempData.Put("Alert", new AlertModel(e.InnerException.Message, AlertType.Error));
                        }
                    }
                }
                else
                {
                    TempData.Put("Alert", new AlertModel(e.Message, AlertType.Error));
                }
            }
            catch (Exception e)
            {
                TempData.Put("Alert", new AlertModel("Exception Error", AlertType.Error));
                if (e.InnerException != null)
                {
                    if (!string.IsNullOrWhiteSpace(e.InnerException.Message))
                    {
                        if (e.InnerException.InnerException != null)
                        {

                            if (!string.IsNullOrWhiteSpace(e.InnerException.InnerException.Message))
                            {
                                TempData.Put("Alert", new AlertModel(e.InnerException.InnerException.Message, AlertType.Error));
                            }
                        }
                        else
                        {
                            TempData.Put("Alert", new AlertModel(e.InnerException.Message, AlertType.Error));
                        }
                    }
                }
                else
                {
                    TempData.Put("Alert", new AlertModel(e.Message, AlertType.Error));
                }
            }

            return View(packageProductVm);


        }


        public ActionResult UpdatePackageProduct(int id)
        {
            ViewBag.edit = "UpdatePackageProduct";
            PackageProductViewModel packageProductVm =
                Mapper.Map<PackageProductViewModel>(_unitOfWork.PackageProductRepository.GetPackageProductById(id));
            packageProductVm.PackageDdl = _unitOfWork.PackageRepository.GetPackages()
                .Select(a => new SelectListItem { Text = a.Name, Value = a.Id.ToString() });
            packageProductVm.ProductDdl = _unitOfWork.ProductRepository.GetProducts()
                .Select(a => new SelectListItem { Text = a.Name, Value = a.Id.ToString() });
            return View("AddPackageProduct", packageProductVm);
        }
        [HttpPost]
        public ActionResult UpdatePackageProduct(PackageProductViewModel packageProductVm, int id)
        {
            ViewBag.edit = "UpdatePackageProduct";
            packageProductVm.PackageDdl = _unitOfWork.PackageRepository.GetPackages()
                .Select(a => new SelectListItem { Text = a.Name, Value = a.Id.ToString() });
            packageProductVm.ProductDdl = _unitOfWork.ProductRepository.GetProducts()
                .Select(a => new SelectListItem { Text = a.Name, Value = a.Id.ToString() });
            try
            {

                if (!ModelState.IsValid)
                {
                    var message = string.Join(" | ", ModelState.Values
                        .SelectMany(v => v.Errors)
                        .Select(e => e.ErrorMessage));
                    TempData.Put("Alert", new AlertModel("ModelState Failure, try again. " + message, AlertType.Error));
                    return View("AddPackageProduct", packageProductVm);
                }
                else
                {
                    Core.Model.PackageProduct packageProduct = Mapper.Map<Core.Model.PackageProduct>(packageProductVm);
                    _unitOfWork.PackageProductRepository.UpdatePackageProduct(id, packageProduct);
                    _unitOfWork.Complete();
                    TempData.Put("Alert", new AlertModel("The packageProduct update successfully", AlertType.Success));
                    return RedirectToAction("PackageProductList", "Package");
                }
            }
            catch (DbException e)
            {
                TempData.Put("Alert", new AlertModel("Exception Error", AlertType.Error));
                if (e.InnerException != null)
                {
                    if (!string.IsNullOrWhiteSpace(e.InnerException.Message))
                    {
                        if (e.InnerException.InnerException != null)
                        {

                            if (!string.IsNullOrWhiteSpace(e.InnerException.InnerException.Message))
                            {
                                TempData.Put("Alert", new AlertModel(e.InnerException.InnerException.Message, AlertType.Error));
                            }
                        }
                        else
                        {
                            TempData.Put("Alert", new AlertModel(e.InnerException.Message, AlertType.Error));
                        }
                    }
                }
                else
                {
                    TempData.Put("Alert", new AlertModel(e.Message, AlertType.Error));
                }
            }
            catch (Exception e)
            {
                TempData.Put("Alert", new AlertModel("Exception Error", AlertType.Error));
                if (e.InnerException != null)
                {
                    if (!string.IsNullOrWhiteSpace(e.InnerException.Message))
                    {
                        if (e.InnerException.InnerException != null)
                        {

                            if (!string.IsNullOrWhiteSpace(e.InnerException.InnerException.Message))
                            {
                                TempData.Put("Alert", new AlertModel(e.InnerException.InnerException.Message, AlertType.Error));
                            }
                        }
                        else
                        {
                            TempData.Put("Alert", new AlertModel(e.InnerException.Message, AlertType.Error));
                        }
                    }
                }
                else
                {
                    TempData.Put("Alert", new AlertModel(e.Message, AlertType.Error));
                }
            }

            return View("AddPackageProduct", packageProductVm);

        }

        public ActionResult ActivatePackageProduct(int id)
        {
            try
            {
                _unitOfWork.PackageProductRepository.ChangeStatus(id, true);
                _unitOfWork.Complete();
                TempData.Put("Alert", new AlertModel("The packageProduct Activated successfully", AlertType.Success));
                return RedirectToAction("PackageProductList", "Package");
            }
            catch (DbException e)
            {
                TempData.Put("Alert", new AlertModel("Exception Error", AlertType.Error));
                if (e.InnerException != null)
                {
                    if (!string.IsNullOrWhiteSpace(e.InnerException.Message))
                    {
                        if (e.InnerException.InnerException != null)
                        {

                            if (!string.IsNullOrWhiteSpace(e.InnerException.InnerException.Message))
                            {
                                TempData.Put("Alert", new AlertModel(e.InnerException.InnerException.Message, AlertType.Error));
                            }
                        }
                        else
                        {
                            TempData.Put("Alert", new AlertModel(e.InnerException.Message, AlertType.Error));
                        }
                    }
                }
                else
                {
                    TempData.Put("Alert", new AlertModel(e.Message, AlertType.Error));
                }
            }
            catch (Exception e)
            {
                TempData.Put("Alert", new AlertModel("Exception Error", AlertType.Error));
                if (e.InnerException != null)
                {
                    if (!string.IsNullOrWhiteSpace(e.InnerException.Message))
                    {
                        if (e.InnerException.InnerException != null)
                        {

                            if (!string.IsNullOrWhiteSpace(e.InnerException.InnerException.Message))
                            {
                                TempData.Put("Alert", new AlertModel(e.InnerException.InnerException.Message, AlertType.Error));
                            }
                        }
                        else
                        {
                            TempData.Put("Alert", new AlertModel(e.InnerException.Message, AlertType.Error));
                        }
                    }
                }
                else
                {
                    TempData.Put("Alert", new AlertModel(e.Message, AlertType.Error));
                }
            }

            return RedirectToAction("PackageProductList", "Package");
        }
        public ActionResult DeActivatePackageProduct(int id)
        {
            try
            {
                _unitOfWork.PackageProductRepository.ChangeStatus(id, false);
                _unitOfWork.Complete();
                TempData.Put("Alert", new AlertModel("The packageProduct DeActivated successfully", AlertType.Success));
                return RedirectToAction("PackageProductList", "Package");
            }
            catch (DbException e)
            {
                TempData.Put("Alert", new AlertModel("Exception Error", AlertType.Error));
                if (e.InnerException != null)
                {
                    if (!string.IsNullOrWhiteSpace(e.InnerException.Message))
                    {
                        if (e.InnerException.InnerException != null)
                        {

                            if (!string.IsNullOrWhiteSpace(e.InnerException.InnerException.Message))
                            {
                                TempData.Put("Alert", new AlertModel(e.InnerException.InnerException.Message, AlertType.Error));
                            }
                        }
                        else
                        {
                            TempData.Put("Alert", new AlertModel(e.InnerException.Message, AlertType.Error));
                        }
                    }
                }
                else
                {
                    TempData.Put("Alert", new AlertModel(e.Message, AlertType.Error));
                }
            }
            catch (Exception e)
            {
                TempData.Put("Alert", new AlertModel("Exception Error", AlertType.Error));
                if (e.InnerException != null)
                {
                    if (!string.IsNullOrWhiteSpace(e.InnerException.Message))
                    {
                        if (e.InnerException.InnerException != null)
                        {

                            if (!string.IsNullOrWhiteSpace(e.InnerException.InnerException.Message))
                            {
                                TempData.Put("Alert", new AlertModel(e.InnerException.InnerException.Message, AlertType.Error));
                            }
                        }
                        else
                        {
                            TempData.Put("Alert", new AlertModel(e.InnerException.Message, AlertType.Error));
                        }
                    }
                }
                else
                {
                    TempData.Put("Alert", new AlertModel(e.Message, AlertType.Error));
                }
            }

            return RedirectToAction("PackageProductList", "Package");
        }
        public ActionResult DeletePackageProduct(int id)
        {
            try
            {
                _unitOfWork.PackageProductRepository.DeletePackageProduct(id);
                _unitOfWork.Complete();
                TempData.Put("Alert", new AlertModel("The package product Deleted successfully", AlertType.Success));
                return RedirectToAction("PackageProductList", "Package");
            }
            catch (DbException e)
            {
                TempData.Put("Alert", new AlertModel("Exception Error", AlertType.Error));
                if (e.InnerException != null)
                {
                    if (!string.IsNullOrWhiteSpace(e.InnerException.Message))
                    {
                        if (e.InnerException.InnerException != null)
                        {

                            if (!string.IsNullOrWhiteSpace(e.InnerException.InnerException.Message))
                            {
                                TempData.Put("Alert", new AlertModel(e.InnerException.InnerException.Message, AlertType.Error));
                            }
                        }
                        else
                        {
                            TempData.Put("Alert", new AlertModel(e.InnerException.Message, AlertType.Error));
                        }
                    }
                }
                else
                {
                    TempData.Put("Alert", new AlertModel(e.Message, AlertType.Error));
                }
            }
            catch (Exception e)
            {
                TempData.Put("Alert", new AlertModel("Exception Error", AlertType.Error));
                if (e.InnerException != null)
                {
                    if (!string.IsNullOrWhiteSpace(e.InnerException.Message))
                    {
                        if (e.InnerException.InnerException != null)
                        {

                            if (!string.IsNullOrWhiteSpace(e.InnerException.InnerException.Message))
                            {
                                TempData.Put("Alert", new AlertModel(e.InnerException.InnerException.Message, AlertType.Error));
                            }
                        }
                        else
                        {
                            TempData.Put("Alert", new AlertModel(e.InnerException.Message, AlertType.Error));
                        }
                    }
                }
                else
                {
                    TempData.Put("Alert", new AlertModel(e.Message, AlertType.Error));
                }
            }

            return RedirectToAction("PackageProductList", "Package");
        }
    }
}